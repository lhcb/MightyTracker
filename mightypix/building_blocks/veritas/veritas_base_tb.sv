
`ifndef veritas_base_tb
    `define veritas_base_tb

    `define VERITAS_TB_TEST_WAIT 150

    class veritas_base_tb;

        string current_test = "NOTEST";
        string current_suite = "DEFAULT_SUITE";
        
        // Test Group selection
        //-----------------
        task test_suite(input string name);
            
            
            #`VERITAS_TB_TEST_WAIT this.current_suite = name;
            this.current_test = "NOTEST";
            $display("[veritas] @%d Suite: %s *************",$time(),current_suite);

        endtask
         task test_suite_stop();
            this.current_suite = "DEFAULT_SUITE";
            #150 this.current_suite = "DEFAULT_SUITE";
        endtask

        // Test Selection
        //----------------
        task test_start(input string name);
            $display("[veritas] @%d Test: %s.%s <<<<<<<<<<<<<<<",$time(),current_suite,name);
            #`VERITAS_TB_TEST_WAIT this.current_test = name;

        endtask

        task test_stop();
            this.current_test = "NOTEST";
            #150 this.current_test = "NOTEST";
        endtask


    endclass : veritas_base_tb
`endif


