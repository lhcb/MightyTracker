// i2c_monitor_vvc.sv
//
// Thomas Frei
//
// Monitors data on sda line and relays data to scoreboard. Triggers events on stop condition
// and restart condition.
//
// Parameters:      Name                    Description
// -----------------------------------------------------------------------------------------
// Constructor:     i2c_if                  Interface with I2C connections
//                  scoreboard_mailbox      Mailbox containing data for scoreboard
//                  stopE                   Stop event of I2C transmission
//                  restartE                Restart event of I2C transmission
// ------------------------------------------------------------------------------------------
   
   `ifndef I2C_MONITOR_VVC
    `define I2C_MONITOR_VVC

    // ------------------------------------------------------------------- 
    // 
    // Includes
    //                  
    // -------------------------------------------------------------------

    `include "i2c_global.sv"
    `include "i2c_interfaces.sv"
    `include "monitor_structures.sv"

    // ------------------------------------------------------------------- 
    // 
    // Class
    //                  
    // -------------------------------------------------------------------

    class I2CMonitor;

        // external mailbox
        local mailbox scoreboard_mailbox;

        // external interfaces
        local virtual i2c_wire_if i2c_if;

        // external events
        local event restartE;
        local event stopE;

        // mailbox for monitor data
        local mailbox data_box = new();
        
        // start event
        local event startE;

        // ------------------------------------------------------------------- 
        // 
        // Functions
        //                  
        // -------------------------------------------------------------------

        // assign interface, mailbox and events
        function new(virtual i2c_wire_if i2c_if, mailbox scoreboard_mailbox, event stopE, event restartE);
            this.i2c_if = i2c_if;
            this.scoreboard_mailbox = scoreboard_mailbox;
            this.stopE = stopE;
            this.restartE = restartE;
        endfunction

        // ------------------------------------------------------------------- 
        // 
        // Tasks
        //                  
        // -------------------------------------------------------------------

        // run tasks
        task run();
            fork
                check_for_start();
                read_wire();
                output_monitor();
            join_none
        endtask

        // ------------------------------------------------------------------- 
        // 
        // Local tasks
        //                  
        // -------------------------------------------------------------------

        // output monitor data
        local task output_monitor();
            sdaTransaction mailbox_transaction = new();

            // handle mailbox data
            forever begin
                data_box.get(mailbox_transaction);

                // forward to scoreboard
                `ifdef INFO
                    scoreboard_mailbox.put(mailbox_transaction);
                `endif                    

                // display data
                `ifdef DEBUG
                    $display("I2C DEBUG:\t\t %p, Time: %0t ns", mailbox_transaction, $time/1000.0);
                `endif
            end
        endtask

        // read data on sda wire
        local task read_wire();
            sdaTransaction current_transaction = new();
            bit read_next_byte;
            integer bit_pos;

            // at start event
            forever begin
                @ (startE) begin
                    read_next_byte = 1;
                    bit_pos = 0;
                    
                    // read in data until loop disabled
                    do begin
                        // for all remaining bits in communication
                        for (; bit_pos < `SIZE_I2C; bit_pos++) begin
                            // each posedge scl
                            @(posedge i2c_if.scl);

                            // get data byte
                            if (bit_pos < `SIZE_BYTE) 
                                current_transaction.data[7-bit_pos] = i2c_if.sda;

                            // get ack (last bit)
                            else begin
                                current_transaction.ack = i2c_if.sda;
                                data_box.put(current_transaction);
                            end
                        end
                        
                        // check for restart condition, stop condition
                        // or next byte transfer
                        fork
                            // restart condition raised
                            // -> start at first bit
                            begin
                                @ (negedge i2c_if.sda);
                                if (i2c_if.scl) begin
                                    ->restartE;
                                    bit_pos = 0;
                                end
                            end

                            // stop condition raised
                            // -> break loop
                            begin
                                @ (posedge i2c_if.sda);
                                if (i2c_if.scl)
                                    read_next_byte = 0;
                            end

                            // next byte transfer
                            // -> save first bit and restart at second bit
                            begin
                                #1; // next cycle, else posedge will be skipped
                                @ (posedge i2c_if.scl);
                                current_transaction = new();
                                current_transaction.data[7] = i2c_if.sda;
                                @ (negedge i2c_if.scl);
                                bit_pos = 1;
                            end
                        join_any

                        // disable remaining running forks
                        disable fork;
                    end while(read_next_byte);

                    // trigger stop event
                    ->stopE;
                end 
            end
        endtask

        // trigger startE event at start condition
        local task check_for_start();
            forever 
                @ (negedge i2c_if.sda) 
                    if (i2c_if.scl) 
                        ->startE;   
        endtask
    endclass
`endif