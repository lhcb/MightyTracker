// mightypix_tests_vvc.sv
//
// Thomas Frei
//
// Class for (static) MightyPix test routines.
//
// -------------------------------------------------------------------------------

`ifndef MIGHTYPIX_TESTS_VVC
    `define MIGHTYPIX_TESTS_VVC

    // ------------------------------------------------------------------- 
    // 
    // Includes
    //                  
    // -------------------------------------------------------------------

    `include "address_space_pkg.sv"
    `include "i2c_global.sv"
    `include "i2c_interfaces.sv"
    `include "monitor_structures.sv"
    
    import address_space_pkg::*;

    // ------------------------------------------------------------------- 
    // 
    // Definitions
    //                  
    // -------------------------------------------------------------------

    // testbench chip IDs
    `define CHIP_ID_BROADCAST   3'b000
    `define CHIP_ID_A           3'b010
    `define CHIP_ID_B           3'b011
    `define CHIP_ID_C           3'b100

    // query delay 
    `define QUERY_DELAY         500

    // ------------------------------------------------------------------- 
    // 
    // Class
    //                  
    // -------------------------------------------------------------------

    class MightyPixTest;
        
        // ------------------------------------------------------------------- 
        // 
        // Tasks
        //                  
        // -------------------------------------------------------------------

        // test basic functionality
        static task functionality_test (lpGBT lpGBT_main);
            // test repeat and burst write
            #`QUERY_DELAY disp("Repeat write 5 bytes");
            lpGBT_main.i2c_write_rand(`CHIP_ID_A, `ADDR_RAND, `M_REPEAT, 5);
            #`QUERY_DELAY disp("Burst write 5 bytes");
            lpGBT_main.i2c_write_rand(`CHIP_ID_A, `ADDR_RAND, `M_BURST, 5);

            // test burst and repeat read
            #`QUERY_DELAY disp("Burst read 3 bytes");
            lpGBT_main.i2c_read(`CHIP_ID_A, 3);
            #`QUERY_DELAY disp("Repeat write 1 byte");
            lpGBT_main.i2c_write_rand(`CHIP_ID_A, `ADDR_RAND, `M_REPEAT, 1);
            #`QUERY_DELAY disp("Repeat read 3 bytes");
            lpGBT_main.i2c_read(`CHIP_ID_A, 3);

            // test invalid register write
            #`QUERY_DELAY disp("Burst write 1 byte with invalid register (must fail)");
            lpGBT_main.i2c_write_rand(`CHIP_ID_A, `ADDR_MAX + 5, `M_BURST, 1);

            // test address overflow by reading address out of range
            #`QUERY_DELAY disp("Burst write 1 byte on last register");
            lpGBT_main.i2c_write_rand(`CHIP_ID_A, `ADDR_MAX, `M_BURST, 1);
            #`QUERY_DELAY disp("Burst read 1 byte with overflow to invalid register (must fail)");
            lpGBT_main.i2c_read(`CHIP_ID_A, 1);

            // test mixed operation 
            #`QUERY_DELAY disp("Repeat direct read 2 bytes");
            lpGBT_main.i2c_read_direct(`CHIP_ID_A, `ADDR_RAND, `M_REPEAT, 2);

            // second chip ID read and write
            #`QUERY_DELAY disp("Repeat write 5 bytes on second chip id");
            lpGBT_main.i2c_write_rand(`CHIP_ID_B, `ADDR_RAND, `M_REPEAT, 5);
            #`QUERY_DELAY disp("Read 2 bytes on other chip id");
            lpGBT_main.i2c_read(`CHIP_ID_B, 2);

            // read nonexistent chip ID
            #`QUERY_DELAY disp("Read 1 byte on incorrect chip id (must fail)");
            lpGBT_main.i2c_read(`CHIP_ID_C, 1);

            // broadcast write 
            #`QUERY_DELAY disp("Burst write 3 bytes over broadcast");
            lpGBT_main.i2c_write_rand(`CHIP_ID_BROADCAST, `ADDR_RAND, `M_BURST, 3);

            // write on two different addresses for each ID
            #`QUERY_DELAY disp("Burst write 1 byte on chip A");
            lpGBT_main.i2c_write_rand(`CHIP_ID_A, `ADDR_RAND, `M_BURST, 1);
            #`QUERY_DELAY disp("Burst write 1 byte on chip B");
            lpGBT_main.i2c_write_rand(`CHIP_ID_B, `ADDR_RAND, `M_BURST, 1);

            // broadcast read on two different addresses
            #`QUERY_DELAY disp("Burst read 3 bytes over broadcast (likely to fail)");
            lpGBT_main.i2c_read(`CHIP_ID_BROADCAST, 3);

            // test mixed operation on two different addresses 
            #`QUERY_DELAY disp("Burst direct read 2 bytes over broadcast (chances to fail)");
            lpGBT_main.i2c_read_direct(`CHIP_ID_BROADCAST, `ADDR_RAND, `M_BURST, 2);

            // set address for chip and read 
            // (direct read in two parts)
            #`QUERY_DELAY disp("Set address of second chip id (no logging)");
            lpGBT_main.i2c_set_address(`CHIP_ID_B, `ADDR_RAND, `M_REPEAT);
            #`QUERY_DELAY disp("Read 2 bytes on second chip id");
            lpGBT_main.i2c_read(`CHIP_ID_B, 2);
        endtask

        // test register address space 
        static task shift_register_test (lpGBT lpGBT_main);
            logic [7:0] data [$];
            bit sin [$];

            // enable i2c usage
            #`QUERY_DELAY disp("Enable i2c");
            data.push_back(8'h03);
            lpGBT_main.i2c_write(`CHIP_ID_A, ADDR_ENABLE, `M_REPEAT, data);

            // read enable
            #`QUERY_DELAY disp("Read enable register");
            lpGBT_main.i2c_read(`CHIP_ID_A, 1);

            // write in shift register
            #`QUERY_DELAY disp("Write shift register");
            lpGBT_main.i2c_write_rand(`CHIP_ID_A, ADDR_SHIFT_REGISTER, `M_REPEAT, 3);

            // illegal shift register read
            #`QUERY_DELAY disp("Illegal shift register read (must fail)");
            lpGBT_main.i2c_read(`CHIP_ID_A, 2);

            // read qconfig
            #`QUERY_DELAY disp("Read QConfig");
            lpGBT_main.i2c_read_direct(`CHIP_ID_A, ADDR_QCONF_START, `M_BURST, CONFIG_NUM_BYTES);

            // read state machine
            //#`QUERY_DELAY disp("Read state machine");
            //lpGBT_main.i2c_read_direct(`CHIP_ID_A, ADDR_STATE, `M_REPEAT, 2);

            // write tfc
            #`QUERY_DELAY disp("Write tfc");
            data.push_back(8'h53);
            lpGBT_main.i2c_write(`CHIP_ID_A, ADDR_TFC, `M_REPEAT, data);

            // write use register
            #`QUERY_DELAY disp("Write use register");
            data.push_back(8'h53);
            lpGBT_main.i2c_write(`CHIP_ID_A, ADDR_ENABLE, `M_REPEAT, data);

            // illegal writes
            #`QUERY_DELAY disp("Illegal state write (must fail)");
            lpGBT_main.i2c_write_rand(`CHIP_ID_A, ADDR_STATE, `M_REPEAT, 2);
            #`QUERY_DELAY disp("Illegal QConfig write (must fail)");
            lpGBT_main.i2c_write_rand(`CHIP_ID_A, ADDR_QCONF_START, `M_REPEAT, 2);

            // read on QConfig 
            // (last accessed register, even if it failed)
            #`QUERY_DELAY disp("Repeat read QConfig start (last accessed register)");
            lpGBT_main.i2c_read(`CHIP_ID_A, 2);

            // read readout
            #`QUERY_DELAY disp("Readout access");
            lpGBT_main.i2c_read_direct(`CHIP_ID_A, ADDR_READOUT, `M_REPEAT, 4*10); // empty fifo

            // read readout
            #`QUERY_DELAY disp("Readout access");
            lpGBT_main.i2c_read_direct(`CHIP_ID_A, ADDR_READOUT, `M_REPEAT, 4*10); // empty fifo

            // read readout
            #`QUERY_DELAY disp("Snapshot access");
            lpGBT_main.i2c_read_direct(`CHIP_ID_A, ADDR_SNAPSHOT, `M_REPEAT, 2*10); // empty fifo

            // write entire shift register
            // MSB first, theoretically max 15 xxx
            for (int i = 0; i < CONFIG_BIT_TOTAL; i++)
                sin[i] = 1'($urandom());

            generate_shift_register_data(sin, $size(sin), data);
            #`QUERY_DELAY disp("Complete write on shift register");
            lpGBT_main.i2c_write(`CHIP_ID_A, ADDR_SHIFT_REGISTER, `M_REPEAT, data);

            // read on QConfig again (non-empty this time)
            #`QUERY_DELAY disp("Read QConfig again with data");
            lpGBT_main.i2c_read_direct(`CHIP_ID_A, ADDR_QCONF_START, `M_BURST, CONFIG_NUM_BYTES);

            #10000;
        endtask

        // ------------------------------------------------------------------- 
        // 
        // Local tasks
        //                  
        // -------------------------------------------------------------------

        `define SR_SIN  8'b0000_0001
        `define SR_SCK1 8'b0000_0010
        `define SR_SCK2 8'b0000_0100
        `define SR_LDA  8'b0000_1000
        `define SR_NONE 8'b0000_0000

        `define SR_SIN_VAL(sin) (`SR_SIN & {7'b000_0000, sin})
              

        // push single sin cycle into queue
        local static task generate_shift_register_sin (bit sin, ref logic [7:0] data [$]);
            data.push_back(`SR_SIN_VAL(sin));
            data.push_back(`SR_SIN_VAL(sin) | `SR_SCK1);
            data.push_back(`SR_SIN_VAL(sin));
            data.push_back(`SR_SIN_VAL(sin) | `SR_SCK2);
            data.push_back(`SR_SIN_VAL(sin));
        endtask

        // push lda into queue
        local static task generate_shift_register_lda (ref logic [7:0] data [$]);
            data.push_back(`SR_LDA);
            data.push_back(`SR_NONE);
        endtask

        // generate sck pattern and append load
        local static task generate_shift_register_data(bit sin [$], int data_length, ref logic [7:0] data [$]);
            // make sure data is empty
            while (data.size())
                data.pop_front();

            // xxx clean?
            foreach (sin[i])                                    
                generate_shift_register_sin(sin[i], data);

            generate_shift_register_lda(data);
        endtask
    endclass
`endif