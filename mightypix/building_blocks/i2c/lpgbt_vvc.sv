// lpgbt_vvc.sv
//
// Thomas Frei
//
// Simulates the lpGBT which provides the scl and sda line data. Allows
// read, write and direct transfers.
//
//
// Parameters:      Name        Description
// ---------------------------------------------------------------------
// Constructor:     i2c_if      Interface with I2C connections
// ---------------------------------------------------------------------

`ifndef LPGBT_VVC
    `define LPGBT_VVC

    // ------------------------------------------------------------------- 
    // 
    // Includes
    //                  
    // -------------------------------------------------------------------

    `include "i2c_global.sv"
    `include "i2c_interfaces.sv"
    `include "monitor_structures.sv"

    // ------------------------------------------------------------------- 
    // 
    // Definitions
    //                  
    // -------------------------------------------------------------------

    `define CLOCK_LPGBT     8                               // clocking speed lpGBT
    `define CLOCK_DIVIDER   40                              // divider for I2C speed
    `define CLOCK_I2C       (`CLOCK_LPGBT*`CLOCK_DIVIDER)   // clocking speed I2C
    `define STOP_DELAY      5                               // delay for stop condition

    // ------------------------------------------------------------------- 
    // 
    // Class
    //                  
    // -------------------------------------------------------------------

    class lpGBT;

        // external i2c interface
        local virtual i2c_wire_if i2c_if;

        // system clock
        local logic clk = 0;

        // ------------------------------------------------------------------- 
        // 
        // Functions
        //                  
        // -------------------------------------------------------------------
        
        // assign scl and sda lines
        function new (virtual i2c_wire_if i2c_if);
            this.i2c_if = i2c_if;
        endfunction

        // ------------------------------------------------------------------- 
        // 
        // Tasks
        //                  
        // -------------------------------------------------------------------

        // generate lpGBT system clock
        task run();
            forever  #`CLOCK_LPGBT clk <= ~clk;
        endtask

        // send set address transfer with given id, address and mode
        task i2c_set_address (input bit [2:0] chip_id, input logic [10:0] address, input bit mode);
            start_condition();

            set_address(chip_id, address, mode);

            stop_condition();
        endtask

        // send write transfer with given id, address, mode and data
        task i2c_write (input bit [2:0] chip_id, input logic [10:0] address, input bit mode, input logic[7:0] data [$]);
            reg [7:0] random_data;
            reg [10:0] reg_addr;

            // too many bytes per transfer
            if (data.size() > `LPGBT_MAX_BYTES-1)
                $warning("LPGBT WARNING:\t\t lpGBT only supports up to 16 bytes per transmission");

            // save address for increment
            reg_addr = address;

            start_condition();

            // provide clock for needed cycles
            fork
                provide_clk(`HEADER_LEN + data.size());
            join_none
            
            // send header
            send_data({chip_id, mode, reg_addr[10:8], `DIR_W});
            send_data(reg_addr[7:0]);

            // send data and adjust address until queue is empty
            while (data.size()) begin
                send_data(data.pop_front());
                reg_addr = reg_addr + mode;
            end

            // wait out last ACK pulse
            @ (negedge i2c_if.scl);

            stop_condition();
        endtask

        // send random write transfer with given id, address, mode and number of bytes
        task i2c_write_rand (input bit [2:0] chip_id, input logic [10:0] address, input bit mode, input integer bytes);
            logic [7:0] data [$];

            // too many bytes per transfer
            if (bytes > `LPGBT_MAX_BYTES-1)
                $warning("LPGBT WARNING:\t\t lpGBT only supports up to 16 bytes per transmission");

            repeat (bytes) 
                data.push_back($urandom & 8'hFF);

            i2c_write(chip_id, address, mode, data);
        endtask

        // send read transfer with given chip id and number of bytes
        task i2c_read (input bit [2:0] chip_id, input integer bytes);

            // too many bytes per transfer
            if (bytes > `LPGBT_MAX_BYTES)
                    $warning("LPGBT WARNING:\t\t lpGBT only supports up to 16 bytes per transmission");

            start_condition();

            // provide clock for needed cycles
            fork
                provide_clk(bytes + 1);
            join_none

            // send relevant header data (address irrelevant)
            send_data({chip_id, '1, 3'b111, `DIR_R});

            // acknowledge all data up until last transfer
            repeat (bytes-1) 
                read_data(`A_ACK);

            // finish transmission with nack
            read_data(`A_NACK);

            stop_condition();
        endtask

        // send direct read transfer with given id, address, mode and number of bytes
        task i2c_read_direct (input bit [2:0] chip_id, input logic [10:0] address, input bit mode, input integer bytes);

            start_condition();

            set_address(chip_id, address, mode);

            // initiate read
            i2c_read(chip_id, bytes);
        endtask

        // ------------------------------------------------------------------- 
        // 
        // Local tasks
        //                  
        // -------------------------------------------------------------------

        // generate scl clock for transmission of bytes
        local task provide_clk (integer bytes);
            repeat (bytes) begin
                repeat (`SIZE_I2C) begin
                    # `CLOCK_I2C i2c_if.scl_drv = 1'b0;
                    # `CLOCK_I2C i2c_if.scl_drv = 1'b1;
                end
            end

            // finish last pulse
            # `CLOCK_I2C i2c_if.scl_drv = 1'b0;
            # `CLOCK_I2C i2c_if.scl_drv = 1'b1;
        endtask

        // initiate start condition
        local task start_condition();
            // synchronise with system clock
            @ (posedge clk);
            i2c_if.sda_drv = 1'b1;
            
            // pull low and synchronise
            @ (posedge clk);
            i2c_if.sda_drv = 0;
            @ (posedge clk);
        endtask

        // initiate stop condition
        local task stop_condition();
            // force sda low
            i2c_if.sda_drv = 0;

            // at last edge
            if (~i2c_if.scl)
                @ (posedge i2c_if.scl);

            // wait some time
            repeat (`STOP_DELAY)
                @ (posedge clk);

            // remove forced low
            i2c_if.sda_drv = 1'b1;
        endtask

        // send address and mode for a given chip id
        local task set_address (input bit [2:0] chip_id, input logic [10:0] address, input bit mode);
            // provide clock for header
            fork
                provide_clk(`HEADER_LEN);
            join_none

            // send header
            send_data({chip_id, mode, address[10:8], `DIR_W});
            send_data(address[7:0]);

            // wait out last clk
            @ (posedge i2c_if.scl);
        endtask

        // sends given data each scl clock
        local task send_data (input reg [7:0] data);

            // for all bytes, send data on negedge
            for (integer i = 7; i >= 0; i = i-1) begin
                @ (negedge i2c_if.scl);
                i2c_if.sda_drv = data[i];
            end

            // negedge after 8th bit, remove data from line
            @ (negedge i2c_if.scl); 
            i2c_if.sda_drv = 1'b1;

            // wait for MightyPix response
            wait_on_ACK();
        endtask

        // read data from register, send given ACK
        local task read_data (input reg accept);
            // wait out transmission 
            repeat (`SIZE_BYTE) 
                @ (posedge i2c_if.scl);  

            // send ACK/NACK
            acknowledge(accept);
        endtask

        // wait for ACK of receiver
        local task wait_on_ACK();
            // ack on posedge
            @ (posedge i2c_if.scl);
        endtask

        // send ACK or NACK for one cycle
        local task acknowledge(input reg accept);
            @ (negedge i2c_if.scl);
            i2c_if.sda_drv = ~accept;
            @ (negedge i2c_if.scl);
            i2c_if.sda_drv = 1'b1;
        endtask        
    endclass
`endif