// monitor_structures.sv
//
// Thomas Frei
//
// Structures for various I2C testbench monitor modules. Classes are 
// required since the mailboxes only use references. New instances are  
// therefore needed for each mailbox entry.
//
// -----------------------------------------------------------------------

`ifndef MONITOR_STRUCTURES
    `define MONITOR_STRUCTURES

    // I2C transaction with all neccessary data
    class i2cTransaction;
        bit origin;
        logic [2:0] id;
        logic [10:0] address;
        logic direction;
        logic mode;
        logic [7:0] data[$];
    endclass

    // transaction from the MightyPix register
    class registerTransaction;
        logic [2:0] id;
        logic [10:0] address;
        logic direction;
        logic [7:0] data;
    endclass

    // transaction on the sda line
    class sdaTransaction;
        logic [7:0] data;
        logic ack;
    endclass
`endif