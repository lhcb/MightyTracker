// I2C_GLOBAL.sv
//
// Thomas Frei
//
// Global definitions for various I2C testbench modules.
//
// -----------------------------------------------------------------------

`ifndef I2C_GLOBAL
    `define I2C_GLOBAL
    
    // ------------------------------------------------------------------- 
    // 
    // Definitions
    //                  
    // -------------------------------------------------------------------

    `define USE_REGISTER

    // output level control
    `define INFO
    //`define DEBUG
    //`define VERBOSE

    // INFO > DEBUG > VERBOSE
    `ifdef VERBOSE
        `ifndef DEBUG
            `define DEBUG
        `endif 
    `endif

    `ifdef DEBUG
        `ifndef INFO
            `define INFO
        `endif
    `endif


    // broadcast ID
    `define ID_BROADCAST    1'b0

    // R/W flags
    `define DIR_R           1'b1
    `define DIR_W           1'b0

    // B/R flags
    `define M_BURST         1'b1
    `define M_REPEAT        1'b0
    
    // ACK flags
    `define A_ACK           1'b1
    `define A_NACK          1'b0

    // data origins
    `define ORIG_LPGBT      1'b0
    `define ORIG_MPIX       1'b1

    // address range
    `define ADDR_MIN        11'h0_00
    `define ADDR_MAX        11'h2_ff
    `define ADDR_RAND       11'($urandom_range(`ADDR_MIN, `ADDR_MAX))

    // header generation 
    `define LINE_SYM        "#"
    `define LINE_LEN        100 // even number
    `define LINE_CALC(str)  (`LINE_LEN - str.len())/2

    // i2c protocol definitions
    `define HEADER_LEN      2
    `define SIZE_BYTE       8
    `define SIZE_I2C        9

    // lpGBT definitions
    `define LPGBT_MAX_BYTES 16

    // ------------------------------------------------------------------- 
    // 
    // Tasks
    //                  
    // -------------------------------------------------------------------

    `ifdef SIMULATION
    // task to display terminal header
    task disp(string str);
        // variables
        string header;
        int len, str_len; 
        header = "";
        str = {" ", str, " "};
        len = `LINE_CALC(str);
        str_len = str.len();

        // construct header
        for (int i = 0; i < `LINE_LEN; i++) 
            header = {header, `LINE_SYM};

        // construct text, even
        for (int i = 0; i < len; i++) 
            str = {`LINE_SYM, str, `LINE_SYM}; 
        str = (str_len % 2)? {str,`LINE_SYM} : str;

        // display text
        $display("\n");
        $display("%s", header); 
        $display("%s", str); 
        $display("%s", header); 
    endtask
    `endif

`endif