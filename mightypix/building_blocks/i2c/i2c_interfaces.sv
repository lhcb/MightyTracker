// i2c_interfaces.sv
//
// Thomas Frei
//
// Interfaces for the communication between the lpGBT and the MightyPix.
//
// -----------------------------------------------------------------------

`ifndef I2C_INTERFACES
    `define I2C_INTERFACES

    // ------------------------------------------------------------------- 
    // 
    // Includes
    //                  
    // -------------------------------------------------------------------

    `include "i2c_global.sv"

    // ------------------------------------------------------------------- 
    // 
    // I2C wire interface
    //                  
    // -------------------------------------------------------------------

    // interface for i2c wires
    interface i2c_wire_if;
        wire scl;
        wire sda;

        `ifdef SIMULATION
            logic scl_drv = 1'b1;
            logic sda_drv = 1'b1;
        `else
            logic scl_drv;
            logic sda_drv;
        `endif

        assign (highz1, strong0) scl = scl_drv;
        assign (highz1, strong0) sda = sda_drv;
    endinterface

    // ------------------------------------------------------------------- 
    // 
    // Register interface
    //                  
    // -------------------------------------------------------------------

    `define SAMPLE_TIME 10ps // xxx maybe lower?

    // interface for MightyPix register
    interface i2c_reg_if #(parameter ADDR_BW = 11);
        // general
        logic register_valid;
        logic [(ADDR_BW-1):0] register_address; 
        
        // read if
        logic [7:0] register_read_data;
        logic register_read; 

        // write if
        logic [7:0] register_write_data; 
        logic register_write;

        `ifdef SIMULATION
        // save read data before negedge
        clocking read_cb @ (negedge register_read);
            default input #`SAMPLE_TIME;
            input register_address;
            input register_read_data;
            input register_valid;
        endclocking

        // save write data before negedge
        clocking write_cb @ (negedge register_write);
            default input #`SAMPLE_TIME;
            input register_address;
            input register_write_data;
            input register_valid;
        endclocking
        `endif

        // control from register interface
        modport control (
        input register_address, register_write_data, register_write, register_read,
        output register_read_data, register_valid
        );

        // output to i2c
        modport i2c (
        input register_valid, register_read_data,
        output register_address, register_read, register_write_data, register_write
        );

        // initial values if register not inserted
        `ifndef USE_REGISTER
            initial begin
                register_valid = '1;
                register_read_data = '0;
            end
        `endif
    endinterface

    // ------------------------------------------------------------------- 
    // 
    // Shift register interface
    //                  
    // -------------------------------------------------------------------

    `ifdef USE_REGISTER
        // interface for shift register wires
        interface shift_register_if;
            wire sin;
            wire ck1;
            wire ck2;
            wire ldA;
            wire ldB;
            wire ldC;
            wire ldD;
            wire ldE;

            // control from register interface
            modport control (
            output sin, ck1, ck2, ldA, ldB, ldC, ldD, ldE
            );

            // output to shift register
            modport out (
            input sin, ck1, ck2, ldA, ldB, ldC, ldD, ldE
            );
        endinterface


        interface addr_space_if #(parameter CONFIG_BIT_TOTAL = 40, parameter READOUT_WIDTH = 30) (
            shift_register_if shift_register_if_A
        );

        // ADDR_ENABLE
        logic use_i2c;
        logic use_tfc_from_i2c;
                    
        // ADDR_SHIFT_REGISTER
        // (in port list)
        

        // ADDR_TFC
        logic [7:0] tfc_from_i2c;
        logic tfc_from_i2c_ready;
                    
        // ADDR_STATE
        logic [7:0] fsm_state;
                    
        // ADDR_READOUT
        logic [(READOUT_WIDTH-1):0]  readout;
        logic fifo_empty;     
                    
        // ADDR_QCONF_START
        logic [(CONFIG_BIT_TOTAL-1):0] QConfig;
        
        //output wire rd,
        //input wire ptr_reset
        endinterface
    `endif
`endif