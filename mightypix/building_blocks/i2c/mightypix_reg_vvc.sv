// mightypix_reg_vvc.sv
//
// Thomas Frei
//
// Class handling the MightyPix register access. Reads random data
// and simulates a register write.
//
// Parameters:      Name        Description
// ------------------------------------------------------------------------------------------
// Constructor:     i2c_if      Interface with I2C connections
//                  reg_if      Interface with register connections
// ------------------------------------------------------------------------------------------

`ifndef MIGHTYPIX_REG_VVC
    `define MIGHTYPIX_REG_VVC

    // ------------------------------------------------------------------- 
    // 
    // Includes
    //                  
    // -------------------------------------------------------------------

    `include "address_space_pkg.sv"
    `include "i2c_global.sv"
    `include "i2c_interfaces.sv"

    import address_space_pkg::*;

    // ------------------------------------------------------------------- 
    // 
    // Definitions
    //                  
    // -------------------------------------------------------------------

    // address range dependent on whether register is inserted xxx obsolete now?
    `ifdef USE_REGISTER
        `define READ_COND   ((reg_if.register_address >= ADDR_QCONF_START && reg_if.register_address <= ADDR_QCONF_END) \
                            || (reg_if.register_address == ADDR_ENABLE) \
                            || (reg_if.register_address == ADDR_READOUT))
        `define WRITE_COND  ((reg_if.register_address == ADDR_SHIFT_REGISTER) \
                            || (reg_if.register_address == ADDR_TFC) \
                            || (reg_if.register_address == ADDR_ENABLE)) \
    `else
        `define READ_COND   reg_if.register_address <= `ADDR_MAX
        `define WRITE_COND  reg_if.register_address <= `ADDR_MAX
    `endif

    // ------------------------------------------------------------------- 
    // 
    // Class
    //                  
    // -------------------------------------------------------------------
    
    class MightyPix;

        // external interfaces
        local virtual i2c_wire_if i2c_if;
        local virtual i2c_reg_if reg_if;

        // ------------------------------------------------------------------- 
        // 
        // Functions
        //                  
        // -------------------------------------------------------------------
        
        // assign interfaces
        function new(virtual i2c_wire_if i2c_if, virtual i2c_reg_if reg_if);
            this.i2c_if = i2c_if;
            this.reg_if = reg_if;
        endfunction

        // ------------------------------------------------------------------- 
        // 
        // Tasks
        //                  
        // -------------------------------------------------------------------

        // run register tasks
        task run();
            fork
                send_data();
                receive_data();
            join_none 
        endtask

        // ------------------------------------------------------------------- 
        // 
        // Local tasks
        //                  
        // -------------------------------------------------------------------

        // send register data to lpGBT
        local task send_data();
            // when data requested
            forever begin
                // check register validity and set output data
                @ (posedge reg_if.register_read) begin
                    if (`READ_COND) begin
                        // create random register data
                        reg_if.register_read_data = $urandom;
                        reg_if.register_valid = '1;
                    end

                    else begin
                        reg_if.register_valid = '0;
                        reg_if.register_read_data = '0;
                    end
                end

                // reset register valid
                @ (negedge reg_if.register_read) 
                    reg_if.register_valid = '1;
            end
        endtask

        // receive register data from lpGBT
        local task receive_data();
            forever begin
                // check register validity
                @ (posedge reg_if.register_write) begin
                    if (`WRITE_COND) 
                        reg_if.register_valid = '1;

                    else
                        reg_if.register_valid = '0;
                end

                // reset register valid
                @ (negedge reg_if.register_write) 
                    reg_if.register_valid = '1;
            end
        endtask 
    endclass
`endif