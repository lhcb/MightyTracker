// address_space_pkg.sv
//
// Thomas Frei
//
// Package containing addresses for internal registers.
//
// -----------------------------------------------------------------------

`ifndef ADDRESS_SPACE_PKG
    `define ADDRESS_SPACE_PKG

    package address_space_pkg;
        // config registers count
        parameter CONFIG_BIT_EXTERNAL_COUNT = 16;
        parameter CONFIG_BIT_COUNT          = 103;
        parameter CONFIG_BIT_TOTAL          = (CONFIG_BIT_COUNT + CONFIG_BIT_EXTERNAL_COUNT);

        // number of bytes needed to store config bits
        parameter CONFIG_NUM_BYTES = $rtoi($ceil(CONFIG_BIT_TOTAL/8.0));

        // width of readout (maybe somewhere else xxx)
        parameter READOUT_WIDTH = 30;

        parameter SNAPSHOT_WIDTH = 16;

        // address space
        enum {  ADDR_ENABLE,
                ADDR_SHIFT_REGISTER,
                ADDR_TFC,
                ADDR_STATE,
                ADDR_READOUT,
                ADDR_SNAPSHOT,
                ADDR_QCONF_START,
                ADDR_QCONF_END = ADDR_QCONF_START + CONFIG_NUM_BYTES - 1,
                ADDR_INVALID} address_space;
    endpackage
`endif