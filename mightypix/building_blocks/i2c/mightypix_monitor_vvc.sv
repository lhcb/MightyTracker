// mightypix_monitor_vvc.sv
//
// Thomas Frei
//
// Monitors data on MightyPix registers and relays them to the provided scoreboard mailbox.
//
// Parameters:      Name                    Description
// -----------------------------------------------------------------------------------------
// Constructor:     i2c_if                  Interface with I2C connections
//                  reg_if                  Interface with register connections
//                  scoreboard_mailbox      Mailbox containing data for scoreboard
//                  id                      ID of monitored MightyPix chip
// ------------------------------------------------------------------------------------------

`ifndef MIGHTYPIX_MONITOR_VVC
    `define MIGHTYPIX_MONITOR_VVC

    // ------------------------------------------------------------------- 
    // 
    // Includes
    //                  
    // -------------------------------------------------------------------

    `include "address_space_pkg.sv"
    `include "i2c_global.sv"
    `include "i2c_interfaces.sv"
    `include "monitor_structures.sv"

    import address_space_pkg::*;

    // ------------------------------------------------------------------- 
    // 
    // Class
    //                  
    // -------------------------------------------------------------------

    class MightypixMonitor;
        parameter ADDR_BW = 11; /// yyy quick fix

        // typedefs for simpler declaration
        `ifdef USE_REGISTER
            typedef virtual addr_space_if #(.CONFIG_BIT_TOTAL(CONFIG_BIT_TOTAL), .READOUT_WIDTH(READOUT_WIDTH)) addr_space_if_type;
        `endif
        typedef virtual i2c_reg_if #(.ADDR_BW(ADDR_BW)) i2c_reg_if_type;

        // external interfaces
        local virtual i2c_wire_if i2c_if;
        local i2c_reg_if_type reg_if;
        `ifdef USE_REGISTER
            local addr_space_if_type addr_if;
        `endif

        // external mailbox
        local mailbox scoreboard_mailbox;

        // external variable
        local logic [2:0] id;

        // external event
        local event stopE;

        // extended wire sizes to match module
        parameter CONFIG_PADDED_SIZE = $rtoi($ceil(CONFIG_BIT_TOTAL/8.0) * 8);
        local logic [CONFIG_PADDED_SIZE-1:0] QConfig_padded;

        parameter READOUT_PADDED_SIZE = $rtoi($ceil(READOUT_WIDTH/8.0) * 8);
        local logic [READOUT_PADDED_SIZE-1:0] readout_padded;

        parameter READOUT_PTR_MAX = $rtoi($ceil(READOUT_WIDTH/8.0) - 1);

        // mailbox for moitor data
        local mailbox register_box = new();
        
        // read/write transaction
        local registerTransaction current_transaction;

        local bit last_read_register_was_valid;

        local bit send_fifo_empty_data;

        // ------------------------------------------------------------------- 
        // 
        // Functions
        //                  
        // -------------------------------------------------------------------

        `ifdef USE_REGISTER
            // assign interfaces, mailbox and variable
            function new (virtual i2c_wire_if i2c_if, i2c_reg_if_type reg_if, addr_space_if_type addr_if, mailbox scoreboard_mailbox, logic [2:0] id, event stopE);
                this.i2c_if = i2c_if;
                this.reg_if = reg_if;
                this.addr_if = addr_if;
                this.scoreboard_mailbox = scoreboard_mailbox;
                this.id = id;
                this.stopE = stopE;
            endfunction            
        `else
            function new (virtual i2c_wire_if i2c_if, virtual i2c_reg_if reg_if, mailbox scoreboard_mailbox, logic [2:0] id, event stopE);
                this.i2c_if = i2c_if;
                this.reg_if = reg_if;
                this.scoreboard_mailbox = scoreboard_mailbox;
                this.id = id;
                this.stopE = stopE;
            endfunction
        `endif

        // checks for needed removal of preemtively loaded register data from mailbox
        // since this data was never sent over the i2c line
        local function bit message_sent_over_i2c(registerTransaction transaction, integer num_of_messages);

            // only needed for last message
            if (num_of_messages == 0)
                // only read commands are affected by this
                if (transaction.direction == `DIR_R) 
                    // if the last read was valid, the data was not transmitted
                    if (last_read_register_was_valid)
                        return '0; 

            return '1;
        endfunction

        // ------------------------------------------------------------------- 
        // 
        // Tasks
        //                  
        // -------------------------------------------------------------------

        // run local tasks
        task run();
            fork
                send_data();
                receive_data();
                output_monitor();
            join_none
        endtask

        // ------------------------------------------------------------------- 
        // 
        // Local tasks
        //                  
        // -------------------------------------------------------------------

        // output and relay monitor data
        local task output_monitor();
            registerTransaction mailbox_transaction;

            // get data and relay if needed
            forever begin
                @ (stopE) begin
                    // for all gathered messages
                    while (register_box.num()) begin
                        mailbox_transaction = new();
                        register_box.get(mailbox_transaction);

                        `ifdef INFO
                            if (message_sent_over_i2c(mailbox_transaction, register_box.num()))
                                scoreboard_mailbox.put(mailbox_transaction);
                        `endif

                        `ifdef DEBUG
                            $display("REGISTER DEBUG:\t\t %p, Time: %0t ns", mailbox_transaction, $time/1000.0);
                        `endif
                    end
                end
            end
        endtask

        // send register data to lpGBT
        local task send_data();
            integer readout_ptr = READOUT_PTR_MAX;
            forever begin
                // correct data at read negedge
                @ (reg_if.read_cb) begin
                    if (reg_if.read_cb.register_valid) begin
                        // save send data in mailbox
                        current_transaction = new();
                        current_transaction.id = id;
                        current_transaction.address = reg_if.read_cb.register_address;
                        current_transaction.direction = `DIR_R;

                        `ifdef USE_REGISTER
                            // read qconfig, normalise address to 0 and read 1 byte (also use byte sized QConfig)
                            if (current_transaction.address >= ADDR_QCONF_START && current_transaction.address <= ADDR_QCONF_END) begin
                                QConfig_padded = {'0, addr_if.QConfig};
                                current_transaction.data = QConfig_padded[((current_transaction.address - ADDR_QCONF_START) * `SIZE_BYTE) +: `SIZE_BYTE];
                            end

                            //else if (current_transaction.address == ADDR_STATE)

                            // read use register
                            else if (current_transaction.address == ADDR_ENABLE)
                                current_transaction.data = {6'b00_0000, addr_if.use_tfc_from_i2c, addr_if.use_i2c};

                            // read readout
                            else if (current_transaction.address == ADDR_READOUT) begin
                                readout_padded = {'0, addr_if.readout};

                                // if FIFO filled, reset ptr and empty flag
                                if (~addr_if.fifo_empty & send_fifo_empty_data) begin
                                    readout_ptr = READOUT_PTR_MAX;
                                    send_fifo_empty_data = 0;
                                end

                                // same as in reg top
                                current_transaction.data = (send_fifo_empty_data)? 8'h00 : readout_padded[readout_ptr*`SIZE_BYTE +: `SIZE_BYTE];

                                send_fifo_empty_data = (readout_ptr == 0 & addr_if.fifo_empty)? '1 : '0;

                                readout_ptr = (readout_ptr != 0) ? readout_ptr - 1 :                   // non 0
                                                (~addr_if.fifo_empty)? READOUT_PTR_MAX : readout_ptr ;  // 0

                            end
                        `else
                            current_transaction.data = reg_if.read_cb.register_read_data;
                        `endif  

                        register_box.put(current_transaction);

                        last_read_register_was_valid = '1;
                    end

                    else 
                        last_read_register_was_valid = '0;

                end
            end
        endtask

        // receive register data from lpGBT
        local task receive_data();
            forever begin    
                // correct data at write negedge    
                @ (reg_if.write_cb) begin
                    if (reg_if.write_cb.register_valid) begin
                        // save receive data in mailbox
                        current_transaction = new();
                        current_transaction.id = id;
                        current_transaction.address = reg_if.write_cb.register_address;
                        current_transaction.data = reg_if.write_cb.register_write_data;
                        current_transaction.direction = `DIR_W;
                        register_box.put(current_transaction);
                    end
                end
            end
        endtask
    endclass
`endif