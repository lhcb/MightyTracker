// i2c_scoreboard_vvc.sv
//
// Thomas Frei
//
// Scoreboard comparing the values of the sda line with the register data of the 
// MightyPix.
//
// Parameters:      Name                    Description
// -----------------------------------------------------------------------------------------
// Constructor:     sda_mailbox             Mailbox containing data from sda line
//                  register_mailbox        Mailbox containing data from MightyPix registers
//                  stopE                   Stop event of I2C transmission
//                  restartE                Restart event of I2C transmission
// ------------------------------------------------------------------------------------------

`ifndef I2C_SCOREBORAD_VVC
    `define I2C_SCOREBORAD_VVC

    // ------------------------------------------------------------------- 
    // 
    // Includes
    //                  
    // -------------------------------------------------------------------

    `include "i2c_global.sv"
    `include "monitor_structures.sv"

    // ------------------------------------------------------------------- 
    // 
    // Defines
    //                  
    // -------------------------------------------------------------------

    `define REGISTER_OUTPUT_DELAY   2
    `define SCORING_DELAY           3

    // ------------------------------------------------------------------- 
    // 
    // Class
    //                  
    // -------------------------------------------------------------------

    class I2CScoreboard;

        // external mailboxes
        local mailbox sda_mailbox;
        local mailbox register_mailbox;

        // external events
        local event stopE;
        local event restartE;

        // local mailboxes
        local mailbox monitor_mailbox = new();
        local mailbox scoreboard_mailbox = new();

        // scoring event
        local event scoreE;

        // restart flag
        local bit restart_occured = 0;

        // ------------------------------------------------------------------- 
        // 
        // Functions
        //                  
        // -------------------------------------------------------------------
        
        // assign mailboxes and events
        function new(mailbox sda_mailbox, mailbox register_mailbox, event stopE, event restartE);
            this.sda_mailbox = sda_mailbox;
            this.register_mailbox = register_mailbox;
            this.stopE = stopE;
            this.restartE = restartE;
        endfunction

        // ------------------------------------------------------------------- 
        // 
        // Local functions
        //                  
        // -------------------------------------------------------------------

        // compare two values, 'x will not be treated as an error
        local function string compare(logic [15:0] a, logic [15:0] b, string msg);
            // verbose output
            `ifdef VERBOSE
                $display("SCOREBOARD_VERBOSE:\t %s: comparing %0h with %0h", msg, a, b);

                if (a + b === 'x) 
                    $display("SCOREBOARD_VERBOSE:\t ignoring %s due to unknown value", msg);
            `endif

            return (a != b)? {", ", msg} : "";
        endfunction

        // compare three values for at least one match, 'x will not be treated as an error
        local function string compare_either(logic [15:0] a, logic [15:0] b, logic [15:0] c, string msg);
            string ret;
            
            // try first compare
            ret = compare(a, b, msg);

            // first compare failed, try second (0 if equal)
            if (ret.compare(""))
                ret = compare(a, c, msg);

            return ret;
        endfunction

        // ------------------------------------------------------------------- 
        // 
        // Tasks
        //                  
        // -------------------------------------------------------------------

        // run tasks
        task run();
            fork
                prepare_i2c_data();
                prepare_register_data();
                catch_restart();
                output_and_relay();
                scoreboard();
            join_none
        endtask

        // ------------------------------------------------------------------- 
        // 
        // Local tasks
        //                  
        // -------------------------------------------------------------------

        // save if a restart occured during acquisition of data
        local task catch_restart();
            forever
                @ (restartE)
                    restart_occured = 1;
        endtask

        // prepare register monitor data
        local task prepare_register_data();
            registerTransaction register_transaction;
            i2cTransaction i2c_transaction;

            bit first_transaction;

            // hash maps
            bit id_in_communication [int];      // IDs in current communication
            logic [10:0] last_address [int];    // last appeared address of given ID

            // get data at stop event
            forever begin
                @ (stopE) begin
                    #`REGISTER_OUTPUT_DELAY; // write out delay (force data to appear after sda transaction)

                    first_transaction = 1;

                    // for all data left in mailbox
                    while (register_mailbox.num()) begin
                        i2c_transaction  = new();

                        register_mailbox.get(register_transaction);

                        // copy all data, predict mode according to addresses
                        i2c_transaction.id = register_transaction.id;
                        i2c_transaction.address = register_transaction.address;
                        i2c_transaction.direction = register_transaction.direction;
                        i2c_transaction.mode = (first_transaction && register_transaction.direction == `DIR_W)? 'x                      // first write transaction -> unknown
                                                : (register_transaction.address == last_address[register_transaction.id])? `M_REPEAT    // same address -> repeat
                                                : (register_transaction.address == last_address[register_transaction.id] + 1)? `M_BURST // incremented address -> burst
                                                : 'x;                                                                                   // address unknown -> unknown
                        i2c_transaction.data.push_back(register_transaction.data);
                        i2c_transaction.origin = `ORIG_MPIX;

                        // save last address for current ID
                        last_address[register_transaction.id] = register_transaction.address;

                        // log ID in communication
                        id_in_communication[i2c_transaction.id] = 1'b1;

                        monitor_mailbox.put(i2c_transaction);

                        first_transaction = 0;
                    end

                    // clear ID entries
                    foreach (id_in_communication[id])
                        id_in_communication.delete(id);
                end
            end
        endtask

        // prepare register monitor data
        local task prepare_i2c_data();
            sdaTransaction sda_transaction = new();
            i2cTransaction i2c_transaction = new();

            // get data at stop event
            forever begin
                @ (stopE) begin
                    i2c_transaction = new();

                    sda_mailbox.get(sda_transaction);

                    // extract header data
                    {i2c_transaction.id, i2c_transaction.mode, i2c_transaction.address[10:8], i2c_transaction.direction} = sda_transaction.data;

                    // extract additional header data for write command
                    if (i2c_transaction.direction == `DIR_W) begin
                        sda_mailbox.get(sda_transaction);
                        i2c_transaction.address[7:0] = sda_transaction.data;

                        // direct command
                        if (restart_occured) begin
                            restart_occured = 0;

                            // submit current transaction and create
                            // separate for read part of direct transaction
                            i2c_transaction.origin = `ORIG_LPGBT;
                            monitor_mailbox.put(i2c_transaction);

                            i2c_transaction = new();

                            // construct read header
                            sda_mailbox.get(sda_transaction);
                            {i2c_transaction.id, i2c_transaction.mode, i2c_transaction.address[10:8], i2c_transaction.direction} = sda_transaction.data;
                        end
                    end

                    // mark unknowns for read command
                    else begin
                        i2c_transaction.address = 'x;
                        i2c_transaction.mode = 'x;
                    end

                    // rest of transaction only consists of data
                    for (integer i = 0; sda_mailbox.num(); i++) begin
                        sda_mailbox.get(sda_transaction);
                        i2c_transaction.data.push_back(sda_transaction.data);
                    end

                    // submit transaction
                    i2c_transaction.origin = `ORIG_LPGBT;
                    monitor_mailbox.put(i2c_transaction);

                    // trigger event with short delay for scoring
                    `ifdef INFO
                        #`SCORING_DELAY ->scoreE;
                    `endif
                end
            end        
        endtask

        // output and relay i2c data
        local task output_and_relay();
            i2cTransaction i2c_transaction = new();
            string display_output;

            forever begin
                monitor_mailbox.get(i2c_transaction);

                // relay message if scoring is enabled
                `ifdef INFO
                    scoreboard_mailbox.put(i2c_transaction);
                `endif                    

                // show debug messages
                `ifdef DEBUG
                    $display("SCOREBOARD DEBUG:\t %p, Time: %0t ns", i2c_transaction, $time/1000.0);
                `endif   
            end
        endtask

        // scoring of mailbox results between i2c and registers
        local task scoreboard();
            // transactions
            i2cTransaction main_transaction = new();            // transaction from lpGBT
            i2cTransaction direct_transaction = new();          // additional (direct) transaction from lpGBT 
            i2cTransaction current_transaction = new();         // current transaction from MightyPix registers
            i2cTransaction temp_transaction = new();            // temporary transaction for transaction manipulation
            i2cTransaction id_separated_transaction [int][$];   // separate transaction per ID (for broadcast)

            // local variables
            bit direct;
            string msg;
            logic [10:0] main_address;
            bit first_comparison;
            int num_of_messages;

            // hash maps
            logic [10:0] last_main_address [int];   // last address of given ID
            bit last_main_mode [int];               // last mode of given ID

            forever begin
                // received transfer
                @ (scoreE) begin
                    // prepare error message
                    msg = "";
                    
                    // get main transaction
                    scoreboard_mailbox.get(main_transaction);

                    // second message as response found
                    if (scoreboard_mailbox.num()) begin
                        // check for direct read
                        scoreboard_mailbox.peek(temp_transaction);

                        // if second message from lpGBT, a direct transaction occured
                        if (temp_transaction.origin == `ORIG_LPGBT) 
                            scoreboard_mailbox.get(direct_transaction);

                        // set flag
                        direct = (temp_transaction.origin == `ORIG_LPGBT)? 1 : 0;

                        // seperate messages by ID for broadband messages
                        for (integer i = 0; scoreboard_mailbox.num(); i++) begin
                            temp_transaction = new();
                            scoreboard_mailbox.get(temp_transaction);

                            id_separated_transaction[temp_transaction.id].push_back(temp_transaction);
                        end

                        // for each ID in broadcast
                        foreach (id_separated_transaction[id]) begin
                            // main address: if read use last stored (no address transmitted), else the one transmitted
                            main_address = (main_transaction.direction == `DIR_R)? last_main_address[id] : main_transaction.address;

                            // one shot message compare
                            first_comparison = 1;

                            // for each message in current ID
                            foreach (id_separated_transaction[id][msg_nr]) begin
                                current_transaction = new();
                                current_transaction = id_separated_transaction[id].pop_front();

                                // compare number of messages
                                if (first_comparison) begin
                                    num_of_messages = (direct)? direct_transaction.data.size() : main_transaction.data.size();
                                    
                                    msg = {msg, compare(num_of_messages, id_separated_transaction[id].size()+1, "Number of Messages")};
                                end
                                    
                                // compare ID, address and mode for all transfers
                                msg = {msg,
                                    compare_either(main_transaction.id, current_transaction.id, `ID_BROADCAST, "ID"),
                                    compare(main_address + (current_transaction.mode * msg_nr), current_transaction.address, "Address"),
                                    compare(main_transaction.mode, current_transaction.mode, "Mode")};

                                
                                // compare second ID, both directions and data for direct transfer
                                if (direct)
                                    msg = {msg, 
                                        compare(main_transaction.direction, `DIR_W, "Read"),
                                        compare_either(direct_transaction.id, current_transaction.id, `ID_BROADCAST, "Direct ID"),
                                        compare(direct_transaction.direction, current_transaction.direction, "Read"),
                                        compare(direct_transaction.data[msg_nr], current_transaction.data.pop_front(), "Data")};
                                

                                // compare direction and data for general transfers
                                else 
                                    msg = {msg, 
                                        compare(main_transaction.direction, current_transaction.direction, "Direction"),
                                        compare(main_transaction.data[msg_nr], current_transaction.data.pop_front(), "Data")};

                                // remove one shot message compare
                                first_comparison = 0;
                            end

                            // update upcoming address if correct ID received
                            if (main_transaction.id == id || main_transaction.id == `ID_BROADCAST) begin
                                // store mode, only changed in write commands
                                if (main_transaction.direction == `DIR_W)
                                    last_main_mode[id] = main_transaction.mode;

                                // store upcoming address 
                                last_main_address[id] = current_transaction.address + last_main_mode[id];
                            end
                        end
                    end

                    // no response for transaction (wrong ID or illegal address)
                    else begin
                        msg = {msg, ", ID or Address"};

                        // quick fix yyy

                        // write commands still update address
                        if (main_transaction.direction == `DIR_W) begin
                            // for each ID in broadcast
                            foreach (last_main_address[id]) begin
                                // update upcoming address if correct ID received
                                if (main_transaction.id == id || main_transaction.id == `ID_BROADCAST) begin
                                    // store mode
                                    last_main_mode[id] = main_transaction.mode;

                                    // store upcoming address
                                    last_main_address[id] = main_transaction.address + last_main_mode[id]*main_transaction.data.size();
                                end
                            end
                        end
                        
                    end

                    // error message generated (0 if equal)
                    if (msg.compare("")) begin
                        // remove leading comma from string
                        msg = msg.substr(2, msg.len() - 1);

                        $display("SCOREBOARD INFO:\t Invalid %s for ID: %h, Address: %h, Time: %0t", msg, main_transaction.id, main_transaction.address, $time/1000.0);
                    end
                        
                    // successful transaction
                    else
                        $display("SCOREBOARD INFO:\t Successful communication at Time: %0t", $time/1000.0);
                end
            end
        endtask
    endclass
`endif