`ifndef shr_vvc
    `define shr_vvc

    `include "shr_interfaces.sv"

    class shr_segment;
        logic [31:0] value;
        integer size = 1;

        function new(integer isize, logic [31:0] v);
            value = v;
            size  = isize;
        endfunction
    endclass

    /**
     * External Driver
     * @author rleys
     *
     */
    class shr_driver;

        real time_delay = 3000ns;

        shr_segment segments[$];

        function new();
        endfunction

        /**
            Creates a new segment and saves it to the local list of segment.
            Later the shift register can be driven by sending the saved list of segments

            Example:
                automatic mysegment = driver.add_segment(6,6'h10);
        */
        function shr_segment add_segment(integer bits, logic [31:0] value);
            shr_segment segment = new (bits,value);
            segments.push_back(segment);
            return segment;
        endfunction 

        task init(virtual SHR_INTERFACE shr_if);
            shr_if.SIN      = 0;
            shr_if.CK1       = 0 ;
            shr_if.CK2       = 0;
            shr_if.LOAD      = 0;
            shr_if.READ_BACK = 0;
        endtask

        task print_length();
            integer length = 0; 

            // Calculate length
            for (int i=0; i<  this.segments.size(); i++)
            begin
               length = length + this.segments[i].size;
            end

            $display("Shift Register has %d bits",length);
        endtask

        task shift_bits(virtual SHR_INTERFACE shr_if, input bit [64:0] bits, input int size);
            $display("SR Bits: %d",size);

            for (int i=0; i< size; i++)
            begin
                shr_if.SIN            <= bits[i];
                #time_delay shr_if.CK1 <= 1;
                #time_delay shr_if.CK1 <= 0;
                #time_delay shr_if.CK2 <= 1;
                #time_delay shr_if.CK2 <= 0;
            end
            #time_delay shr_if.LOAD <= 1;
            #time_delay shr_if.LOAD <= 0;


        endtask

        task shift_local_segments(virtual SHR_INTERFACE shr_if);
            shift_segments(shr_if,this.segments);
        endtask

        /**
         * Use this task as natural programming: First bits in the SR on the left of the assignment (first in the list), this task will reverse to first output the last bits
         * @param segments -
         */
        task shift_segments(virtual SHR_INTERFACE shr_if,input shr_segment segments[$]);
            
            
            shr_segment current;
            segments.reverse();
            
            $display("Driving SHR, whith %d segments",segments.size());
            init(shr_if);
            
            while (segments.size()>0)
            begin
                current = segments.pop_front();

                //shr.enabled = 1'b1;
                for (int i=0; i< current.size; i++)
                begin
                    shr_if.SIN            <= current.value[current.size-1-i];
                    #time_delay shr_if.CK1 <= 1;
                    #time_delay shr_if.CK1 <= 0;
                    #time_delay shr_if.CK2 <= 1;
                    #time_delay shr_if.CK2 <= 0;
                end


            end

            #time_delay shr_if.LOAD <= 1;
            #time_delay shr_if.LOAD <= 0;


        //shr.enabled   = 1'b0;



        endtask

        task shift_zeros(virtual SHR_INTERFACE shr_if,input int count);

            //shr.enabled = 1'b1;
            for (int i=0; i< count; i++)
            begin
                shr_if.SIN            <= 0;
                #time_delay shr_if.CK1 <= 1;
                #time_delay shr_if.CK1 <= 0;
                #time_delay shr_if.CK2 <= 1;
                #time_delay shr_if.CK2 <= 0;
            end
            #time_delay shr_if.LOAD <= 1;
            #time_delay shr_if.LOAD <= 0;
        //shr.enabled   = 1'b0;



        endtask

    endclass

`endif
