
`ifndef SHR_INTERFACES
    `define SHR_INTERFACES



    interface SHR_INTERFACE;

        logic CK1;
        logic CK2;
        logic SIN;
        logic SOUT;
        logic LOAD;
        logic READ_BACK;

        logic enabled;

        modport driver(output CK1,output CK2, input SIN, output SOUT,output LOAD,output READ_BACK);
        modport driver_only(output CK1,output CK2, output SOUT,output LOAD,output READ_BACK);
        
        modport driver_control(output CK1,output CK2,output LOAD,output READ_BACK);

        modport driven(input CK1,input CK2,output SIN,input SOUT,input READ_BACK,input LOAD);

        modport external(input CK1,input CK2, input SIN,output SOUT,input READ_BACK,input LOAD);




        modport written(input CK1,input CK2,input SIN,input READ_BACK,input LOAD,output SOUT);
        modport writter(output CK1,output CK2,output SOUT,output READ_BACK,output LOAD,input SIN);
        modport writter_receiver(input CK1,input CK2,input SOUT,input READ_BACK,input LOAD,output SIN);
        

    endinterface

    `ifdef SIMULATION
        interface SHR_INTERFACE_SIM;
            SHR_INTERFACE shr();

            // Tasks for Simulation


            task init();
                shr.SIN       <= 0;
                shr.CK1       <= 0 ;
                shr.CK2       <= 0;
                shr.LOAD      <= 0;
                shr.READ_BACK <=0;
            endtask

            task shift_bits(input bit [64:0] bits, input int size);
                $display("SR Bits: %d",size);
                shr.enabled = 1'b1;
                for (int i=0; i< size; i++)
                begin
                    shr.SIN      = bits[i];
                    #100 shr.CK1 = 1;
                    #100 shr.CK1 = 0;
                    #100 shr.CK2 = 1;
                    #100 shr.CK2 = 0;
                end
                #150 shr.LOAD = 1;
                #150 shr.LOAD = 0;
                shr.enabled   = 1'b0;

            endtask




        endinterface
    `endif


`endif
