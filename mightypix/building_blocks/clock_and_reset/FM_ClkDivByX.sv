`timescale 1ns/1ps

/**
 * DIV_BY Parameter must be multiple of 2
 */
module FM_clkDivByX #(parameter DIV_BY=4) (
        input  wire clk_in,
        input  wire res_n,
        output wire clk_out

    );


    localparam BITS = DIV_BY / 2;
    reg [BITS-1:0] clk_out_reg;
    assign clk_out = clk_out_reg[BITS-1];

    always @ (posedge clk_in or negedge res_n)
    begin
        if (~res_n)
        begin
            clk_out_reg = {(BITS){1'b0}};
        end
        else
        begin
            clk_out_reg = {clk_out_reg[BITS-2:0],!clk_out_reg[BITS-1]};
        end
    end

endmodule
