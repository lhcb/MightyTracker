`timescale 1ns/1ps
module FM_ClkGen_MHz_ctrl #(parameter MHZ = 100,parameter OFFSET=0)(
        output logic clk,
        input  wire start
    );


// Half the period in ns
    parameter delay   = ((1.0 / (MHZ*1000000)) * 1000000000 )/ 2.0;
    logic     started = 0;
    //parameter offset_delay = delay * OFFSET_RATIO;
    initial
    begin
        clk             = 1'b0;
        #OFFSET started = 1'b1;
    end

    always
    begin
        #delay wait(started==1);
        if (start)
        begin
             clk = !clk;
        end
       
    end

endmodule

