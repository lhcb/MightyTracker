`timescale 1ns/1ps
module FM_ClkGen_MHz_Aligned_To_Clock #(parameter MHZ = 100,parameter OFFSET_RATIO=0)(
        output logic clk,input wire ref_clk
    );


// Half the period in ns
    parameter delay        = ((1.0 / (MHZ*1000000)) * 1000000000 )/ 2.0;
    parameter offset_delay = delay * OFFSET_RATIO;
    initial
    begin
        #offset_delay clk = 1'b0;
    end

    always
    begin
        #delay @(posedge ref_clk); 
        clk <= ~clk;
    end

endmodule

