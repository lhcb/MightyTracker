`timescale 1ns/1ps

module FM_ClkGen(
        clk_out
    );

    output clk_out;

    reg clk_out;

// Half the period
    parameter delay = 1;

    always #delay clk_out <= ~clk_out;
    initial
    begin
        clk_out = 1'b0;
    end
endmodule