`timescale 1ns/1ps

module FM_clkDivBy2(
			clk_in,
			clk_out,
			res_n
			);

input			clk_in;
input			res_n;
output			clk_out;

reg			clk_out;

always @ (posedge clk_in or negedge res_n)
    if (~res_n) clk_out <= 1'b0;
    else clk_out <= ~clk_out;

endmodule
