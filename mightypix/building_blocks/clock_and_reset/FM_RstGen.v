`timescale 1ns/1ps
//Reset active high
module FM_RstGen(
			clk_in,
			rst_n
			);

 input			clk_in;
 output			rst_n;
 reg			rst_n;

task doResetNclkCycles;
 input [31:0]	nClkCycles;
 integer	count;

 begin
  @(posedge clk_in)
  enableReset;
  for (count = 0; count < nClkCycles; count = count +1)
   begin
    @(posedge clk_in)
    #0.005;
   end
  disableReset;
 end
endtask

task enableReset;
 begin
  #0.005 rst_n = 1'b1;
 end
endtask

task disableReset;
 begin
  #0.005 rst_n = 1'b0;
 end
endtask

initial
 begin
  rst_n = 1'b0;
 end

endmodule
