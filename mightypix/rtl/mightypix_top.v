
`timescale 1ns/1ps


/*
    Top Level Module for MightyPix
*/

module mightypix_top #(
    // Number of Extra Shift Register bits placed on the side of this module
    parameter   CONFIG_BIT_EXTERNAL_COUNT = 16,
    parameter   CONFIG_BIT_COUNT = 103,

    // I2C params
    parameter   I2C_ID_BW = 3,
    parameter   I2C_ADDR_BW = 11,

    // Serializer
    parameter   USE_CLOCKGEN = 1,

    //Fsm
    parameter SYNCWORD = {2'b10, 30'h3F555555},

    //Syncpattern
    parameter SYNCPATTERN = 18'h2AAAA
    ) (

    // Clocks and Reset
    //----------------------

    // Fast clock for serialiser
    input wire clk_40M, // 40Mhz clock from lpGBT
    input wire clk_320M, // 320 Mhz clock from lpGBT or maybe internal PLL
    input wire clk_640M, // use to be clk_800p,

    output wire clkOut_40M, //PLL Feedback
    output wire clkOut_320M,
    output wire clk40_from_TFC, //PLL Reference
    

    // 
    //output wire clk_8n,
    //output wire clk_40_from_clockgen,

    // Resets can be connected together in simulation
    input wire   Ser_res_n,
    input wire   RO_res_n,
    input wire   TSGen_res_n,
    input wire   TFCalign_res_n,
    input wire   i2c_res_n,

    // Matrix Interface
    //--------------

    //-- Data from the matrix
    //-- RL 23/08 -> Modified definition to match atlaspix3 matrix style, this has now better chances of being the right interface
    input wire PriFromDet,//scan out - if one there is a hit in eocs
    input wire [8:0]    RowAddFromDet, // - rows
    input wire [7:0]    ColAddFromDet, // - columns
    input wire [6:0]    AmpFromDet,    // 
    input wire [11:0]   TSFromDet,     // time stamp 12 bits
    input wire [11:0]   NegTSFromDet,
    input wire [6:0]    TDCFromDet,    // TDC Timestamp, now 7 bits
    input wire [2:0]    FineTSFromDet, //

    // TS Counters to matrix
    output wire [11:0]  TSToDet,         // time stamp
    output wire [11:0]  NegTSToDet,         // time stamp
    output wire [2:0]   FineTSToDet,     // time stamp for tot
    output wire [6:0]   TSToDetTDC,

    // Readout to the matrix
    output wire LdCol,
    output wire RdCol,
    output wire LdPix,
    output wire PullDN,

    //TFC in
    input wire tfc_in,

    // TS clock
    // Input TS Clock for external clock test
    //input wire clk_timestamp_in,

    // Serialiser out
    //------------

    // The two bits from serialiser out are muxed DDR outside of the Digital part and passed to the LVDS output buffer
    output wire [1:0] d_out,
    output wire [1:0] DataOut_slow,
    output wire DataOut_slowest,


    // Shift Registers
    //---------------------

    // Shift Register coming from external pads
    input wire SIn_Ext,
    input wire Ck1_Ext,
    input wire Ck2_Ext,
    input wire Ld_Ext1,
    input wire Ld_Ext2,
    input wire Ld_Ext3,
    input wire Ld_Ext4,
    input wire Ld_Ext5,

    // Readback + Sout for every separated Shift Register
    input  wire Readback_Ext,
    output wire SOut_Ext, // This SOUT is the output of the selected  Readback

    output wire SR_CK1_out,
    output wire SR_CK2_out,
    output wire SR_SIn_out,

    // Loads
    // These are from atlas pix, Mightypix will probably need those separated loads too for EOC Ram and Configs
    // Ld_Internal_Ext2
    output wire Ld_Internal_Ext1,//4
    output wire Ld_Internal_Ext2,//4
    output wire Ld_Internal_Ext3,//5
    output wire Ld_Internal_Ext4,//6
    output wire Ld_Internal_Ext5,//7  
    //output wire LdRow,//8
    //output wire LdColumn,//9

    input wire WrRAM_in0,//new//0
    input wire WrRAM_in1,//new//1
    input wire WrRAM_in2,//new//2
    input wire WrRAM_in3,//new//3

    // RAM writes in Pixel (maybe will dissapear)
    // Rich 10/01/22 -> commented out for now
    output wire WrRAM_out0,//new//0
    output wire WrRAM_out1,//new//1
    output wire WrRAM_out2,//new//2
    output wire WrRAM_out3,//new//3

    // Extra Config Bits
    output wire [CONFIG_BIT_EXTERNAL_COUNT-1:0] SRExtraBits,
    output wire [CONFIG_BIT_EXTERNAL_COUNT-1:0] SRExtraBitsN,

    // I2C
    //----------------
    inout wire i2c_sda,
    inout wire i2c_scl,
    input wire [(I2C_ID_BW-1):0] i2c_chip_id,

    input wire use_I2C_extern, //for SR config
    input wire use_I2C_tfc_extern,

    //Debug out
    output wire TSclkdel
);

    wire [3:0] timer;
    wire [2:0] Cnt8;


    wire SIn_Conf;
    wire Load_Conf;
    wire Ck1_Conf;
    wire Ck2_Conf;


    wire [29:0] DataOut_A;
    wire dataoff_A;
    wire datavalid_A;

    wire [31:0] DataOutToSer;

    wire Scr_en;
    wire syncRes;

    wire Injection_SC; 

    //output wire [7:0] QConfigOut

    // I2C Internal Connections
    //----------------

    wire i2c_clk = clk_40M;

    wire [7:0] register_read_data;
    wire register_read_data_ready;
    wire register_valid;

    wire [I2C_ADDR_BW-1:0] register_address;
    wire [7:0] register_write_data; 
    wire register_write;
    wire register_read;

    wire [29:0] fifo_data_to_i2c;
    wire [15:0] snapshot_to_i2c;

    //I2C driven SR wires
    wire use_I2C;

    //assign use_I2C = 1'b0;

    wire i2c_nack;

    wire SIn_I2C;
    wire Ck1_I2C;
    wire Ck2_I2C;
    wire Ld_I2C1;
    wire Ld_I2C2;
    wire Ld_I2C3;
    wire Ld_I2C4;
    wire Ld_I2C5;


    //TFC
    //----------------
    wire use_I2C_tfc;
    wire use_I2C_for_tfc;

    wire [7:0] i2c_tfc_data;
    wire i2c_tfc_ready;

    wire cmd_mux;
    wire cmd_cal;
    wire cmd_tsgen;

    wire TFC_Ser_res_n;
    wire TFC_RO_res_n;
    wire TFC_I2C_res_n;

    assign use_I2C_tfc = use_I2C_tfc_extern ? use_I2C_for_tfc : 0;


    //TSGen
    wire snapshot;
    wire bxreset;

    wire [15:0] BinCounter;
    wire [15:0] BCcnt_snapshot;


    // Shift Register Config Bits, internal for Digital Part
    //----------------

    //-- I/Os for shift register: MUX of U/O sources: external or I2C, Load outputs etc...
   
    // interal signals are the actual Shift register signals, after source selection
    // Right now only external, add I2C or others here


    wire SR_CK1_Internal  = use_I2C_extern ? (use_I2C ? Ck1_I2C : Ck1_Ext) : Ck1_Ext;
    wire SR_CK2_Internal  = use_I2C_extern ? (use_I2C ? Ck2_I2C : Ck2_Ext) : Ck2_Ext;
    wire SR_SIn_Internal  = use_I2C_extern ? (use_I2C ? SIn_I2C : SIn_Ext) : SIn_Ext;
    wire SR_Ld_Internal   = use_I2C_extern ? (use_I2C ? Ld_I2C1 : Ld_Ext1) : Ld_Ext1;
    wire SR_Ld_Internal2  = use_I2C_extern ? (use_I2C ? Ld_I2C2 : Ld_Ext2) : Ld_Ext2;
    wire SR_Ld_Internal3  = use_I2C_extern ? (use_I2C ? Ld_I2C3 : Ld_Ext3) : Ld_Ext3;
    wire SR_Ld_Internal4  = use_I2C_extern ? (use_I2C ? Ld_I2C4 : Ld_Ext4) : Ld_Ext4;
    wire SR_Ld_Internal5  = use_I2C_extern ? (use_I2C ? Ld_I2C5 : Ld_Ext5) : Ld_Ext5;

    assign SR_CK1_out = SR_CK1_Internal;
    assign SR_CK2_out = SR_CK2_Internal;
    assign SR_SIn_out = SR_SIn_Internal;


    assign Ld_Internal_Ext   = SR_Ld_Internal;    
    assign Ld_Internal_Ext2   = SR_Ld_Internal2;
    assign Ld_Internal_Ext3   = SR_Ld_Internal3;
    assign Ld_Internal_Ext4   = SR_Ld_Internal4;
    assign Ld_Internal_Ext5   = SR_Ld_Internal5;

    //WrRAM
    assign WrRAM_out0   = WrRAM_in0;    
    assign WrRAM_out1   = WrRAM_in1;
    assign WrRAM_out2   = WrRAM_in2;
    assign WrRAM_out3   = WrRAM_in3;

    //-- Bits definitions for Shift Register
    //-----------------

    //localparam CONFIG_BIT_COUNT = 46;
    localparam CONFIG_BIT_TOTAL = CONFIG_BIT_COUNT + CONFIG_BIT_EXTERNAL_COUNT;

    wire [(CONFIG_BIT_TOTAL-1):0] QConfig;
    wire [(CONFIG_BIT_TOTAL-1):0] QConfigN;

    // External
    // Spare Extra configs written out to I/O
    assign SRExtraBits  = QConfig  [CONFIG_BIT_TOTAL-1 -:  CONFIG_BIT_EXTERNAL_COUNT];
    assign SRExtraBitsN = QConfigN [CONFIG_BIT_TOTAL-1 -:  CONFIG_BIT_EXTERNAL_COUNT];
    
    wire [CONFIG_BIT_TOTAL:0] SInConfig;//58 -> 90
    assign SInConfig[0] = SR_SIn_Internal ; // Sin is the first bit of Sin

    // Internal Sout, this is the last bit of the internal Shift Register
    wire SOut_int = SInConfig[CONFIG_BIT_TOTAL];

    // Parameters from Shift Register assigned to wires for easy reading 
    wire [5:0] Ickdivend            = QConfig[5:0];
    wire [5:0] Ickdivend2           = QConfig[11:6];
    wire [3:0] Itimerend            = QConfig[15:12];
    wire [3:0] Islowdownend         = QConfig[19:16];
    wire [5:0] Imaxcycend           = QConfig[25:20];
    
    wire [3:0] sr_empty             = QConfig[27:26];

    wire [3:0] Iresetckdivend       = QConfig[31:28];
    wire Isendcounter               = QConfig[32];
    wire [5:0] Itsphase             = QConfig[38:33]; 
    wire EnSync_SC                  = QConfig[39];
    wire [4:0] IslowdownLdColEnd    = QConfig[44:40];
    wire countsheeps                = QConfig[45];
    wire [29:0] syncpattern         = QConfig[75:46];
    wire [3:0] bxresetval           = QConfig[79:76];

    //TDC calculation
    wire [6:0] ts_tdc_min           = QConfig[86:80];
    wire [6:0] ts_tdc_max           = QConfig[93:87];

    wire [5:0] Ickdivend3           = QConfig[99:94];
    wire IenclkgenB                 = QConfigN[100];
    wire Iphase                     = QConfig[101];
    wire Ien_ScrB                   = QConfigN[102];
    wire Ien_tscalc                 = QConfigN[103];

    //-- Shift Register Impl.
    //-- Connect Single SR Bits behing each other
    generate 

        genvar j;

        for (j=0; j<CONFIG_BIT_TOTAL; j=j+1) begin: GenConfigBit
        
            ConfigBit ConfigBit_I(  
                .Ck1(SR_CK1_Internal),
                .Ck2(SR_CK2_Internal),
                .Ld(SR_Ld_Internal),
                .SIn(SInConfig[j]),
                .RB(Readback_Ext),
                .SOut(SInConfig[j+1]),
                .Q(QConfig[j]),
                .QB(QConfigN[j])
            );
        
        end: GenConfigBit
    endgenerate


    //Reset
    wire Ser_res_n_int  = Ser_res_n ? TFC_Ser_res_n : 1'b0;
    wire RO_res_n_int   = RO_res_n  ? TFC_RO_res_n  : 1'b0;
    wire i2c_res_n_int  = i2c_res_n ? TFC_I2C_res_n : 1'b0;

    /*
    Buffer Buffer_I(
        
        
        .TSFromDet_PartA_int(TSFromDet_PartA_int),//new
        .TSFromDet_PartA(TSFromDet_PartA),
        .TSFromDet_PartB_int(TSFromDet_PartB_int),//new
        .TSFromDet_PartB(TSFromDet_PartB),
        .TSFromDet_PartC_int(TSFromDet_PartC_int),//new
        .TSFromDet_PartC(TSFromDet_PartC),
        
        .RowAddFromDet_PartA_int(RowAddFromDet_PartA_int),
        .RowAddFromDet_PartA(RowAddFromDet_PartA),
        
        .ColAddFromDet_PartA_int(ColAddFromDet_PartA_int),
        .ColAddFromDet_PartA(ColAddFromDet_PartA),
        
        .RowAddFromDet_PartB_int(RowAddFromDet_PartB_int),
        .RowAddFromDet_PartB(RowAddFromDet_PartB),
        
        .ColAddFromDet_PartB_int(ColAddFromDet_PartB_int),
        .ColAddFromDet_PartB(ColAddFromDet_PartB),
        
        .RowAddFromDet_PartC_int(RowAddFromDet_PartC_int),
        .RowAddFromDet_PartC(RowAddFromDet_PartC),
        
        .ColAddFromDet_PartC_int(ColAddFromDet_PartC_int),
        .ColAddFromDet_PartC(ColAddFromDet_PartC),
        
        .RdCol_PartA(RdCol_PartA),
        
        .RdCol_PartB(RdCol_PartB),
        
        .RdCol_PartC(RdCol_PartC)

    );*/

    tfc_decoder_top tfc_decoder_top_I (
        .clk_in(clkOut_320M),//(clk_320M),
        .clk_40M(clk_40M),

        .tfc_in(tfc_in),

        .res_n(RO_res_n),
        .align_res_n(TFCalign_res_n),

        .Cnt8(Cnt8),

        //Lock input, ignores new commands while high
        .tfc_lock(1'b0),

        //watchdog timeout reset
        .wd_reset(),

        //Decoded CMD out
        .cmd_mux(cmd_mux),
        .cmd_cal(cmd_cal),
        .cmd_tsgen({snapshot,bxreset}),

        .serializer_res_n(TFC_Ser_res_n),
        .readout_res_n(TFC_RO_res_n),
        .i2c_res_n(TFC_I2C_res_n),

        //Interrupt out
        .cmd_received(),

        .tfc_from_i2c(i2c_tfc_data),
        .tfc_from_i2c_ready(i2c_tfc_ready),
        .use_tfc_from_i2c(use_I2C_tfc)
    );


    MightyPixTSGen MightyPixTSGen_I(
        .clk_640M(clk_640M),
        .clk_320M(clkOut_320M),//(clk_320M),
        .clk_40M(clk_40M),
        .res_n(TSGen_res_n),

        .ckdivend(Ickdivend),
        .ckdivend2(Ickdivend2),
        .ckdivend3(Ickdivend3),
        .tsphase(Itsphase),
        .TSToDet(TSToDet),
        .NegTSToDet(NegTSToDet),
        .TSToDet2(FineTSToDet),
        .TSToDetTDC(TSToDetTDC),
        .BinCounterOut(BinCounter),
        .BCcnt_snapshot(BCcnt_snapshot),
        .Cnt8(Cnt8),

        .bxreset(bxreset),
        .bxresetval(bxresetval),
        .snapshot(snapshot),
        .snapshot_ready(snapshot_ready)
    );

	
    //Add clkdiv counter output
    mightypix_readout_fsm #(.SYNCWORD(SYNCWORD))
    readout_fsm (
        .clk(clkOut_320M),//(clk_320M),
        .res_n(RO_res_n_int),	 
        .Fixedpattern(8'hAA),
        .RowAddFromDet(RowAddFromDet),
        .ColAddFromDet(ColAddFromDet),

        .TSFromDet(TSFromDet),
        .NegTSFromDet(NegTSFromDet),
        .FineTSFromDet(FineTSFromDet),
        .TDCFromDet(TDCFromDet),
        .BinCounter(BinCounter),
        .PriFromDet(PriFromDet),	 
        .LdCol(LdCol),
        .RdCol(RdCol),
        .LdPix(LdPix),
        .PullDN(PullDN),
        .timerend(Itimerend),//divider for Readout
        .slowdownend(Islowdownend),//wait in ldpix if empty
        .maxcycend(Imaxcycend),//max read cyc
        .resetckdivend(Iresetckdivend), //divider for reset (sync) clk
        .sendcounter(Isendcounter), // send only the timestamp counter 
        .DataOut(DataOut_A),//important data valid only 4 x 4ns

        .datavalid(datavalid_A),
        .dataoff(dataoff_A),
        .slowdownLdColEnd(IslowdownLdColEnd),
        .countsheeps(countsheeps),
        .timer_out(timer),

        .AmpFromDet(AmpFromDet),

        .Cnt8(Cnt8)
    );

    
    MightypixMux MightypixMux_I (
        .clk(clkOut_320M),//(clk_320M),

        .Cnt8(Cnt8),
        .res_n(RO_res_n_int),

        .syncpattern(syncpattern),

        .WordFromDetector(DataOut_A),

        .cmd_decoded(cmd_mux),

        .BXIDfromTSGen(BinCounter[11:0]),

        .datawordOut(DataOutToSer),

        .Scr_en(),

        .timer(timer[1:0])
    );

    SerializerTop #(.USE_PLL(USE_CLOCKGEN))
    serialiser (
        //clk in
        .clkIn_40M(clk_40M),
        .clkIn_320M(clk_320M),
        .clkIn_640M(clk_640M),

        //clk out
        .clkOut_320M(clkOut_320M),
        .clkOut_40M(clkOut_40M), //FB Clk to pll

        //Data in
        .DataIn_A(DataOutToSer),//(DataOut_A),
        .dataoff_A(dataoff_A),
        .datavalid_A(datavalid_A),

        //timer clkdivider from Readout FSM in
        .timer(timer[1:0]),

        //switch between fast, slow and slowest, based on Readout FSM divider
        .speed_mode(Itimerend[1:0]),

        //Scrambler enable in
        .Scr_en(Ien_ScrB),

        //Data out
        .BitDataOut_A(d_out),

        .DataOut_slow(DataOut_slow),
        .DataOut_slowest(DataOut_slowest),

        //Reset in
        .Scr_res_n(Ser_res_n_int),
        .Ser_res_n(Ser_res_n_int),

        //Phasecounter out
        .Cnt8(Cnt8),

        //Enable 320M from Clockgen
        .enclkGen(IenclkgenB),

        //Set DFF phase
        .phase(Iphase)

    );

    //FSM to I2C FIFO
    async_fifo #(.WIDTH(32),.PTR_WIDTH(3),.FALLTROUGH(0))
    async_fifo_I(

        // Write
        .data_in(DataOut_A),
        .wr((timer == 0) && (Cnt8==0)),
        .wr_clk(clk_320M),

        // Read
        .data_out(fifo_data_to_i2c),
        .rd(fifo_read_from_i2c),
        .rd_clk(i2c_clk),

        // Flags
        .full(readout_fifo_full),
        .empty(readout_fifo_empty),

        .wr_res_n(RO_res_n_int),
        .rd_res_n(i2c_res_n_int)

    );

    //Snapshot FIFO
    async_fifo #(.WIDTH(16),.PTR_WIDTH(2),.FALLTROUGH(0))
    async_fifo_snapshot(

        // Write
        .data_in(BCcnt_snapshot),
        .wr(snapshot_ready),
        .wr_clk(clk_320M),

        // Read
        .data_out(snapshot_to_i2c),
        .rd(snapshot_read_from_i2c),
        .rd_clk(i2c_clk),

        // Flags
        .full(snapshot_fifo_full),
        .empty(snapshot_fifo_empty),

        .wr_res_n(RO_res_n_int),
        .rd_res_n(i2c_res_n_int)
    );

    // I2C
    //-------------------
    i2c_slave_top #(.ID_BW(I2C_ID_BW), .ADDR_BW(I2C_ADDR_BW))
    i2c_master_I(
        // operation
        .clk(i2c_clk),
        .res_n(i2c_res_n_int),

        // inouts
        .scl(i2c_scl),
        .sda(i2c_sda),

        // iputs
        .chip_id(i2c_chip_id),

        // outputs
        .register_address(register_address),
        .register_valid(register_valid),

        // read if
        .register_read_data(register_read_data),
        .register_read(register_read),

        // write if
        .register_write_data(register_write_data),
        .register_write(register_write),

        .nack(i2c_nack)
    );

    register_interface_top #(.ADDR_BW(I2C_ADDR_BW))
    register_interface_A (
        // operation
        .clk(clk_40M),
        .res_n(i2c_res_n_int),

        // outputs
        .register_address(register_address),
        .register_valid(register_valid),

        // read if
        .register_read_data(register_read_data),
        .register_read(register_read),
        
        // write if
        .register_write_data(register_write_data),
        .register_write(register_write),

        // shift regsiter
        .QConfig(QConfig),
        .fsm_state(),


        .sin(SIn_I2C),
        .ck1(Ck1_I2C),
        .ck2(Ck2_I2C),
        .ldA(Ld_I2C1),
        .ldB(Ld_I2C2),
        .ldC(Ld_I2C3),
        .ldD(Ld_I2C4),
        .ldE(Ld_I2C5),

        //use i2c for SR config
        .use_i2c(use_I2C),

        //use tfc from i2c reg
        .use_tfc_from_i2c(use_I2C_for_tfc),

        .tfc_from_i2c(i2c_tfc_data),
        .tfc_from_i2c_ready(i2c_tfc_ready),

        //FSM Readout
        .fifo_empty(readout_fifo_empty),
        .rd(fifo_read_from_i2c),
        .readout(fifo_data_to_i2c),
        .ptr_reset(i2c_nack),

        //Snapshot readout
        .snapshot_fifo_empty(snapshot_fifo_empty),
        .snapshot_rd(snapshot_read_from_i2c),
        .snapshot(snapshot_to_i2c),
        .snapshot_ptr_reset(i2c_nack)
    );

endmodule
