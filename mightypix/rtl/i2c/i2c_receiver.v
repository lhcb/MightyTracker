// i2c_receiver.v
//
// Thomas Frei
//
// Receive i2c data over sda wire. Module is clocked by scl wire and therefore only 
// runs while an i2c transfer is in progress (maybe add watchdog?). Additional testing 
// needed.
//
// Module ports:    Name        Description
// -------------------------------------------------------------------
// Operation:       res_n               Reset on negative clock edge 
//                                      (asynchronous)
//
// Inouts:          scl                 Serial clock of I2C bus
//                  sda                 Serial data line of I2C bus
//
// Inputs:          receive_request     Data receive initiation by master
//                  ack                 Acknowledge reception
//
// Outputs:         receive_complete    Data transfer of 1 byte complete
//                  data[7:0]           Received data from I2C transfer
//                  
// -------------------------------------------------------------------

`timescale 1ns/1ps

module i2c_receiver(
    // operation
    input wire res_n,

    // i2c
    inout wire scl,
    inout wire sda,

    // inputs
    input wire receive_request,
    input wire ack,

    // outputs
    output wire receive_complete,
    output reg [7:0] data
);

// state machine
enum reg [1:0] {REC_IDLE,       // wait for receive request
                REC_RECEIVE,    // receive 1 byte of data
                REC_ACK         // ack data and notify master
                } state;

// ------------------------------------------------------------------- 
// 
// internals
//                  
// ------------------------------------------------------------------- 

// counter to track data
reg [2:0] data_cntr;
wire data_cntr_full;
assign data_cntr_full = data_cntr[2] & data_cntr[1] & data_cntr[0]; // III

// driver for sda line
reg sda_reg;
assign (highz1, strong0) sda = sda_reg;

// low if either res_n goes low or both request and ready are low
wire res_n_op;
assign res_n_op = (receive_request | receive_complete) & res_n;

// receiver ready when sending ack
assign receive_complete = (state == REC_ACK); //~sda_reg; xxx check

// ------------------------------------------------------------------- 
// 
// state machine
//                  
// ------------------------------------------------------------------- 

// reciever state machine
        //data <= 8'h00; xxx can I remove this?
always @(posedge scl or negedge res_n_op) begin
    // reset state machine
    if (!res_n_op) 
        state <= REC_IDLE;
    

    else begin
        // state machine transitions
        case(state)
            REC_IDLE    : state <= (receive_request)? REC_RECEIVE : state;  // request received
            REC_RECEIVE : state <= (data_cntr_full)? REC_ACK : state;       // full byte in shift register
            REC_ACK     : state <= (!receive_request)? REC_IDLE : state;    // request lifted (xxx or after 1 cycle)
            default     : state <= REC_IDLE;
        endcase

        // state machine data processing
        case(state)
            REC_IDLE    :   begin
                                // save first received bit
                                data[0] <= sda;
                                data_cntr <= 1;   
                            end

            REC_RECEIVE :   begin
                                // push data into shift register
                                data <= {data[6:0], sda};
                                data_cntr <= data_cntr+1;
                            end

            REC_ACK     :   ;// nothing to process here
        endcase
    end
end

// pull sda low on negedge during ack phase
always @(negedge scl or negedge res_n) begin
    if (!res_n)
        sda_reg <= 1;

    else
        sda_reg <= (state == REC_ACK)? ~ack : 1;
end

endmodule