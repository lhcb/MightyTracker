// i2c_slave_top.v
//
// Thomas Frei
//
// Modellation of the slave functionality for the I2C bus interface. Allows the
// reception of read and write commands, as well as multiple commands in 
// succession. Supports a mixed mode operation to switch from send to read
// command without the need of stopping the ongoing data transfer.        
//
// Module ports:    Name                            Description
// ------------------------------------------------------------------------------------
// Operation:       clk                             Operating clock of module
//                  res_n                           Reset on negative clock edge 
//                                                  (asynchronous)
//
// Inouts:          scl                             Serial clock of I2C bus
//                  sda                             Serial data line of I2C bus
//
// Inputs:          chip_id[(ID_BW-1):0]            Identifier of this chip 
//                  register_valid                  Signals if register access is valid
//                  register_read_data[7:0]         Read data from accessed address
//                  
//
// Outputs:         nack                            Data not acknowledged during send    
//                  register_address[(ADDR_BW-1):0] Register address for access
//                  register_read                   Request of reading data from register
//                  register_write_data[7:0]        Data to be written to address
//                  register_write                  Request of writing data to register         
// -------------------------------------------------------------------------------------
// Parameters:      ID_BW                           Number of ID bits, 1 < N < 7
//                  ADDR_BW                         Number of address bits, 0 < N
// -------------------------------------------------------------------------------------

`timescale 1ns/1ps

module i2c_slave_top
    #(parameter ID_BW = 3, parameter ADDR_BW = 10) // 1 < ID < 7, 0 < ADDR
    (
    // operation
    input wire clk,
    input wire res_n,

    // i2c
    inout wire scl,
    inout wire sda,

    // inputs
    input wire [(ID_BW-1):0] chip_id,

    // outputs
    output wire nack,

    // r/w interface
    input wire register_valid,
    output reg [(ADDR_BW-1):0] register_address, 
    
    // read if
    input wire [7:0] register_read_data,
    output reg register_read, 

    // write if
    output reg [7:0] register_write_data,
    output reg register_write
);

// state machine
enum reg [1:0] {I2C_IDLE,       // wait for data
                I2C_HEADER,     // receive header
                I2C_READ,       // process read command
                I2C_WRITE       // process write command
                } state;

// ------------------------------------------------------------------- 
// 
// internals
//                  
// ------------------------------------------------------------------- 

// header length, total number of bytes (ceiled)
// ID + r/w bit + b/r bit + address (+7 because integer floors value)
parameter integer HEADER_LEN = ((ID_BW + 1 + 1 + ADDR_BW + 7) / 8); 

// buffer for all header data
reg [7:0] header_buffer [(HEADER_LEN-1):0];
reg [$clog2(HEADER_LEN+1):0] header_cntr;

// id and rw access in header buffer
wire [(ID_BW-1):0] id;
wire rw;
assign id = header_buffer[0][7:(7 - ID_BW+1)];
assign rw = header_buffer[0][0];

// address and burst acces in header buffer
wire [(ADDR_BW-1):0] addr;
wire br;

// xxx test this thoroughly
// generate circuit for parametrised extraction
// of the transmitted register address

// b/r bit is always preceding the msb of the address
// the address is always directly succeeding the ID, unless smaller than 8 bit
// the last 8 bits of the address will always be right aligned
generate
    genvar k;

    // if b/r bit in first byte
    if (ID_BW < 7) begin
        assign br = header_buffer[0][(7-ID_BW)];

        // if address data in first byte
        if (ID_BW < 6)
            assign addr[(ADDR_BW-1):((ADDR_BW-1) - (ID_BW-1))] = header_buffer[0][(7-ID_BW-1):1];
    end
        
    // assign address bytes, either full 8 bytes or rest
    for (k = ADDR_BW - (8 - ID_BW-1 - 1); k > 0; k = k-8) begin
        if (k >= 8)
            assign addr[k-1:k-8] = header_buffer[ADDR_BW/8 - (k - 1)/8]; // each full byte
        else
            assign addr[k-1:0] = header_buffer[ADDR_BW/8 - (k - 1)/8][k:0]; // remaining addres bits
    end
endgenerate


reg burst_en;




// transfer data
wire [7:0] data_in;

// transfer completion flags
wire send_complete;
wire receive_complete;

// request flags
reg receive_request;
reg send_request;


// send flag
reg send_in_progress;

// id reception
reg id_received;
wire id_correct;
wire correct_id_received;
wire incorrect_id_received; // yyy addition
assign id_correct = ((id == chip_id) | id == 0);
assign correct_id_received = id_received & id_correct;
assign incorrect_id_received = ~correct_id_received & (header_cntr == 1); // yyy

// header reception flag
wire header_received;
assign header_received = (header_cntr == HEADER_LEN);

// acknowledge flag
// ack if correct header and if register valid; only in necessary states
wire ack;
// assign ack = (id_correct & state == I2C_HEADER) | (register_valid & state != I2C_HEADER);
assign ack = id_correct & register_valid;

// scl and sda history for edge detection
// detects start and stop condition
reg [1:0] scl_hist;
reg [1:0] sda_hist;

wire scl_stable;
wire sda_negedge;
wire sda_posedge;
assign scl_stable = scl_hist[1] & scl_hist[0]; // scl 11
assign sda_negedge = sda_hist[1] & ~sda_hist[0]; // sda 10 (negedge)
assign sda_posedge = ~sda_hist[1] & sda_hist[0]; // sda 01 (posedge)

wire start_condition;
//wire stop_condition; yyy
assign start_condition = scl_stable & sda_negedge;


// ------------------------------------------------------------------- 
// 
// modules
//                  
// ------------------------------------------------------------------- 

// i2c receiver
i2c_receiver i2c_receiverA(
    // operation
    .res_n(res_n),

    // i2c wires
    .scl(scl),
    .sda(sda),

    // inputs
    .receive_request(receive_request),
    .ack(ack),

    // outputs
    .receive_complete(receive_complete),
    .data(data_in)
);

// i2c_sender
i2c_sender i2c_senderA(
    // operation
    .res_n(res_n),

    // i2c wires
    .scl(scl),
    .sda(sda),

    // inputs
    .send_request(send_request),
    .data(register_read_data),

    // outputs
    .send_complete(send_complete),
    .nack(nack)
);

// xxx put back later                                        // yyy added this
assign stop_condition = (scl_stable & sda_posedge) | nack | incorrect_id_received; // | ~register_valid; // also stop at nack or invalid register

// ------------------------------------------------------------------- 
// 
// simulation specific assignments
//                  
// ------------------------------------------------------------------- 

`ifdef SIMULATION

    // counter for reset
    reg [$clog2(ADDR_BW):0] init_cntr;
    initial begin
        // reset header buffer
        for(init_cntr = 0; init_cntr < HEADER_LEN; init_cntr=init_cntr+1) 
            header_buffer[init_cntr] <= 0;
    
        // reset address buffer
        for(init_cntr = 0; init_cntr < ADDR_BW; init_cntr=init_cntr+1)
            register_address[init_cntr] <= 0;
    end
`endif

// ------------------------------------------------------------------- 
// 
// state machine
//                  
// ------------------------------------------------------------------- 

reg[$clog2(ADDR_BW):0] addr_cntr;

// master state machine
always @(posedge clk or negedge res_n) begin
    // reset state machine registers
    if (!res_n)
        state <= I2C_IDLE;

    else begin
        // state machine transitions
        case(state)
            I2C_IDLE    : state <= (start_condition)? I2C_HEADER : state;   // start condition
            I2C_HEADER  : state <= (stop_condition)? I2C_IDLE               // transmission aborted yyy addition
                                    : (!correct_id_received)? state         // receive rest of header
                                    : (rw)? I2C_READ                        // read initiated
                                    : (header_received)? I2C_WRITE          // write initiated
                                    : state;                                // receive rest of address
            I2C_READ    : state <= (stop_condition)? I2C_IDLE : state;      // stop condition
            I2C_WRITE   : state <= (stop_condition)? I2C_IDLE               // stop condition 
                                    : (start_condition)? I2C_HEADER         // restart
                                    : state;                                // stay in write
            default     : state <= I2C_IDLE;                                // error mitigation
        endcase

        // state machine data processing
        case(state)
            I2C_IDLE    :   begin
                                // keep requests low
                                receive_request <= 0;
                                send_request <= 0;
                                register_write <= 0;

                                // reset header flags
                                id_received <= 0;
                                header_cntr <= 0;

                                // reset ready flag
                                register_read <= 0;

                                send_in_progress <= 0; // remove leftover send
                            end

            I2C_HEADER  :   begin
                                // data requested and ready
                                if (receive_request && receive_complete) begin 
                                    // remove request and save data 
                                    receive_request <= 0;

                                    header_buffer[header_cntr] <= data_in;
                                    header_cntr <= header_cntr + 1;

                                    // first byte of header with id
                                    id_received <= 1;                                    
                                end 

                                // waiting for data, set/keep request enabled
                                else if(~receive_complete)             
                                    receive_request <= 1;

                            end
            I2C_READ    :   begin
                                // top data ready to send
                                if (send_in_progress) begin
                                    // at start of send, remove read
                                    // start sender 
                                    if (~send_complete) begin
                                        register_read <= 0;
                                        send_request <= 1;
                                    end

                                    // save data after send completed
                                    // (once -> block during register read) 
                                    if (send_complete && ~register_read) begin

                                        // remove progress
                                        send_in_progress <= 0; 

                                        // increment address if burst enabled
                                        register_address <= register_address + burst_en; // xxx what if out of bounds?
                                    end
                                end 

                                else begin
                                    // 2. initiate send (only after header reception complete)
                                    if (register_read & ~receive_complete)
                                        send_in_progress <= 1;

                                    // 1. request data from top
                                    else 
                                        register_read <= 1;
                                    
                                end
                            end
            I2C_WRITE   :   begin
                                // new address received
                                // save address, burst enable
                                if (correct_id_received) begin
                                    register_address <= addr;
                                    burst_en <= br;
                                    id_received <= 0;
                                end
                                
                                // data requested and ready
                                if (receive_request && receive_complete) begin
                                    // remove request and save data 
                                    receive_request <= 0;

                                    register_write_data <= data_in;
                                    register_write <= 1;
                                end

                                // waiting for data, set/keep request enabled
                                else if(~receive_complete) begin
                                    receive_request <= 1;

                                    // one shot reg addr increment
                                    if (register_write) begin
                                        register_write <= 0;
                                        // increment address if burst enabled
                                        register_address <= register_address + burst_en; // wrong place? xxx too soon
                                    end
                                end


                                // repeat idle assignments
                                // for mixed mode if restart initiated
                                if (start_condition) begin
                                    // keep requests low
                                    receive_request <= 0;
                                    send_request <= 0;
                                    register_write <= 0;

                                    // reset header flags
                                    id_received <= 0;
                                    header_cntr <= 0;
                                end
                                    
                            end
        endcase

        // keep history on scl and sda
        scl_hist <= {scl_hist[0], scl};
        sda_hist <= {sda_hist[0], sda};
    end
end
endmodule