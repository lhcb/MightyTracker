// i2c_slave_top.v
//
// Thomas Frei
//
// This module handles the read and write requests sent over the I2C line. According
// to the register address it checks for the validity of the request and either writes
// the corresponding register or returns the requested read value. 
//
// Module ports:    Name                            Description
// ------------------------------------------------------------------------------------
// Operation:       clk                             Operating clock of module
//                  res_n                           Reset on negative clock edge 
//                                                  (asynchronous)
//
// Inputs:          register_address[(ADDR_BW-1):0] Register address for access
//                  register_read                   Request of reading data from register
//                  register_write_data[7:0]        Data to be written to address
//                  register_write                  Request of writing data to register
//                  QConfig[(CONFIG_BIT_TOTAL-1):0] Data in QConfig register
//                  readout[(READOUT_WIDTH-1):0]    Readout data
//                  fifo_empty                      Flag for empty FIFO
//                  ptr_reset                       Reset signal for readout pointer
//                  fsm_state[7:0]                  Readout state machine status
//   
// Outputs:         register_valid                  Signals if register access is valid
//                  register_read_data[7:0]         Read data from accessed address    
//                  use_i2c                         Set if I2C enabled
//                  tfc_from_i2c[7:0]               TFC data from I2C transfer
//                  tfc_from_i2c_ready              Signals if TFC data is ready
//                  use_tfc_from_i2c                Set if TFC over I2C enabled
//                  sin                             Serial input of shift register
//                  ck1                             Clock 1 of shift register
//                  ck2                             Clock 2 of shift register
//                  ldA                             Load A of shift register
//                  ldB                             Load B of shift register
//                  ldC                             Load C of shift register
//                  ldD                             Load D of shift register
//                  ldE                             Load E of shift register
//                          
// -------------------------------------------------------------------------------------
// Parameters:      ADDR_BW                         Number of address bits, 0 < N
//                  READOUT_WIDTH                   Width of readout input
// -------------------------------------------------------------------------------------

`timescale 1ns/1ps

`include "address_space_pkg.sv"
`include "i2c_interfaces.sv"

module register_interface_top 
      import address_space_pkg::*;
      #(parameter ADDR_BW = 10, 
        parameter READOUT_WIDTH = 30)
      (
      // operation
      input wire clk,
      input wire res_n,

      // general
      output reg register_valid,
      input wire [(ADDR_BW-1):0] register_address, 
      
      // read if
      output reg [7:0] register_read_data,
      input wire register_read, 

      // write if
      input wire [7:0] register_write_data,
      input wire register_write,

      // QConfig
      input wire [(CONFIG_BIT_TOTAL-1):0] QConfig,

      // readout fifo
      input wire [(READOUT_WIDTH-1):0]  readout,
      input wire fifo_empty,
      input wire ptr_reset,
      output wire rd,

      // snapshot fifo
      input wire [(SNAPSHOT_WIDTH-1):0]  snapshot,
      input wire snapshot_fifo_empty,
      input wire snapshot_ptr_reset,
      output wire snapshot_rd,
      
      // state machine
      input wire [7:0] fsm_state,

      // i2c flag
      output wire use_i2c,

      // tfc over i2c
      output reg [7:0] tfc_from_i2c,
      output reg tfc_from_i2c_ready,
      output wire use_tfc_from_i2c,

      // shift register
      output wire sin,
      output wire ck1,
      output wire ck2,
      output wire ldA,
      output wire ldB,
      output wire ldC,
      output wire ldD,
      output wire ldE
);

// ------------------------------------------------------------------- 
// 
// parameters
//                  
// ------------------------------------------------------------------- 

localparam CONFIG_PADDED_SIZE = CONFIG_NUM_BYTES * 8;
localparam CONFIG_PADDING_SIZE = (CONFIG_PADDED_SIZE - CONFIG_BIT_TOTAL);

localparam READOUT_PADDED_SIZE = $rtoi($ceil(READOUT_WIDTH/8.0)) * 8; // check if clog simpler xxx
localparam READOUT_PADDING_SIZE = (READOUT_PADDED_SIZE - READOUT_WIDTH);

localparam FIFO_EMPTY_DATA = 8'h00;

// ------------------------------------------------------------------- 
// 
// internals
//                  
// ------------------------------------------------------------------- 

// QConfig
wire [7:0] QConfig_byte;

// QConfig mux
ranged_mux #(.SIZE_IN($bits(QConfig)), .SIZE_OUT($bits(QConfig_byte)), .FROM(ADDR_QCONF_START)) mux_QConfig(
    // select
    .select(register_address[3:0]), // match size xxx

    // inout
    .in(QConfig),
    .out(QConfig_byte)
);

// shift register
reg [7:0] shift_register_data;
assign {ldE, ldD, ldC, ldB, ldA, ck2, ck1, sin} = shift_register_data;        // sin LSB
//assign {sin, ck1, ck2, ldA, ldB, ldC, ldD, ldE} = shift_register_data;      // sin MSB

// use register
reg [7:0] use_register;
assign {use_tfc_from_i2c, use_i2c} = use_register[1:0];

// readout

wire [7:0] readout_byte;
localparam READOUT_PTR_MAX = $rtoi($ceil(READOUT_WIDTH/8.0) - 1);
reg [($clog2(READOUT_PTR_MAX)-1):0] readout_ptr; // xxx check if correct

//snapshot
wire [7:0] snapshot_byte;
localparam SNAPSHOT_PTR_MAX = 1;
reg snapshot_ptr;

// readout mux
ranged_mux #(.SIZE_IN($bits(readout)), .SIZE_OUT($bits(readout_byte)), .FROM(0)) mux_readout(
    // select
    .select(readout_ptr),

    // inout
    .in(readout),
    .out(readout_byte)
);

ranged_mux #(.SIZE_IN($bits(snapshot)), .SIZE_OUT($bits(snapshot_byte)), .FROM(0)) mux_snapshot(
    // select
    .select(snapshot_ptr),

    // inout
    .in(snapshot),
    .out(snapshot_byte)
);

// ------------------------------------------------------------------- 
// 
// state machine
//                  
// ------------------------------------------------------------------- 

reg decr_ptr;
reg snaphost_decr_ptr;
reg rd_disable, snapshot_rd_disable;

reg send_fifo_empty_data;
reg send_snapshot_fifo_empty_data;

assign rd = register_read & (readout_ptr == 3) & decr_ptr & ~rd_disable & ~fifo_empty; // has to be a better way xxx
assign snapshot_rd = register_read & (snapshot_ptr == 1) & snaphost_decr_ptr & ~snapshot_rd_disable & ~snapshot_fifo_empty; // has to be a better way xxx

// module main
always @(posedge clk, negedge res_n) begin

      // output registers
      if (!res_n) begin
            // xxx are they needed?
            shift_register_data <= 8'h00; // probably
            tfc_from_i2c <= 8'h00;            // unlikely
            use_register <= 8'h00;        // yes
            readout_ptr <= 0;    // yes
            snapshot_ptr <= 0;
            decr_ptr <= '0;
            snaphost_decr_ptr <= 0;
            rd_disable <= '0;
            snapshot_rd_disable <= '0;
            send_fifo_empty_data <= 0;
            send_snapshot_fifo_empty_data <= 0;
      end

      else begin
            // write requested output data into register
            if (register_read) begin
                  if (register_address >= ADDR_QCONF_START && register_address <= ADDR_QCONF_END)
                        register_read_data <= QConfig_byte;                   
                  
                 /* else if (register_address == ADDR_STATE)
                        register_read_data <= fsm_state;*/
                  
                  else if (register_address == ADDR_ENABLE)
                        register_read_data <= use_register;   

                  else if (register_address == ADDR_READOUT) begin
                  
                        // send either empty filler data or readout byte
                        register_read_data <= (send_fifo_empty_data)? FIFO_EMPTY_DATA : readout_byte;

                        // one shot
                        if (decr_ptr == '0) begin
                              // enable one shot
                              decr_ptr <= '1;

                              // determine if fifo is empty and filler data has to be sent
                              send_fifo_empty_data <= (readout_ptr == 0 & fifo_empty)? '1 : '0;

                              // decrement pointer, only reset if non-empty FIFO
                              readout_ptr <= (readout_ptr != 0) ? readout_ptr - 1 :             // not 0
                                                (~fifo_empty)? READOUT_PTR_MAX : readout_ptr ;  // 0

                        end

                        // reset pointer
                        // (usually at end of read communication)
                        if (ptr_reset)
                              readout_ptr <= '0;

                        // short pulse behaviour on read disable
                        rd_disable <= (readout_ptr == READOUT_PTR_MAX)? '1 : '0;
                  end
                  else if (register_address == ADDR_SNAPSHOT) begin
                  
                        // send either empty filler data or readout byte
                        register_read_data <= (send_snapshot_fifo_empty_data)? FIFO_EMPTY_DATA : snapshot_byte;

                        // one shot
                        if (snaphost_decr_ptr == '0) begin
                              // enable one shot
                              snaphost_decr_ptr <= '1;

                              // determine if fifo is empty and filler data has to be sent
                              send_snapshot_fifo_empty_data <= (snapshot_ptr == 0 & snapshot_fifo_empty)? '1 : '0;

                              // decrement pointer, only reset if non-empty FIFO
                              snapshot_ptr <= (snapshot_ptr != 0) ? snapshot_ptr - 1 :             // not 0
                                                (~snapshot_fifo_empty)? SNAPSHOT_PTR_MAX : snapshot_ptr ;  // 0

                        end

                        // reset pointer
                        // (usually at end of read communication)
                        if (ptr_reset)
                              snapshot_ptr <= '0;

                        // short pulse behaviour on read disable
                        snapshot_rd_disable <= (snapshot_ptr == SNAPSHOT_PTR_MAX)? '1 : '0;
                  end                 
                  
                  // non-existent register
                  else begin
                        register_valid <= 1'b0;
                        register_read_data <= 8'h00;
                  end
            end

            // write given write data into correct register
            else if (register_write) begin
                  if (register_address == ADDR_SHIFT_REGISTER)
                        shift_register_data <= register_write_data;
                  
                  else if (register_address == ADDR_TFC) begin
                        tfc_from_i2c <= register_write_data; 
                        tfc_from_i2c_ready <= 1'b1;
                  end
                  
                  else if (register_address == ADDR_ENABLE)
                        use_register <= register_write_data; 

                  // non-existent register
                  else
                        register_valid <= 1'b0;
            end

            // reset flags during idle
            else begin
                  register_valid <= 1'b1;
                  tfc_from_i2c_ready <= 1'b0;
                  decr_ptr <= '0;
                  snaphost_decr_ptr <= 0;
            end
      end
end
endmodule