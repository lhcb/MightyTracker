// ranged_mux.v
//
// Thomas Frei
//
// Creates a multiplexer with a given input and output with. The input
// will be divided into output-sized parts. If the sizes do not match
// additional padding will be applied. For the select input an offset
// can be defined to allow a specific range for the output selection.
//
// Module ports:  Name        Description
// --------------------------------------------------------------------
// Inputs:        select      Select of input ports 
//                in[]        Input ports, concatenated
//
// Outputs:       out[]       Output port
// ---------------------------------------------------------------------
// Parameters:    SIZE_IN     Total length of input wires
//                SIZE_OUT    Total length of output wires
//                FROM        Lower select value for first input port
//                TO          Upper select value (automatically set)
// ---------------------------------------------------------------------

`timescale 1ns/1ps

module ranged_mux 
      #(parameter SIZE_IN = 30,
        parameter SIZE_OUT = 8,
        parameter FROM = 0,
        parameter TO = $rtoi(FROM + $ceil(1.0*SIZE_IN/SIZE_OUT) - 1))
      (

      // select
      input wire [($clog2(TO)-1):0] select,

      // inout
      input wire [(SIZE_IN-1):0] in,
      output wire [(SIZE_OUT-1):0] out
);

// ------------------------------------------------------------------- 
// 
// parameters
//                  
// ------------------------------------------------------------------- 

parameter INPUTS = ((TO - FROM) + 1);                 // total number of inputs
parameter SIZE_IN_EXTENDED = INPUTS * SIZE_OUT;       // entire input with padding
parameter SIZE_PADDING = SIZE_IN_EXTENDED - SIZE_IN;  // length of zero padding

// ------------------------------------------------------------------- 
// 
// generate
//                  
// ------------------------------------------------------------------- 

// extended in for padding
wire [(SIZE_IN_EXTENDED-1):0] in_extended;

generate
      // assign in to extension and pad if needed
      assign in_extended = {'0, in};

      // create multiplexer
      for (genvar i = FROM; i <= TO; i = i + 1) 
            assign out = (select == i)? in_extended[((i - FROM)*SIZE_OUT + (SIZE_OUT-1)) : ((i - FROM)*SIZE_OUT)] : 'z;
endgenerate
endmodule