// i2c_sender.v
//
// Thomas Frei
//
// Send i2c data over sda wire. Module is clocked by scl wire and therefore only 
// runs while an i2c transfer is in progress (maybe add watchdog?). Clock stretching
// not included (yet). Additional testing needed.
//
// Module ports:    Name        Description
// ------------------------------------------------------------------
// Operation:       res_n           Reset on negative clock edge 
//                                  (asynchronous)
//
// Inouts:          scl             Serial clock of I2C bus
//                  sda             Serial data line of I2C bus
//
// Inputs:          send_request    Data send initiation by master 
//                  data[7:0]       Write data for I2C transfer
//
// Outputs:         send_complete   Data transfer of 1 byte complete
//                  nack            Data not acknowledged
// -------------------------------------------------------------------

`timescale 1ns/1ps

module i2c_sender(
    // operation
    input wire res_n,

    // i2c
    inout wire scl,
    inout wire sda,

    // inputs
    input wire send_request,
    input wire [7:0] data,

    // outputs
    output wire send_complete,
    output reg nack
);

// state machine
enum reg [1:0] {SND_IDLE,   // wait for send request
                SND_SEND,   // send 1 byte of data
                SND_ACK     // wait for ack and notify master
                } state;

// ------------------------------------------------------------------- 
// 
// internals
//                  
// ------------------------------------------------------------------- 

// counter to track data
reg [2:0] data_cntr;
wire data_cntr_full;
assign data_cntr_full = data_cntr[0] & data_cntr[1] & data_cntr[2];

// buffer for send data
reg [7:0] buffer;

// msb on sda line only during send phase
wire buffer_en;
assign buffer_en = (state == SND_SEND)? 1'b1 : 1'b0;
assign (highz1, strong0) sda = (buffer[7] | ~buffer_en); //msb first

// sender ready when receiving ack
reg ack;
assign send_complete = (state == SND_ACK);//ack; xxx check

// ------------------------------------------------------------------- 
// 
// state machine
//                  
// ------------------------------------------------------------------- 

// sender state machine
always @(negedge scl or negedge res_n) begin
    // reset state machine registers
    if (!res_n) 
        state <= SND_IDLE;
    
    else begin
        // state machine transitions
        case(state)
            SND_IDLE    : state <= (send_request)? SND_SEND : state;        // request received
            SND_SEND    : state <= (data_cntr_full)? SND_ACK : state;       // full byte in shift register
            SND_ACK     : state <= (!send_request)? SND_IDLE : SND_SEND;    // request lifted or reinitiated
            default     : state <= SND_IDLE;
        endcase

        // state machine data processing
        case(state)
            SND_IDLE:   begin
                            // accpet data and reset counter
                            buffer <= data; 
                            data_cntr <= 0;
                        end

            SND_SEND:   begin
                            // push data out by shift register
                            buffer <= {buffer[6:0], buffer[7]};
                            data_cntr <= data_cntr+1;
                        end
                        
            SND_ACK :   begin
                            // accpet data and reset counter in case
                            // of multiple writes in succession
                            buffer <= data;
                            data_cntr <= 0;
                        end
        endcase
    end
end

// check for reception of ack and nack
always @(posedge scl or negedge res_n) begin
    if(!res_n) begin
        nack <= 0;
        ack <= 0;
    end

    else begin
        nack <= (state == SND_ACK)? sda : 0;
        ack <= (state == SND_ACK)? ~sda : 0;
    end
end

endmodule
