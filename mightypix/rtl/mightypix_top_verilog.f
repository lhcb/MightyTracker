

$BASE/rtl/mightypix_top.v
$BASE/rtl/readout_fsm/mightypix_readout_fsm.sv
$BASE/rtl/readout_fsm/MightypixMux.sv
$BASE/rtl/bxcounter/MightyPixTSGen.sv
$BASE/rtl/bxcounter/TScounter.sv
$BASE/rtl/ConfigBit.v

$BASE/rtl/fifo/async_fifo.sv


$BASE/rtl/serializer/SerializerMain.sv
$BASE/rtl/serializer/SerializerTop.sv
$BASE/rtl/serializer/SerializerTree.sv
$BASE/rtl/serializer/PhaseCounter.sv
$BASE/rtl/serializer/ClockGen_new.sv
$BASE/rtl/serializer/ScramblerVelo.sv


$BASE/rtl/tfc_decoder/tfc_dec.sv
$BASE/rtl/tfc_decoder/tfc_rec_align.sv
$BASE/rtl/tfc_decoder/tfc_decoder_top.sv
$BASE/rtl/tfc_decoder/tfc_fereset.sv
$BASE/rtl/tfc_decoder/tfc_40M_recovery.sv



$BASE/rtl/i2c/i2c_receiver.v
$BASE/rtl/i2c/i2c_sender.v
$BASE/rtl/i2c/i2c_slave_top.sv
$BASE/rtl/i2c/register_interface_top.sv
$BASE/rtl/i2c/ranged_mux.sv

+incdir+$BASE/building_blocks/i2c
+incdir+$BASE/rtl/tfc_decoder
