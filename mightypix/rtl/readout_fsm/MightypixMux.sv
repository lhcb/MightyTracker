/*
    Mightypux DatawordOut Mux controlled by TFC commands
*/

`include "tfc_cmd_pkg.sv"

import tfc_cmd_pkg::*;

module MightypixMux #(
        parameter SYNCPATTERN = 18'h2AAAA,
        parameter logic [1:0] STARTBITS = 2'b10,
        parameter POSEDGE_CNT8 = 3'd7
    )(
        input logic clk,
        input logic res_n,

        input logic [2:0] Cnt8,
        input logic [1:0] timer,

        input logic [29:0] WordFromDetector,

        input logic cmd_decoded,

        input logic [11:0] BXIDfromTSGen,

        output logic [31:0] datawordOut,

        input logic [29:0] syncpattern,

        //Sim only
        output logic Scr_en
    );

    logic [29:0] datawordOut_reg;

    assign datawordOut = {STARTBITS, datawordOut_reg};

    always @(posedge clk) begin
        if (!res_n) begin
            datawordOut_reg <= 0;
        end
        else if(timer == 0 && Cnt8 == POSEDGE_CNT8) begin
            /* case(cmd_decoded)

                2'b01:
                    begin
                        datawordOut_reg <= {BXIDfromTSGen[11:0],SYNCPATTERN};
                    end
                //TODO: Remove align word
                2'b10:
                    begin
                        datawordOut_reg <= 30'h3F_55_55_55; //temporary
                        //datawordOut_reg <= {6'b11_1111,24'b0};
                    end

                2'b00:
                    begin
                        datawordOut_reg <= WordFromDetector;
                    end

                default:
                    begin
                        //datawordOut_reg <= {BXIDfromCMDDec[12:0],SYNCPATTERN};
                        datawordOut_reg <= WordFromDetector;
                    end
            endcase */

            unique case(cmd_decoded)
                1'b1: // SYNC
                    begin
                        datawordOut_reg <= syncpattern;
                    end

                1'b0: // FSM OUTPUT
                    begin
                        datawordOut_reg <= WordFromDetector;
                    end
            endcase
        end
    end

    `ifdef SIMULATION
        always @(posedge clk, negedge res_n) begin
            if(!res_n)
                Scr_en <= 1;
            else if(timer == 0 && Cnt8 == 7)
                case(cmd_decoded)
                    2'b01: Scr_en <= 1;
                    2'b10: Scr_en <= 1;
                    2'b00: Scr_en <= 1;
                    default: Scr_en <= 1;
                endcase
        end
    `else
        assign Scr_en = 1'b1;
    `endif

endmodule