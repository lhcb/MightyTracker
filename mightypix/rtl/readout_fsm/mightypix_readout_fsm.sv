/*
    Mightpix Readout FSM
*/
`timescale 1ns/1ps


// FOR SIMULATION
`ifdef EOC_MODEL
    `include "eoc_model_pkg.sv"

    import eoc_model_pkg::*;
`endif

module mightypix_readout_fsm #(
    parameter   POSEDGE_CNT8 = 3'd7,
                SYNCWORD = 32'hBCBCBCBC,
                `ifdef EOC_MODEL
                TDC = 0
                `else
                TDC = 1
                `endif
)(

    // Clock + Reset
    input wire clk,
    input wire res_n,

    `ifdef EOC_MODEL
    input wire [CNT8_EDGE_EOC-1:0] Cnt8,
    `else 
    input wire [2:0] Cnt8, 
    `endif

    input wire [7:0] Fixedpattern,

    // Row/col from Det
    input wire [8:0] RowAddFromDet,
    input wire [7:0] ColAddFromDet,

    // Timestamp from Det
    input wire [11:0] TSFromDet,
    input wire [11:0] NegTSFromDet,
    input wire [2:0] FineTSFromDet,
    input wire [6:0] TDCFromDet,

    // ToT from Det
    input wire [6:0] AmpFromDet,

    // Interrupt from Det
    input wire PriFromDet,

    // Timestamp to Detector from TSGEN
    input wire [15:0] BinCounter,
    
    // Output to Hitbuffer
    output wire LdCol,
    output wire RdCol,
    output wire LdPix,
    output wire PullDN,

    // FSM Clockdivider
    input wire [3:0] timerend,
    input wire [3:0] slowdownend,
    input wire [4:0] slowdownLdColEnd,
    input wire [5:0] maxcycend,
    input wire [3:0] resetckdivend,

    input wire sendcounter,
    output logic [29:0] DataOut,

    output logic datavalid, //TODO: remove
    output logic dataoff, //TODO: remove

    input wire countsheeps,

    //Clkdiv counter (timer) output to serializer
    output wire [3:0] timer_out,

    //TDC_constants
    input wire [6:0] ts_tdc_min,
    input wire [6:0] ts_tdc_max,

    input logic tfc_sync

);


    enum logic [10:0] {
        StateSync           = 11'b100_0000_0000,
        StatePD1            = 11'b010_0000_0000,
        StatePD2            = 11'b001_0000_0000,
        StateLdCol1         = 11'b000_1000_0000,
        StateLdCol2         = 11'b000_0100_0000,
        StateLdPix1         = 11'b000_0010_0000,
        StateLdPix2         = 11'b000_0001_0000,
        StateRdCol1         = 11'b000_0000_1000,
        StateRdCol2         = 11'b000_0000_0100,

        StateSendCounter1   = 11'b000_0000_0010,
        StateSendCounter2   = 11'b000_0000_0001
    } State;

    enum logic [3:0] {
        HeaderDefault       = 4'hD,
        HeaderRd1           = 4'hA,
        HeaderRd2           = 4'hB,
        HeaderSync          = 4'hF
    } Header;


    logic [3:0] timer;
    logic [3:0] slowdown;

    logic [4:0] slowdownLdCol;

    logic [5:0] maxcyc;

    logic [7:0] resetcounter;
    logic [3:0] resetckdiv;

    assign timer_out = timer;

    // TDC
    logic [6:0] tdc_diff_divby4;
    logic [12:0] TS_calculated;

    always_ff @( posedge clk, negedge res_n ) begin
        if(!res_n) begin
            State <= StateSync;
            timer <= 0;
            slowdown <= 0;
            slowdownLdCol <= 0;//new
            maxcyc <= 0;
            resetcounter <= 0;
            resetckdiv <= 0;

        end //res
        else begin
            `ifdef EOC_MODEL
            if(Cnt8 == CNT8_EDGE_EOC) begin //Divide by pkg setting
            `else
            if(Cnt8 == POSEDGE_CNT8) begin //Divide by 8
            `endif
                if(timer < timerend)
                    timer <= timer + 1;
                else
                    timer <= 0;

                if(timer == 0) begin

                    case ( State )

                        StateSync: begin

                            if(resetckdiv < resetckdivend) resetckdiv <= resetckdiv + 1;
                            else resetckdiv <= 0;

                            if(resetckdiv==0) resetcounter <= resetcounter + 1;
                            `ifdef SIMULATION
                                if(resetcounter == 8'd16)
                                    if(sendcounter) State <= StateSendCounter1;
                                    else State <= StatePD1;
                            `else
                                if(resetcounter == 8'd255)
                                    if(sendcounter) State <= StateSendCounter1;
                                    else State <= StatePD1;
                            `endif
                        end

                        StatePD1: State <= StatePD2; //State <= StateLdCol1;//

                        StatePD2: State <= StateLdCol1;

                        StateLdCol1: begin

                            if(slowdownLdCol < slowdownLdColEnd) slowdownLdCol <= slowdownLdCol + 1;//new
                            else begin
                                slowdownLdCol <= 0;
                                State <= StateLdCol2;
                                //State <= StateLdPix1;
                            end

                        end

                        StateLdCol2: begin		

                            if(countsheeps == 1) begin
                                if(PriFromDet)begin
                                    State <= StateRdCol1;
                                end
                                else begin
                                    State <= StateLdPix1;
                                end
                            end
                            else begin
                                State <= StateLdPix1;
                            end

                        end

                        StateLdPix1: begin

                            if(PriFromDet) State <= StateLdPix2;
                            else begin//if no hits in last frame wait longer in LdPix state
                                if(slowdown < slowdownend) slowdown <= slowdown + 1;
                                else begin
                                    slowdown <= 0;
                                    State <= StateLdPix2;
                                end
                            end

                        end

                        StateLdPix2: begin

                            if(PriFromDet) //if there are hits read col
                                State <= StateRdCol1;
                            //otherwise start state
                            else
                                State <= StatePD1;
                    
                        end	


                        StateRdCol1: State <= StateRdCol2;

                        StateRdCol2: begin

                            if(PriFromDet)begin//if there are hits read col
                                if(maxcyc < maxcycend) begin//if not too many cycles
                                    maxcyc <= maxcyc + 1;
                                    State <= StateRdCol1;
                                end
                                else begin//if too many cycles start state
                                    maxcyc <= 0;
                                    State <= StatePD1;
                                end
                            end//if no hits start state
                            else begin
                                State <= StatePD1;
                                maxcyc <= 0;  // Change from MuPix7: Reset maxcyc here
                            end
                        end

                        StateSendCounter1: State <= StateSendCounter2;

                        StateSendCounter2: begin

                            if(sendcounter) State <= StateSendCounter1;
                            else State <= StatePD1;
                        end

                        default: State <= StatePD1;//to avoid hanging

                    endcase
                    
                end //timer
            end //cnt8==7
        end// not_res
    end //end_always

    /* assign LdPix  = ((State == StateLdPix1) && !Cnt8[2]);
    assign LdCol  = ((State == StateLdCol1) && !Cnt8[2]);
    assign RdCol  = ((State == StateRdCol1) && !Cnt8[2]);
    assign PullDN = ((State == StatePD1)    && !Cnt8[2]); */

    assign LdPix  = (State == StateLdPix1);
    assign LdCol  = (State == StateLdCol1);
    assign RdCol  = (State == StateRdCol1);
    assign PullDN = (State == StatePD1);

    always_comb begin : assign_fsm_header
        case(State)
            StateRdCol1: Header = HeaderRd1;
            StateRdCol2: Header = HeaderRd2;
            default:     Header = HeaderDefault;
        endcase
    end

    logic [6:0] ts_tdc_bin;
    logic [11:0] ts1_bin;

    generate
        genvar i;
        
        for (i=0; i<7; i=i+1) begin : gray2bin_tstdc
            assign ts_tdc_bin[i] = ^TDCFromDet[7-1:i];
        end
    endgenerate

    generate
        genvar i2;
        for (i2=0; i2<12; i2=i2+1) begin : gray2bin_ts1
            assign ts1_bin[i2] = ^TSFromDet[12-1:i2];
        end
    endgenerate

    /* always_comb begin: TS_TDC_calc
        tdc_diff_divby4 = ((ts_tdc_max-ts_tdc_min) << 2);

        if(State == StateRdCol1) begin
            if(TDCFromDet[6:0] < (ts_tdc_min + tdc_diff_divby4)) begin
                TS_calculated = TSFromDet + 'd1;
            end
            else if (TDCFromDet[6:0] < (ts_tdc_min + ((ts_tdc_max-ts_tdc_min) << 1) + tdc_diff_divby4)) begin
                TS_calculated = TSFromDet;
            end
            else begin
                TS_calculated = TSFromDet;
            end
        end
    end */

    always_ff @( posedge clk, negedge res_n) begin
        if(!res_n) begin
            DataOut <= SYNCWORD;
        end //res
        else if(timer == 0 && Cnt8 == 0)

            if (TDC)
                case(State)
                    //StateSync:   DataOut <= SYNCWORD;
                    StateLdPix1: DataOut <= {Header, BinCounter[15:0], 10'b0};
                    StateRdCol1: DataOut <= {RowAddFromDet[8:0], ColAddFromDet[7:0], FineTSFromDet[2:0], AmpFromDet[6:0], 3'b0};
                    StateRdCol2: DataOut <= {TSFromDet[11:0], NegTSFromDet[11:0], TDCFromDet[5:0]};
                    default:     DataOut <= {Header, BinCounter[15:0], 10'b0};
                endcase
            else
                case(State)
                    //StateSync:   DataOut <= SYNCWORD;
                    StateLdPix1: DataOut <= {Header, BinCounter[15:0], 10'b0};
                    StateRdCol1: DataOut <= {Header, RowAddFromDet[8:0], ColAddFromDet[7:0], 7'b111_1111, 2'b0}; //TEST AmpFromDet[6:0], 2'b0};
                    StateRdCol2: DataOut <= {Header, TSFromDet[11:0], FineTSFromDet[2:0], 11'b0};
                    default:     DataOut <= {Header, BinCounter[15:0], 10'b0};
                endcase

    end // end_always

endmodule
