/*
    This module generates the gray-encoded BXID and FineTS

    TSToDet: 2 variants:  Default:  16bit counter driven by 40MHz TS clock, bxres synced from 320M to 40M
                        Variant1: 16bit counter driven by 320MHz clock gated by Cnt8
    TSToDet_neg: negedge sampled copy of TSToDet

    TSToDet2:  3bit 320MHz fine counter

    TODO: programmable delay?, ckdivend,tsphase?

    WIP
*/
`timescale 1ns/1ps

`include "tfc_cmd_pkg.sv"

import tfc_cmd_pkg::*;

module MightyPixTSGen #(
    parameter TS_MAIN_WIDTH = 12,
    parameter TS_FINE_WIDTH = 3,
    parameter TS_EXT_WIDTH = 16,
    parameter TS_TDC_WIDTH = 7,
    parameter TS_TOT_WIDTH = 7,

    parameter TDC = 1,

    parameter POSEDGE_CNT8 = 3'd7,
    parameter BXCOUNT_FROM_SAMPLED40M = 0,

    parameter CKDIV_WIDTH = 6
    )(
    input logic clk_40M,
    input logic clk_320M,
    input logic clk_640M,

    input logic res_n,

    input logic [2:0] Cnt8,

    input logic bxreset,
    input logic [3:0] bxresetval,

    input logic snapshot,

    input logic [CKDIV_WIDTH-1:0] ckdivend,
    input logic [CKDIV_WIDTH-1:0] ckdivend2,
    input logic [CKDIV_WIDTH-1:0] ckdivend3,
    input logic [5:0] tsphase,

    output logic [TS_MAIN_WIDTH-1:0] TSToDet,
    output logic [TS_MAIN_WIDTH-1:0] NegTSToDet,
    output logic [TS_FINE_WIDTH-1:0] TSToDet2,

    output logic [TS_TDC_WIDTH-1:0] TSToDetTDC,
    output logic [TS_TOT_WIDTH-1:0] TSToDetTOT,

    output logic [TS_EXT_WIDTH-1:0] BinCounterOut,
    output logic [TS_EXT_WIDTH-1:0] BCcnt_snapshot,

    output logic snapshot_ready
);

    logic [TS_MAIN_WIDTH-1:0] TSmainreg, TSmainreg_neg, TSoutregbin;
    logic [TS_FINE_WIDTH-1:0] TSfine, TSfinereg;

    logic [TS_TDC_WIDTH-1:0] TSTDCreg;
    logic [TS_TOT_WIDTH-1:0] TSToTreg;

    logic [2:0] bxres_del;

    logic block_snapshot;

    assign BinCounterOut = TSoutregbin;

    assign TSToDet = TSmainreg;
    
    assign TSToDet2 = TSfine;

    assign TSToDetTOT = TSToTreg;
    
    //TDC
    assign TSToDetTDC = TSTDCreg;
    assign NegTSToDet = TSmainreg_neg;
    

    //Sync bxres to 40MHz Domain
    always_ff @( posedge clk_40M, negedge res_n ) begin
        if (!res_n)
            bxres_del <= 0;
        else
            bxres_del <= {bxres_del[1:0], bxreset};
    end

    //Snapshot
    always_ff @( posedge clk_320M) begin
        if(!res_n) begin
            block_snapshot <= 0;
            snapshot_ready <= 0;
        end
        else if (snapshot) begin
            if(!block_snapshot) begin
                BCcnt_snapshot <= TSoutregbin;
                block_snapshot <= 1'b1;
                snapshot_ready <= 1'b1;
            end
            else
                snapshot_ready <= 0;
        end
        else begin
            block_snapshot <= 0;
            snapshot_ready <= 0;
        end
    end

    //BX Counter
    generate
    if(BXCOUNT_FROM_SAMPLED40M)

        TScounter #(
            .CKDIV_WIDTH     (CKDIV_WIDTH),
            .TS_WIDTH        (TS_EXT_WIDTH)
        ) TScounter_main320_I (
            .clk             (clk_320M),
            .res_n           (res_n),
            .syncres         (bxreset),
            .countif         (Cnt8 == POSEDGE_CNT8),
            .resetval        (bxresetval),
            .ckdivend        (ckdivend),
            .TSoutreggray    (TSmainreg),
            .TSoutregbin     (TSoutregbin)
        );

    else

        TScounter #(
            .CKDIV_WIDTH     (CKDIV_WIDTH),
            .TS_WIDTH        (TS_EXT_WIDTH)
        ) TScounter_main40_I (
            .clk             (clk_40M),
            .res_n           (res_n),
            .syncres         (bxres_del[1]),
            .resetval        (bxresetval),
            .countif         (1'b1),
            .ckdivend        (ckdivend),
            .TSoutreggray    (TSmainreg),
            .TSoutregbin     (TSoutregbin)
        );

    endgenerate

    TScounter #(
            .CKDIV_WIDTH     (CKDIV_WIDTH),
            .TS_WIDTH        (TS_TOT_WIDTH)
    ) TScounter_ToT_I (
            .clk             (clk_40M),
            .res_n           (res_n),
            .syncres         (bxres_del[1]),
            .resetval        (1'b0),
            .countif         (1'b1),
            .ckdivend        (ckdivend2),
            .TSoutreggray    (TSToTreg),
            .TSoutregbin     ()
    );

    generate
    if (TDC) begin
        //Negedge counter
        always_ff @( negedge clk_40M) begin
            TSmainreg_neg <= TSmainreg;
        end

        TScounter #(
            .CKDIV_WIDTH     (CKDIV_WIDTH),
            .TS_WIDTH        (TS_TDC_WIDTH)
        ) TScounter_TDC_I (
            .clk             (clk_40M),
            .res_n           (res_n),
            .syncres         (bxres_del[1]),
            .resetval        (1'b0),
            .countif         (1'b1),
            .ckdivend        (ckdivend3),
            .TSoutreggray    (TSTDCreg),
            .TSoutregbin     ()
        );
    end
    else begin

        // FINETS Counter
    
        assign TSfine[TS_FINE_WIDTH-1] = Cnt8[TS_FINE_WIDTH-1];
        
        genvar j;
        for (j=0; j<TS_FINE_WIDTH-1; j=j+1) begin: GenTSfine_bin2gray
            assign TSfine[j] = Cnt8[j+1] ^ Cnt8[j];
        end: GenTSfine_bin2gray

    end
    endgenerate
  

endmodule