 module TScounter#(

    parameter   CKDIV_WIDTH = 6,
                TS_WIDTH = 12
    )(
    input logic clk,
    input logic res_n,

    input logic syncres,

    input logic countif,

    input logic [3:0] resetval,

    input logic [CKDIV_WIDTH-1:0] ckdivend,

    output logic [TS_WIDTH-1:0] TSoutreggray,

    output logic [TS_WIDTH-1:0] TSoutregbin

 );

    logic [TS_WIDTH-1:0] TS_bin_next, cnt, TSreg;

    logic [CKDIV_WIDTH-1:0] ckdiv;
        
    assign TS_bin_next = cnt + 'd1;
    
    assign TSoutregbin = cnt;
    assign TSoutreggray = TSreg;

    always_ff @( posedge clk, negedge res_n) begin
            if(!res_n) begin
                cnt <= 0;
                TSreg <= 0;
                ckdiv <= 0;
            end
            else begin
                if ( syncres ) begin
                    cnt <= resetval;
                    TSreg <= (resetval >> 1) ^ resetval;
                end
                else if( countif ) begin
                    if(ckdiv < ckdivend)
                        ckdiv <= ckdiv + 1;
                    else begin
                        ckdiv <= 0;
                        cnt <= cnt + 1;
                        TSreg <= (TS_bin_next >> 1) ^ TS_bin_next;
                    end
                end
            end
        end

 endmodule