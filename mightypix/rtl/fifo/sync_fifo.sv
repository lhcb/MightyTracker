/*
    Sync FIFO
*/
module sync_fifo #(
    parameter PTR_WIDTH    = 5,
    parameter WIDTH        = 5,
    parameter FALLTHROUGH  = 0
)
(
    input logic clk,
    input logic res_n,
    input logic [WIDTH-1:0] data_in,
    input logic rd_en,
    input logic wr_en,
    output logic empty,
    output logic full,
    output logic [WIDTH-1:0] data_out
);
 
    logic [WIDTH-1:0] memory [0:(1 << PTR_WIDTH-1)-1];
    logic [PTR_WIDTH-1:0] rd_ptr, wr_ptr;

 
    always_comb begin
        empty   = rd_ptr == wr_ptr;
        full    = rd_ptr == {~wr_ptr[PTR_WIDTH-1],wr_ptr[PTR_WIDTH-2:0]};
    end
    
    always_ff @(posedge clk, negedge res_n) begin
        if(!res_n) begin
            rd_ptr <= 0;
            wr_ptr <= 0;
        end
        else begin

            if( rd_en && !empty ) begin
                data_out <= memory[rd_ptr];
                rd_ptr <= rd_ptr + 1;
            end
            
            if( wr_en && !full ) begin
                memory[wr_ptr] <= data_in;
                wr_ptr <= wr_ptr + 1;
            end
        end
    end
 
 
endmodule