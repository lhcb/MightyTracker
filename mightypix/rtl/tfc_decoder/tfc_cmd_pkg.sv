`ifndef TFC_CMD_PKG
    `define TFC_CMD_PKG

    package tfc_cmd_pkg;

        parameter TFC_ALIGNBITS = 2'b11;
        parameter TFC_ALIGNBIT_LSB = 1'b0;
        
        //parameter ALIGNWORD = {TFC_ALIGNBITS, 6'b000010};

        //Aligns to TFC Words with the following patter: 11xxxxx0
        //Bit positions from Bit5 to Bit1
        typedef struct packed {
            logic bxreset;
            logic snapshot;
            logic fereset;
            logic cal_bits;
            logic mux_bits;
        } TFC_bits;

        parameter POSEDGE_CNT8 = 3'd0;

        //number of tfc bits
        parameter TFC_LEN = 8;

        //watchdog counter
        parameter WD_CNT_LEN = 8;

    endpackage
`endif