/*
    TFC_FEReset

    Resets everything except TFC and BXCount
*/
module tfc_fereset (

    input logic clk_in,
    input logic clk_40M,

    input logic cmd_reset,

    input logic res_n,

    //320 MHz
    output logic serializer_res_n,
    output logic readout_res_n,

    //40 Mhz
    output logic i2c_res_n

    );

    logic [2:0] i2c_res_n_del;

    assign i2c_res_n = i2c_res_n_del[1];
    
    //320MHz reset
    always_ff @(posedge clk_in) begin
        if(cmd_reset) begin
            serializer_res_n <= 0;
            readout_res_n <= 0;
        end
        else begin
            serializer_res_n <= 1'b1;
            readout_res_n <= 1'b1;
        end
    end

    //40Mhz reset
    always_ff @( posedge clk_40M) begin
            i2c_res_n_del <= {i2c_res_n_del[1:0], ~cmd_reset};
    end

    
    
endmodule