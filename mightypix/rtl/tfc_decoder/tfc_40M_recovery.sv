/*
    Experimental: Recover 40MHz clk from TFC
    sync counter if aligned, but continue if unaligned to provide continous PLL reference
*/

module tfc_40M_recovery (
    input logic clk_in,
    input logic [2:0] i_tfc_counter,
    input logic aligned,

    output logic clk40_from_TFC
);   
    
    logic [$clog2(TFC_LEN)-1:0] i_clk40M;



    always_ff @(posedge clk_in) begin
        if(aligned) i_clk40M <= i_tfc_counter + 1;
        else i_clk40M <= i_clk40M + 1;
    end

    always_ff @(posedge clk_in) begin
        if (i_clk40M == 0)
            clk40_from_TFC <= 1;
        else if(i_clk40M == 4)
            clk40_from_TFC <= 0;
    end

endmodule