`include "tfc_cmd_pkg.sv"

import tfc_cmd_pkg::*;

module tfc_dec (
    input logic clk_in,

    input logic [7:0] cmd_rec,
    input logic cmd_received,

    input logic tfc_lock,

    input logic res_n,

    // CMD Decoded out
    output logic cmd_mux,
    output logic cmd_cal,
    output logic cmd_reset,
    output logic [1:0] cmd_tsgen,

    output logic wd_reset
);

    enum logic [1:0] {
        StateIdle,
        StateCMDExec,
        StateWaitforLock
    } State;

    logic [7:0] tfc_command;

    logic [WD_CNT_LEN-1:0] wd_counter;
    logic wd_counter_run;

    logic [8:0] stretch_bxreset;

    TFC_bits tfc_bits;

    assign cmd_mux = tfc_bits.mux_bits;
    assign cmd_reset = tfc_bits.fereset;
    assign cmd_cal = tfc_bits.cal_bits;
    assign cmd_tsgen = {tfc_bits.snapshot, tfc_bits.bxreset};

    always @(posedge clk_in) begin
        if (!res_n)begin
            State <= StateIdle;

            tfc_command <= 0;
            tfc_bits <= 0;

        end
        else begin
            /* case(State)
                StateIdle:          State <= (cmd_received) ? StateCMDExec : State;
                StateCMDExec:       State <= StateWaitforLock;
                StateWaitforLock:   State <= (wd_reset | !tfc_lock) ? StateIdle : State;
                default:            State <= StateIdle;
            endcase */

            case(State)
                StateIdle:          State <= (cmd_received) ? StateWaitforLock : State;
                StateWaitforLock:   State <= (wd_reset | !tfc_lock) ? StateIdle : State;
                default:            State <= StateIdle;
            endcase

            /* case(State)

                StateIdle:  begin
                    wd_counter_run <= 0;

                    if (cmd_received)
                        tfc_command <=  cmd_rec;               
                end

                StateCMDExec: begin
                    wd_counter_run <= 1;

                    // Write 6bit vector to bits
                    // Modify bit positions in tfc_package
                    tfc_bits <= tfc_command[5:1];
                end

                StateWaitforLock: ;//wait for tfc_lock

            endcase */

            case(State)

                StateIdle:  begin
                    if (cmd_received) begin
                        wd_counter_run <= 1;
                        tfc_bits <=  cmd_rec[5:1];
                    end
                    else
                        wd_counter_run <= 0;        
                end

                /* StateCMDExec: begin
                    wd_counter_run <= 1;

                    // Write 6bit vector to bits
                    // Modify bit positions in tfc_package
                    tfc_bits <= tfc_command[5:1];
                end */

                StateWaitforLock: ;//wait for tfc_lock

            endcase
        end
    end

    always @(posedge clk_in) begin
        if(!res_n) begin
            wd_counter <= 0;
            wd_reset <= 0;
        end
        else begin
            if (wd_counter_run) begin
                if (wd_counter == '1) begin
                    wd_counter <= 0;
                    wd_reset <= 1;
                end
                else
                    wd_counter <= wd_counter + 1;

            end
            else begin
                wd_counter <= 0;
                wd_reset <= 0;
            end
        end
    end



endmodule