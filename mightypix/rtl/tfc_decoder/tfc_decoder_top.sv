/*
    TFC_Command_Decoder

    Receives and decodes serial 8-bit TFC commands
    The decoder has lock input, which can be used to hold the current command. To prevent lock-up, it features a timeout watchdog.
*/
module tfc_decoder_top (
    input logic clk_in,

    input logic clk_40M,

    input logic tfc_in,
    input logic tfc_lock,

    input logic res_n,
    input logic align_res_n,

    input logic [2:0] Cnt8,

    //Cmd outputs
    output logic cmd_mux,
    output logic cmd_cal,
    output logic [1:0] cmd_tsgen,

    // Reset out
    output logic serializer_res_n,
    output logic readout_res_n,
    output logic i2c_res_n,

    output logic wd_reset,

    output logic cmd_received,

    //From I2C
    input logic [7:0] tfc_from_i2c,
    input logic tfc_from_i2c_ready,
    input logic use_tfc_from_i2c,

    //recoverd clock
    output logic clk40_from_TFC

);
    logic [7:0] cmd_rec;
    logic cmd_reset;

    tfc_rec_align tfc_rec_I(
        .*
    );

    tfc_dec tfc_dec_I(
        .*
    );

    tfc_fereset tfc_fereset_I(
        .*
    );
    
endmodule