/*
    TFC Receiver: Receive 8bit commands from TFC Input @320MHz or from I2C + alignment
    Separate resets for cmd receiver and alignment to comply with FEreset requirements
*/

`include "tfc_cmd_pkg.sv"

//import tfc_cmd_pkg::ALIGNWORD;
import tfc_cmd_pkg::TFC_LEN;
import tfc_cmd_pkg::TFC_ALIGNBITS;
import tfc_cmd_pkg::TFC_ALIGNBIT_LSB;

module tfc_rec_align (
    input logic clk_in,
    input logic tfc_in,

    input logic [2:0] Cnt8,

    input logic res_n,
    input logic align_res_n,

    output logic [7:0] cmd_rec,

    output logic cmd_received,

    //TFC from I2C
    input logic [7:0] tfc_from_i2c,
    input logic tfc_from_i2c_ready,
    input logic use_tfc_from_i2c,

    output logic clk40_from_TFC
);


    logic cmd_received_reg;
    
    logic aligned, aligned_temp;
    
    logic [$clog2(TFC_LEN)-1:0] i;

    logic [TFC_LEN-1:0] tfc_reg, cmd_decoded_reg, tfc_reg_temp;

    assign cmd_rec = cmd_decoded_reg;
    assign cmd_received = cmd_received_reg;

    task match_msb();
        aligned_temp <= 0;

        if (tfc_in == TFC_ALIGNBITS[1]) begin
            i <= 1;
            tfc_reg_temp[7] <= tfc_in;
        end
        else
            i <= 0;
    endtask

    //Recover 40M clock from TFC
    tfc_40M_recovery tfc_40M_recovery_I (
        .clk_in(clk_in),
        .i_tfc_counter(i),
        .aligned(aligned_temp),
        .clk40_from_TFC(clk40_from_TFC)
    );

    always @(negedge clk_in, negedge align_res_n) begin //Sample on nedegde for relaxed timing, probably not needed
        if(!align_res_n) begin
            tfc_reg <= 0;
            aligned <= 0;
            aligned_temp <= 0;
            i <= 0;
            tfc_reg_temp <= 0;

        end
        else begin
            aligned <= aligned_temp;
            
            tfc_reg_temp[7 - i] <= tfc_in;

            case(i)
                3'd0: begin
                    if (tfc_in == TFC_ALIGNBITS[1])
                        i <= i + 1;
                    else begin
                        i <= 0;
                        aligned_temp <= 0;
                    end                  
                end

                3'd1: begin
                    if (tfc_in == TFC_ALIGNBITS[0])
                        i <= i + 1;
                    else
                        match_msb();
                end

                3'd7: begin
                    if(tfc_in == TFC_ALIGNBIT_LSB) begin
                        aligned_temp <= 1;
                        tfc_reg <= tfc_reg_temp;
                        i <= 0;
                    end
                    else 
                        match_msb();
                end

                default:
                    i <= i + 1;
            endcase  
        end
    end

    always @(posedge clk_in, negedge res_n) begin
        if (!res_n) begin
            cmd_decoded_reg <= 0;
            cmd_received_reg <= 0;

        end
        else begin

            if(use_tfc_from_i2c) begin
                    if (tfc_from_i2c_ready)
                        cmd_decoded_reg <= tfc_from_i2c;
            end
            else if(i == 0 && aligned_temp) begin
                
                cmd_received_reg <= 1;
                cmd_decoded_reg <= tfc_reg;
            end
            else
                cmd_received_reg <= 0;
        end
    end

endmodule