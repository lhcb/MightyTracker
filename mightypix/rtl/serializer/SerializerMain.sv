/*
    This module provides a Mux for fast, slow and slowest speed readout, depending on the fsm clkdiv timerend.

    Fast: 32bit Data is read in after 7th edge of cnt8. For every count of cnt8, 4bits are muxed into the serializer tree.
    Slow: 16 bits are read every two counts of the timer counter from the FSM. For every count of cnt8, 2bits are muxed to a dual edge FF.
    Slowest: 8bit are read for every count of the timer counter. For every count of cnt8 one bit is muxed to a single edge FF.

    INVALID: If timerend is set to 2 (divide by 3), send every word 3 times with 1G28

    TODO: nodatacnt needed? datavalid has to be checked before scrambling

 */
`timescale 1ns/1ps

//FAST 1x, SLOW 2x, SLOWEST 4x
typedef enum logic [1:0] {
   FAST     = 2'd0,
   SLOW     = 2'd1,
   SLOWEST  = 2'd3,

   INVALID  = 2'd2
} Speed;

module SerializerMain #(
    parameter POSEDGE_CNT8 = 3'd7
    )(

    // Input from Scrambler
    input logic [31:0] DataIn_16n_A,

    input logic clk_320M,
    output logic ResetEn,

    //Mux outputs
    output logic [3:0] DataOut_fast,
    output logic [1:0] DataOut_slow,
    output logic DataOut_slowest,

    input logic res_n,
    input logic syncres,

    input logic [2:0] Cnt8,

    // input clkdiv counter from readout FSM
    input logic [1:0] timer,

    //readout mode
    input logic [1:0] speed_mode //timer clkdiv
);

logic [31:0] DataIn_A;

logic [3:0] DataOut_fast_A;
logic [1:0] DataOut_slow_A;
logic DataOut_slowest_A;

assign DataOut_fast     = DataOut_fast_A;
assign DataOut_slow     = DataOut_slow_A;
assign DataOut_slowest  = DataOut_slowest_A;

//the ser takes the data on edge after cnt 7
always @(posedge clk_320M) begin
    if (Cnt8 == POSEDGE_CNT8)
        case(speed_mode)
            FAST, INVALID: 
                //Mux 32 to 32 with Cnt8
                DataIn_A[31:0] <= DataIn_16n_A[31:0];

            SLOW: begin
                //Mux 32 to 16 with timer[0]
                unique case (timer[0])
                    3'd0: DataIn_A[15:0] <= DataIn_16n_A[31:16];
                    3'd1: DataIn_A[15:0] <= DataIn_16n_A[15:0];
                endcase
            end

            SLOWEST:
                //Mux 32 to 8 with timer[1:0]
                unique case(timer[1:0])
                    3'd0: DataIn_A[7:0] <= DataIn_16n_A[31:24];
                    3'd1: DataIn_A[7:0] <= DataIn_16n_A[23:16];
                    3'd2: DataIn_A[7:0] <= DataIn_16n_A[15:8];
                    3'd3: DataIn_A[7:0] <= DataIn_16n_A[7:0];
                endcase

        endcase
end

//Mux
always @(posedge clk_320M) begin
    if (~res_n) begin
        DataOut_fast_A <= '0;
        DataOut_slow_A <= '0;
        DataOut_slowest_A <= '0;
    end
    else
        case(speed_mode)
            FAST, INVALID:
                //Mux 32 to 4 with Cnt8 
                unique case(Cnt8)
                    3'd0: DataOut_fast_A <= DataIn_A[31:28];
                    3'd1: DataOut_fast_A <= DataIn_A[27:24];
                    3'd2: DataOut_fast_A <= DataIn_A[23:20];
                    3'd3: DataOut_fast_A <= DataIn_A[19:16];
                    3'd4: DataOut_fast_A <= DataIn_A[15:12];
                    3'd5: DataOut_fast_A <= DataIn_A[11:8];
                    3'd6: DataOut_fast_A <= DataIn_A[7:4];
                    3'd7: DataOut_fast_A <= DataIn_A[3:0];
                endcase

            SLOW:
                //Mux 16 to 2 with Cnt8
                unique case(Cnt8)
                    3'd0: DataOut_slow_A <= DataIn_A[15:14];
                    3'd1: DataOut_slow_A <= DataIn_A[13:12];
                    3'd2: DataOut_slow_A <= DataIn_A[11:10];
                    3'd3: DataOut_slow_A <= DataIn_A[9:8];
                    3'd4: DataOut_slow_A <= DataIn_A[7:6];
                    3'd5: DataOut_slow_A <= DataIn_A[5:4];
                    3'd6: DataOut_slow_A <= DataIn_A[3:2];
                    3'd7: DataOut_slow_A <= DataIn_A[1:0];
                endcase
                
            SLOWEST:
                //Mux 1 to 1 with Cnt8
                unique case(Cnt8)
                    3'd0: DataOut_slowest_A  <= DataIn_A[7];
                    3'd1: DataOut_slowest_A  <= DataIn_A[6];
                    3'd2: DataOut_slowest_A  <= DataIn_A[5];
                    3'd3: DataOut_slowest_A  <= DataIn_A[4];
                    3'd4: DataOut_slowest_A  <= DataIn_A[3];
                    3'd5: DataOut_slowest_A  <= DataIn_A[2];
                    3'd6: DataOut_slowest_A  <= DataIn_A[1];
                    3'd7: DataOut_slowest_A  <= DataIn_A[0];
                endcase
        endcase
end

endmodule
