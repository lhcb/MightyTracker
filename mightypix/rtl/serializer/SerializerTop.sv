/*
    Mightypix Serializer

    30bit dataword from Readout FSM is scrambled and Startbit 0 and Stopbit 1 is added.
    The resulting 32bit word is muxed, in SerializerMain.

    ClockGen produces clocks for serializer and FSM.
    Phasecounter synchronizes the 40 MHz ref_clk to the 320 MHz Clock
*/
`timescale 1ns/1ps

module SerializerTop #(
    parameter USE_PLL = 1,
    parameter RESET_VAL_CNT8 = 3'd2
    )(
    
    //clk in
    input logic clkIn_40M,
    input logic clkIn_640M,

    input logic clkIn_320M,
    output logic clkOut_320M,

    //Data in
    input logic [31:0] DataIn_A,
    input logic dataoff_A,
    input logic datavalid_A,

    //timer clkdivider from Readout FSM in
    input logic [1:0] timer,

    //switch between fast, slow and slowest
    input logic [1:0] speed_mode,

    //Scrambler enable in
    input logic Scr_en,

    //Data out
    output logic [1:0] BitDataOut_A,

    output logic [1:0] DataOut_slow,
    output logic DataOut_slowest,

    //Reset in
    input logic Scr_res_n,
    input logic Ser_res_n,

    //Phasecounter out
    output logic [2:0] Cnt8,

    //Clk to pll out
    output logic clkOut_40M,

    //enable Clockgen
    input logic enclkGen,

    //DFF phase
    input logic phase
);

logic clk_320M, clk_320M_clockgen;

logic parity;
logic [31:0] ScrOut_A;

logic [3:0] DataOut_A;


//new: Configbit instead of generate
assign clk_320M = enclkGen ? clk_320M_clockgen : clkIn_320M;


assign clkOut_320M = clk_320M;

//Generates 320MHz for logic and 40Mhz for PLL from 640MHz
ClockGen_new ClockGen_I(
    .clkIn_640M(clkIn_640M),
    .clkOut_40M(clkOut_40M),
    .clkOut_320M(clk_320M_clockgen),
    .syncres()
);

//Generate 40MHz synchronized to 320MHz clock
PhaseCounter #(.CNT8_RESET_VAL(RESET_VAL_CNT8))
PhaseCounter_I(
    .sig_i(clkIn_40M),
    .clk_i(clk_320M),
    .resN_i(Ser_res_n),
    .cnt8_o(Cnt8)
);

//Parallel Scrambler with 30bit input + 2 highest bits are passed through
VeloPixScrambler VeloPixScrambler_I(
    .dataIn(DataIn_A),
    .clk(clk_320M),
    .res_n(Scr_res_n),
    .scrambleEnable(Scr_en),
    .parity(parity),
    .dataOut(ScrOut_A),
    .dataOutValid(),
    .timer(timer)
);

SerializerMain SerializerMain_I(
    .DataIn_16n_A(ScrOut_A),

    .clk_320M(clk_320M),
    .ResetEn(),

    .res_n(Ser_res_n),
    .syncres(),

    .DataOut_fast(DataOut_A),
    .DataOut_slow(DataOut_slow),
    .DataOut_slowest(DataOut_slowest),

    .Cnt8(Cnt8),
    .timer(timer),
    .speed_mode(speed_mode)
);

// This is only 4:1 serializer, other chips have 8:1 tree here
SerializerTree_4to2 SerializerTree_4to2_A(
    .clkIn_640M(clkIn_640M),
    .clkIn_320M(clk_320M),
    .DataIn(DataOut_A),
    .DataOut(BitDataOut_A),
    .phase(phase)
);
    
endmodule
