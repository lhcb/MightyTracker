/*
2-Stage Serializer 
*/
`timescale 1ns/1ps

module SerializerTree_4to2 #(

    parameter EXTERNAL_320M = 0
)(
    input logic clkIn_640M,
    input logic clkIn_320M,

    input logic [3:0] DataIn,
    output logic [1:0] DataOut,

    input logic phase
);

    logic [3:0] Stage1;
    logic [1:0] Stage2;

    logic [2:0] clk320M_sync;

    assign DataOut = Stage2; 

    always_ff @ (posedge clkIn_320M) begin
        Stage1[0] <= DataIn[0];
        Stage1[1] <= DataIn[1];
        Stage1[2] <= DataIn[2];
        Stage1[3] <= DataIn[3];
    end

    /* generate
        if(EXTERNAL_320M) begin
            //Experimental

            //3 stage synchronizer generates inverted sample of 320M clkIn
            always_ff @ (posedge clkIn_640M) begin
                clk320M_sync <= {clk320M_sync[1:0], clkIn_320M};
            end

            //Inverted output order
            always_ff @ (posedge clkIn_640M) begin
                Stage2[1] <= clk320M_sync[2]  ?  Stage1[1] : Stage1[3];
                Stage2[0] <= clk320M_sync[2]  ?  Stage1[0] : Stage1[2];
            end
        
        end
        else

            always_ff @ (posedge clkIn_640M) begin
                Stage2[1] <= clkIn_320M  ?  Stage1[3] : Stage1[1];
                Stage2[0] <= clkIn_320M  ?  Stage1[2] : Stage1[0];
            end

    endgenerate */

    
    always_ff @ (posedge clkIn_640M) begin
        if (phase) begin
            Stage2[1] <= clkIn_320M ? Stage1[1] : Stage1[3];
            Stage2[0] <= clkIn_320M ? Stage1[0] : Stage1[2];
        end
        else begin
            Stage2[1] <= clkIn_320M ? Stage1[3] : Stage1[1];
            Stage2[0] <= clkIn_320M ? Stage1[2] : Stage1[0];
        end
    end
endmodule