/*
  This module synchronizes the 40 MHz ref_clk to the 320 MHz Clock
*/
module PhaseCounter #(
        parameter CNT8_RESET_VAL = 3'd2,
        parameter SEQ = 1
    )
    (
        input logic sig_i,
        input logic clk_i,
        input logic resN_i,

        output logic [2:0] cnt8_o
    );

    logic [2:0] sig_i_del;
    logic set8;

    //Edge Detector
    generate
    if (SEQ)   
        always_ff @( posedge clk_i ) begin : set8_seq
            set8 <= ~sig_i_del[2] & sig_i_del[1];
        end
    else
        always_comb begin : set8_comb
            set8 = ~sig_i_del[2] & sig_i_del[1];
        end 
    endgenerate

    always_ff @( posedge clk_i ) begin
        sig_i_del <= {sig_i_del[1:0], sig_i}; 
    end

    //3bit counter
    always_ff @( posedge clk_i ) begin
        //No cnt8_o reset needed
        if(set8)
            cnt8_o <= SEQ ? CNT8_RESET_VAL+1 : CNT8_RESET_VAL;
        else
            cnt8_o <= cnt8_o + 1;  
    end

endmodule