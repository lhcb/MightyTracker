/*
 Clockgen New generates 320MHz and 40 MHz from 640MHz input

 TODO: -syncres generation needed?
 */
`timescale 1ns/1ps

module ClockGen_new (
    input logic clkIn_640M,

    output logic clkOut_320M,
    output logic clkOut_40M,

    output logic syncres
);

//Assign value for sim
`ifdef SIMULATION
    logic [14:0] FastCnt16 = 15'b11111111111111;
    logic FastCnt2 = 1'b0;
    logic [2:0] FastCnt4 = 3'b111;
`else
    logic [14:0] FastCnt16;
    logic FastCnt2;
    logic [2:0] FastCnt4;
`endif

logic resregor1, resregor2;
logic [3:0] resreg;

/* //320 MHz Clock
wire ResetCnt2;
assign ResetCnt2 = ~FastCnt2;
//assign clkOut_320M = FastCnt2;

always @ (posedge clkIn_640M) begin
    FastCnt2 <= ResetCnt2;//res 1 cnt 1
end */

//40 MHz Clock
wire ResetCnt;
assign ResetCnt = ~FastCnt16[0];
assign clkOut_40M = FastCnt16[7];

always @ (posedge clkIn_640M) begin

    FastCnt16[14] <= ResetCnt;//res 1 cnt 1
    FastCnt16[13] <= ~(~ResetCnt & ~FastCnt16[14]);//res 1 cnt 0 res 0 cnt cnt
    FastCnt16[12] <= ~(~ResetCnt & ~FastCnt16[13]);
    FastCnt16[11] <= ~(~ResetCnt & ~FastCnt16[12]);
    FastCnt16[10] <= ~(~ResetCnt & ~FastCnt16[11]);
    FastCnt16[9] <= ~(~ResetCnt & ~FastCnt16[10]);
    FastCnt16[8] <= ~(~ResetCnt & ~FastCnt16[9]);
    FastCnt16[7] <= ~(~ResetCnt & ~FastCnt16[8]);
    FastCnt16[6] <= ~(~ResetCnt & ~FastCnt16[7]);
    FastCnt16[5] <= ~(~ResetCnt & ~FastCnt16[6]);
    FastCnt16[4] <= ~(~ResetCnt & ~FastCnt16[5]);
    FastCnt16[3] <= ~(~ResetCnt & ~FastCnt16[4]);
    FastCnt16[2] <= ~(~ResetCnt & ~FastCnt16[3]);
    FastCnt16[1] <= ~(~ResetCnt & ~FastCnt16[2]);
    FastCnt16[0] <= ~(~ResetCnt & ~FastCnt16[1]);
end



//160 MHz Clock
logic clkOut_160M;
wire ResetCnt4B;//16 oct
assign ResetCnt4B = FastCnt4[0];//16 oct
assign clkOut_160M = FastCnt4[1];

always @ (posedge clkIn_640M) begin

    FastCnt4[2] <= ~ResetCnt4B;//res 1 cnt 1
    FastCnt4[1] <= ~(ResetCnt4B & ~FastCnt4[2]);//res 1 cnt 0 res 0 cnt cnt
    FastCnt4[0] <= ~(ResetCnt4B & ~FastCnt4[1]);//res 1 cnt 0 res 0 cnt cnt

end


//Generate 320MHz from xor'd 160MHz and 160MHz_del
logic clkOut_320M_2;
logic clkOut_320M_AND1, clkOut_320M_AND2, clkOut_320M_del;
logic clkOut_160M_del;

always @ (posedge clkIn_640M) begin
  
    clkOut_160M_del <= clkOut_160M;
    clkOut_320M_AND1 <= ~(clkOut_160M_del & ~clkOut_160M);
    clkOut_320M_AND2 <= ~(~clkOut_160M_del & clkOut_160M);
    clkOut_320M_del <= ~(clkOut_320M_AND1 & clkOut_320M_AND2);//was del
    clkOut_320M <= clkOut_320M_del;

    //clkOut_1n6 <= clkOut_3n2_del ^ clkOut_3n2;
end

endmodule
