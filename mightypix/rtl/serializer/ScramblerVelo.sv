//Verilog Rewrite of VeloPix GWT Scrambler

module VeloPixScrambler (

        input logic [31:0] dataIn,

        input logic clk,

        input logic [1:0] timer,

        input logic res_n,
        input logic scrambleEnable,

        output logic parity,
        output logic dataOutValid,
        output logic [31:0] dataOut
    );

    logic [31:0] dataOut_temp;
    logic [29:0] prevIn;
    logic parity_calculated;
    logic s_DV;

    logic dataIn_valid_temp;
    
    assign parity = parity_calculated;

    always @(posedge clk) begin
        if (!res_n) begin
            prevIn          <= '0;
            dataOut_temp    <= '0;
            dataOut         <= '0;
            s_DV            <= 1'b0;
            dataOutValid    <= 1'b0;
        end
        else begin
            if (!scrambleEnable) begin
                dataOut_temp[31:0] <= dataIn[31:0];
            end
            else begin
                dataOut_temp[0]    <= dataIn[0]  ^ prevIn[0]  ^ prevIn[1]  ^ prevIn[15] ^ prevIn[16];
                dataOut_temp[1]    <= dataIn[1]  ^ prevIn[1]  ^ prevIn[2]  ^ prevIn[16] ^ prevIn[17];
                dataOut_temp[2]    <= dataIn[2]  ^ prevIn[2]  ^ prevIn[3]  ^ prevIn[17] ^ prevIn[18];
                dataOut_temp[3]    <= dataIn[3]  ^ prevIn[3]  ^ prevIn[4]  ^ prevIn[18] ^ prevIn[19];
                dataOut_temp[4]    <= dataIn[4]  ^ prevIn[4]  ^ prevIn[5]  ^ prevIn[19] ^ prevIn[20];
                dataOut_temp[5]    <= dataIn[5]  ^ prevIn[5]  ^ prevIn[6]  ^ prevIn[20] ^ prevIn[21];
                dataOut_temp[6]    <= dataIn[6]  ^ prevIn[6]  ^ prevIn[7]  ^ prevIn[21] ^ prevIn[22];
                dataOut_temp[7]    <= dataIn[7]  ^ prevIn[7]  ^ prevIn[8]  ^ prevIn[22] ^ prevIn[23];
                dataOut_temp[8]    <= dataIn[8]  ^ prevIn[8]  ^ prevIn[9]  ^ prevIn[23] ^ prevIn[24];
                dataOut_temp[9]    <= dataIn[9]  ^ prevIn[9]  ^ prevIn[10] ^ prevIn[24] ^ prevIn[25];
                dataOut_temp[10]   <= dataIn[10] ^ prevIn[10] ^ prevIn[11] ^ prevIn[25] ^ prevIn[26];
                dataOut_temp[11]   <= dataIn[11] ^ prevIn[11] ^ prevIn[12] ^ prevIn[26] ^ prevIn[27];
                dataOut_temp[12]   <= dataIn[12] ^ prevIn[12] ^ prevIn[13] ^ prevIn[27] ^ prevIn[28];
                dataOut_temp[13]   <= dataIn[13] ^ prevIn[13] ^ prevIn[14] ^ prevIn[28] ^ prevIn[29];
                dataOut_temp[14]   <= dataIn[14] ^ prevIn[14] ^ prevIn[15] ^ prevIn[29] ^ dataIn[0];
                dataOut_temp[15]   <= dataIn[15] ^ prevIn[15] ^ prevIn[16] ^ dataIn[0]  ^ dataIn[1];
                dataOut_temp[16]   <= dataIn[16] ^ prevIn[16] ^ prevIn[17] ^ dataIn[1]  ^ dataIn[2];
                dataOut_temp[17]   <= dataIn[17] ^ prevIn[17] ^ prevIn[18] ^ dataIn[2]  ^ dataIn[3];
                dataOut_temp[18]   <= dataIn[18] ^ prevIn[18] ^ prevIn[19] ^ dataIn[3]  ^ dataIn[4];
                dataOut_temp[19]   <= dataIn[19] ^ prevIn[19] ^ prevIn[20] ^ dataIn[4]  ^ dataIn[5];
                dataOut_temp[20]   <= dataIn[20] ^ prevIn[20] ^ prevIn[21] ^ dataIn[5]  ^ dataIn[6];
                dataOut_temp[21]   <= dataIn[21] ^ prevIn[21] ^ prevIn[22] ^ dataIn[6]  ^ dataIn[7];
                dataOut_temp[22]   <= dataIn[22] ^ prevIn[22] ^ prevIn[23] ^ dataIn[7]  ^ dataIn[8];
                dataOut_temp[23]   <= dataIn[23] ^ prevIn[23] ^ prevIn[24] ^ dataIn[8]  ^ dataIn[9];
                dataOut_temp[24]   <= dataIn[24] ^ prevIn[24] ^ prevIn[25] ^ dataIn[9]  ^ dataIn[10];
                dataOut_temp[25]   <= dataIn[25] ^ prevIn[25] ^ prevIn[26] ^ dataIn[10] ^ dataIn[11];
                dataOut_temp[26]   <= dataIn[26] ^ prevIn[26] ^ prevIn[27] ^ dataIn[11] ^ dataIn[12];
                dataOut_temp[27]   <= dataIn[27] ^ prevIn[27] ^ prevIn[28] ^ dataIn[12] ^ dataIn[13];
                dataOut_temp[28]   <= dataIn[28] ^ prevIn[28] ^ prevIn[29] ^ dataIn[13] ^ dataIn[14];
                dataOut_temp[29]   <= dataIn[29] ^ prevIn[29] ^ dataIn[0]  ^ dataIn[14] ^ dataIn[15];

                dataOut_temp[31:30] <= dataIn[31:30];
            
            end

            prevIn <= dataIn[29:0];
            if(timer == 0)
                dataOut <= dataOut_temp;

            s_DV <= 1'd1;
            dataOutValid <= s_DV;
        end
    end

    //Parity calculation
    always @(posedge clk) begin
        if(!res_n)
            parity_calculated <= 1'b0;
        else begin
            if (!scrambleEnable)
                parity_calculated <= 1'b0;
            else if (timer == 0) //Calculate parity one cycle before writing dataOut
                parity_calculated <= ^dataOut_temp[29:0]; //bitwise xor

        end       
    end

endmodule