
## Top  (top, readout, SR)
$BASE/rtl/legacy_mupix10/MuPixDigitalTop.v
$BASE/rtl/legacy_mupix10/MuPixTSGen.v
$BASE/rtl/legacy_mupix10/MuPixReadout.v
$BASE/rtl/legacy_mupix10/ConfigBit.v

## Serialiser 
$BASE/rtl/serializer/SerializerMain.v
$BASE/rtl/serializer/SerializerTop.v
$BASE/rtl/serializer/SerializerTree.v
$BASE/rtl/serializer/ScramblerVelo.vhd
$BASE/rtl/serializer/PhaseCounter.v
$BASE/rtl/serializer/ClockGen_new.v


## I2C

## Old removed for now
#$BASE/rtl/legacy_mupix10/SlowControlStatemachine.v
#$BASE/rtl/legacy_mupix10/Buffer.v
#$BASE/rtl/legacy_mupix10/aurora_trans_simple.v
#$BASE/rtl/legacy_mupix10/8b10bwrapper.v
#$BASE/rtl/legacy_mupix10/CW_8b10b_enc.v