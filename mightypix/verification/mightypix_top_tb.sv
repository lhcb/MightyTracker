`timescale 1ns/1ps

`include "chip_matrix_vvc.sv"
`include "serializer/chip_serializer_vvc.sv"
`include "tfc_decoder/chip_tfcdec_vvc.sv"
`include "veritas_base_tb.sv"
`include "shr_vvc.sv"

`include "lpgbt_vvc.sv"
`include "i2c_monitor_vvc.sv"
`include "mightypix_reg_vvc.sv"
`include "mightypix_monitor_vvc.sv"
`include "i2c_scoreboard_vvc.sv"
`include "i2c_global.sv"
`include "mightypix_tests_vvc.sv"

`include "address_space_pkg.sv"

import address_space_pkg::*;

module tb_top;


    /**
        This object is only used to define current running test suite and test in the testbench.
        It's internal String variable can be displayed in the wafevorm to easily identify which parts of the wafevorm belong to which test

    */
    veritas_base_tb veritas_tb = new();

    //localparam CLK_SPEED = ((1.0/1000.0)*1000000);
    logic   clk_ts;

    //-- 800p Clock from PLL in real design
    logic   clk_40;
    logic   clk_320;
    logic   clk_640;
    logic   res_n;

    wire clkOut_320;

    // I2C
    //----------------
    //wire scl;
    //wire sda;

    logic use_I2C_extern = 1'b0;
    logic use_I2C_tfc_extern = 1'b0;

    //localparam  I2C_CHIP_ID = 3'b1;

    i2c_wire_if i2c_master_wire_if();
    i2c_reg_if #(.ADDR_BW(11)) i2c_reg_if_A();
    shift_register_if shift_register();
    addr_space_if #(.CONFIG_BIT_TOTAL(CONFIG_BIT_TOTAL), .READOUT_WIDTH(READOUT_WIDTH)) addr_space_if_A(.shift_register_if_A(shift_register));


    // wire pullup
    pullup(i2c_master_wire_if.scl);
    pullup(i2c_master_wire_if.sda);

    // mailboxes
    mailbox i2c_mailbox = new();
    mailbox register_mailbox = new();

    // events
    event stopE;
    event restartE;

    // TBD


    // Matrix
    //---------------

    //-- Matrix Connections
    chip_matrix_if matrix_if();

    //Serialiser
    //--------------------
    chip_serializer_if serializer_if();

    //TFC Decoder
    chip_tfcdec_if tfcdec_if(clk_320);


    assign tfcdec_if.Cnt8 = serializer_if.Cnt8;

    // Clocks + Reset
    //------------
    localparam CLK40_SPEED  = 40;
    localparam CLK320_SPEED = 320;
    localparam CLK640_SPEED = 640;
    localparam TSCLK_SPEED  = 40;

    //! FM_ClkGen_MHz: Creates glitches because clockperiods are not exact, due to rounding errors
    //FM_ClkGen_MHz #(.MHZ(CLK40_SPEED),.OFFSET(1500))    clk_gen_40  (.clk(clk_40));
    //FM_ClkGen_MHz #(.MHZ(CLK320_SPEED),.OFFSET(1500))   clk_gen_320 (.clk(clk_320));
    //FM_ClkGen_MHz #(.MHZ(CLK640_SPEED),.OFFSET(1500))   clk_gen_640 (.clk(clk_640));

    //FM_ClkGen_MHz #(.MHZ(TSCLK_SPEED),.OFFSET(1500))    clk_gen_ts  (.clk(clk_ts));
    initial begin
        clk_640 = 1;
        clk_320 = 1;
        clk_40 = 1;
        clk_ts = 1;
    end

    always #1 clk_640 <= ~clk_640;
    always #2 clk_320 <= ~clk_320;
    always #16 clk_40 <= ~clk_40;
    always #16 clk_ts <= ~clk_ts;


    FM_RstnGen resn_gen(.res_n(res_n),.ref_clk(clk_40));

    // Shift Register
    //--------------------

    //-- External Shift Register
    localparam CONFIG_BIT_EXTERNAL_COUNT = 16;
    SHR_INTERFACE shiftregister_driver_if();
    SHR_INTERFACE shiftregister_output_if();
    wire [CONFIG_BIT_EXTERNAL_COUNT-1:0] SRExtraBits;

    bit [7:0] tfc_command = 8'b1100_0010;


    initial
    begin
        // SDF Annotation
        // This must be place at the beginning of times and not in any "if" conditions
        // Only used after post place and route, just ignore for the first verilog only simulations
        //-------------------
        `ifdef SIMULATE_WITH_SDF
            `ifdef SDF_CORNER
                $sdf_annotate("../par/stream_out/MuPixDigitalTop.`SDF_CORNER.sdf", mproc.u1,, "annotate.log", "maximum");
            `else
                // $sdf_annotate("../par/stream_out/MPROC_v2.functional_800p.slow.sdf", mproc.u1,, "annotate.log", "maximum:typical:minimun");
                //$sdf_annotate("../par/stream_out/MPROC_v2.functional_800p.slow.sdf", mproc.u1,, "annotate.log","MAXIMUM");
                //
                //$sdf_annotate("../par/stream_out/MPROC_v2.functional_1n25.slow.sdf", mproc.u1,, "annotate.log","MAXIMUM");

                $sdf_annotate("../par/stream_out/MPROC_v2.functional_800p.slow.sdf", mproc.u1,, "annotate.log","MAXIMUM");
                //$sdf_annotate("../par/stream_out/MPROC_v2.functional_800p.noedge.slow.sdf", mproc.u1,, "annotate.log","MAXIMUM");
            `endif
        `endif
    end

    // Test
    //------------------------
    initial
    begin



        // Chip Model
        automatic chip_matrix_driver    matrix_driver        = new();
        automatic chip_matrix_monitor   matrix_monitor      = new();

        //Serializer
        //automatic chip_serializer_monitor serializer_monitor = new();
        automatic top_serializer_test top_serializer_test = new(serializer_if);

        //TFC Decoder
        automatic top_tfcdec_test top_tfcdec_test = new(tfcdec_if);
        automatic chip_tfcdec_driver top_tfcdec_driver = new(tfcdec_if);

        // I2c
        automatic lpGBT lpGBT_A = new(i2c_master_wire_if);
        automatic I2CMonitor I2CMonitor_A = new(i2c_master_wire_if, i2c_mailbox, stopE, restartE);
        automatic I2CScoreboard I2CScoreboard_A = new(i2c_mailbox, register_mailbox, stopE, restartE);
        automatic MightyPixTest MightyPixTest_A = new();
        automatic MightypixMonitor MightypixMonitor_A = new(i2c_master_wire_if, i2c_reg_if_A, addr_space_if_A, register_mailbox, `CHIP_ID_A, stopE);
        //automatic MightyPix MightyPix_A = new(i2c_master_wire_if, i2c_reg_if_A);
        
        // TBD

        // Shift Register
        automatic shr_driver            shift_register_driver   = new();

        automatic shr_segment ckdivend          = shift_register_driver.add_segment(6,0);
        automatic shr_segment ckdivend2         = shift_register_driver.add_segment(6,0);
        automatic shr_segment timerend          = shift_register_driver.add_segment(4,4'd0);
        automatic shr_segment slowdownend       = shift_register_driver.add_segment(4,0);
        automatic shr_segment maxcycend         = shift_register_driver.add_segment(6,0);

        automatic shr_segment sr_empty         = shift_register_driver.add_segment(2,1);


        automatic shr_segment resetckdivend     = shift_register_driver.add_segment(4,0);
        automatic shr_segment sendcounter       = shift_register_driver.add_segment(1,0);
        automatic shr_segment itsphase          = shift_register_driver.add_segment(6,1);
        automatic shr_segment EnSync_SC         = shift_register_driver.add_segment(1,0);
        automatic shr_segment slowdownldcolend  = shift_register_driver.add_segment(5,5'b00000);
        automatic shr_segment countsheeps       = shift_register_driver.add_segment(1,0);
        automatic shr_segment syncpattern       = shift_register_driver.add_segment(30,30'h3FFF8000);
        automatic shr_segment bxresetval        = shift_register_driver.add_segment(4,4'd0);
        automatic shr_segment ts_tdc_min        = shift_register_driver.add_segment(7,7'd1);
        automatic shr_segment ts_tdc_max        = shift_register_driver.add_segment(7,7'd10);
        automatic shr_segment ckdivend3         = shift_register_driver.add_segment(6,1);
        automatic shr_segment IenclkgenB        = shift_register_driver.add_segment(1,0);
        automatic shr_segment Iphase            = shift_register_driver.add_segment(1,0);
        automatic shr_segment IenScr            = shift_register_driver.add_segment(1,1);
        automatic shr_segment dummy             = shift_register_driver.add_segment(16,16'hACDB);
        //automatic shr_segment dummy2            = shift_register_driver.add_segment(16,16'hACDB);


        // Scoreboard
        //automatic scoreboard sb = new();

        // Inits
        //-----------
        shift_register_driver.time_delay = 5ns; // Delay determines speed of Shift Register interface

        matrix_monitor.silent                  = 0; // Console will display monitored generated pixel hits

        // Start
        //---------------------------------------------------

        // Reset
        resn_gen.enableReset ();



        // Some inits for some internal signals in DUT which will be XXX in simulation because not reset
        // The lack of reset in design is expected to ensure no RS Flip Flip is inferred which are a bit slower than standard flip-flops
        `ifdef GTL_SIMULATION

            /*force mproc.u1.SerializerTop_I_ClockGen_I_FastCnt5 = 4'b0000;
            force mproc.u1.clkOut_4n                           = 1'b0;
            force mproc.u1.SerializerTop_I_ClockGen_I_FastCnt4 = 3'b000;
            force mproc.u1.SerializerTop_I_ClockGen_I_resreg[3:0] = 4'b0000;

            #500 ck800p_start = 1;
            repeat(10)
            begin
                @(posedge clk_800p);
            end
            ck800p_start = 0;

            release mproc.u1.clkOut_4n;
            release mproc.u1.SerializerTop_I_ClockGen_I_FastCnt5;
            release mproc.u1.SerializerTop_I_ClockGen_I_FastCnt4;
            release mproc.u1.SerializerTop_I_ClockGen_I_resreg[3:0];*/
        `endif



        //------------------------------------------------------------

        // Start Monitors etc..
        ///------------------
        fork
            matrix_driver.drive_pixel_data(res_n,matrix_if );
            matrix_monitor.monitor(matrix_if);

            //Monitor only
            top_serializer_test.monitor_only();
            
            top_tfcdec_test.monitor_only(clk_320, clk_40);
            top_tfcdec_driver.tfc_gen_command(tfc_command, clk_320, clk_40);

            // I2C
            lpGBT_A.run();
            I2CMonitor_A.run();
            MightypixMonitor_A.run();
            I2CScoreboard_A.run();
            //MightyPix_A.run();

            // TBD

            // Scoreboard -> later
            /*sb.monitor_debug(spi_protocol_decoder.debug);
            sb.monitor_debug(receiver_8b10b_protocol_decoder.debug);
            sb.monitor_pixels_output(spi_protocol_decoder.pixels);
            sb.monitor_pixels_output(receiver_8b10b_protocol_decoder.pixels);
            sb.monitor_pixels_matrix(matrix_monitor.received_transactions);*/

        join_none

        // Reset Sequence
        //---------
        shift_register_driver.print_length();
        shift_register_driver.init(shiftregister_driver_if); // This inits IO

        resn_gen.enableReset (); // Release resets


        //-- Send Configuration to Chip
        #250ns veritas_tb.test_start("Sending SR Configuration to chip");

        shift_register_driver.shift_zeros(shiftregister_driver_if,64);
        shift_register_driver.shift_local_segments(shiftregister_driver_if);

        // Extra bits should be ACDB
        if (SRExtraBits!=16'hACDB)
        begin
            #500 $fatal("SR Bits don't match!");
        end

        veritas_tb.test_stop();


        //-- Remove main reset, keep transmit paths in reset to avoid simulation chaos
        resn_gen.disableReset();

        //MightyPixTest_A.functionality_test(lpGBT_A);

        // Matrix Tests
        //--------------------

        //-- Waitout the Sync State at the beginning of the matrix
        //-- Not Necessary now, Keep commented for reference
        /*`ifdef GTL_SIMULATION
            // wait(mproc.u1.MuPixReadout_I_State!=MPROC_Readout_FIFO_8b10b_pkg::StateSync);
            wait(mproc.u1.MuPixReadout_I_State!=MPROC_Readout_FIFO_8b10b_pkg::StateSync);
        `else
            wait(mproc.MuPixReadout_I.State!=MPROC_Readout_FIFO_8b10b_pkg::StateSync);
        `endif*/


        // Pixel Simulation
        // -----------------------

        // First test -> juste produce one pixel hit to demonstrate
        #3000 tfc_command=8'b1100_0000;
        #3000 tfc_command=8'b1100_0010; //send alignword
        #3000 tfc_command=8'b1100_0000;

        #1500 veritas_tb.test_start("Generate One Pixel");

        matrix_driver.generate_pixel_data(30);

        veritas_tb.test_stop();


        #10000 tfc_command=8'b1101_0000; //snapshot
        #100 tfc_command=8'b1110_0000; //Bxreset
        #30 tfc_command=8'b1100_0000;
        #100 tfc_command=8'b1100_1000; // FEReset
        #30  tfc_command=8'b1100_0000;
        #100 tfc_command=8'b1000_0000; //unaligned
        #100 tfc_command=8'b1100_0000;

        // I2C tests (increase Simulation time to 600000 if you want to run all I2C test)
        //--------------------
        #1000 //use_I2C_extern = 1;
        use_I2C_tfc_extern = 0;
        use_I2C_extern = 0;
        #50 MightyPixTest_A.shift_register_test(lpGBT_A);
        #50000
        use_I2C_tfc_extern = 1;
        use_I2C_extern = 1;

    end

    always begin
        #60000 $stop();
    end

    //Monitor serializer submodule io
    assign serializer_if.timer = dut.timer;
    assign serializer_if.speed_mode = dut.QConfig[15:12];
    assign serializer_if.DataIn_A = dut.DataOutToSer;
    assign serializer_if.Scr_res_n = dut.Ser_res_n;
    assign serializer_if.Ser_res_n = dut.Ser_res_n;
    assign serializer_if.Cnt8 = dut.Cnt8;

    //Monitor TFC submodule io
    assign tfcdec_if.res_n = res_n;
    assign tfcdec_if.use_I2C_tfc = dut.use_I2C_tfc;
    assign tfcdec_if.i2c_tfc_data = dut.i2c_tfc_data;
    assign tfcdec_if.tfc_cmd_dec_out = dut.tfc_decoder_top_I.tfc_dec_I.tfc_bits;

    //Monitor i2c submodule io
    assign i2c_reg_if_A.register_valid = dut.register_valid;
    assign i2c_reg_if_A.register_address = dut.register_address;
    assign i2c_reg_if_A.register_read_data = dut.register_read_data;
    assign i2c_reg_if_A.register_read = dut.register_read;
    assign i2c_reg_if_A.register_write_data = dut.register_write_data;
    assign i2c_reg_if_A.register_write = dut.register_write;

    assign addr_space_if_A.QConfig = dut.QConfig;
    //assign addr_space_if_A.fsm_state = dut.fsm_state;
    assign addr_space_if_A.tfc_from_i2c_ready = dut.i2c_tfc_ready;
    assign addr_space_if_A.use_tfc_from_i2c = dut.use_I2C_for_tfc;
    assign addr_space_if_A.tfc_from_i2c = dut.i2c_tfc_data;
    assign addr_space_if_A.use_i2c = dut.use_I2C;
    assign addr_space_if_A.readout = dut.fifo_data_to_i2c;
    assign addr_space_if_A.fifo_empty = dut.readout_fifo_empty;

    assign shift_register.sin = dut.SIn_I2C;
    assign shift_register.ck1 = dut.Ck1_I2C;
    assign shift_register.ck2 = dut.Ck2_I2C;
    assign shift_register.ldA = dut.Ld_I2C1;

    // DUT
    //---------------------------
    mightypix_top dut (

        // Clocking + Reset
        //---------
        .clk_40M            ( clk_40    ),
        .clk_320M           ( clk_320   ),
        .clk_640M           ( clk_640   ),

        .Ser_res_n          ( res_n     ),
        .RO_res_n           ( res_n     ),
        .TSGen_res_n        ( res_n     ),
        .TFCalign_res_n     ( res_n     ),
        .i2c_res_n          ( res_n     ),

        .clkOut_320M        (clkOut_320 ),

        // Matrix
        //-------------

        //from matrix
        .PriFromDet    ( matrix_if.PriFromDet    ),
        .RowAddFromDet ( matrix_if.RowAddFromDet ),
        .ColAddFromDet ( matrix_if.ColAddFromDet ),
        .TSFromDet     ( matrix_if.TSFromDet     ),
        .NegTSFromDet  ( matrix_if.NegTSFromDet  ),
        .TDCFromDet    ( matrix_if.TDCFromDet    ),
        .AmpFromDet    ( matrix_if.AmpFromDet    ),
        .FineTSFromDet ( matrix_if.FineTSFromDet    ),

        //to matrix
        .TSToDet    ( matrix_if.TSToDet    ),
        .NegTSToDet  ( matrix_if.NegTSToDet  ),
        .FineTSToDet ( matrix_if.FineTSToDet ),
        .TSToDetTDC  ( matrix_if.TSToDetTDC    ),

        // Readout Control
        .LdCol  ( matrix_if.LdCol   ),
        .RdCol  ( matrix_if.RdCol   ),
        .LdPix  ( matrix_if.LdPix   ),
        .PullDN ( matrix_if.PullDN ),

        // Serialiser Out
        //----------------
        .d_out(serializer_if.Bitdataout),
        .DataOut_slow(serializer_if.DataOut_slow),
        .DataOut_slowest(serializer_if.DataOut_slowest),

        // TFC In
        .tfc_in(tfcdec_if.tfc_in),

        //PLL FB
        .clkOut_40M(),

        // SR
        //--------------
        .SIn_Ext        (shiftregister_driver_if.SIN),
        .Ck1_Ext        (shiftregister_driver_if.CK1),
        .Ck2_Ext        (shiftregister_driver_if.CK2),
        .Ld_Ext1         (shiftregister_driver_if.LOAD),
        .Readback_Ext   (1'b0   ),
        .SRExtraBits    (  SRExtraBits     ),
        .SRExtraBitsN   (       ),

        .WrRAM_in0      (       ),
        .WrRAM_in1      (       ),
        .WrRAM_in2      (       ),
        .WrRAM_in3      (       ),

        .SOut_Ext(),


        //.shift_register_input (shiftregister_driver_if),
        //.shift_register_output(shiftregister_output_if),

        // I2C
        //------------
        .i2c_sda        (i2c_master_wire_if.sda),
        .i2c_scl        (i2c_master_wire_if.scl),
        .i2c_chip_id    (`CHIP_ID_A),

        .use_I2C_extern(use_I2C_extern),
        .use_I2C_tfc_extern(use_I2C_tfc_extern)
    );


    // SerDes Sim
    //-------------------
    SerDesTop_SIM SerDesTop_SIM_I(
        .DataIn_fast(serializer_if.Bitdataout),
        .DataIn_slow(serializer_if.DataOut_slow),
        .DataIn_slowest(serializer_if.DataOut_slowest),

        .clkIn_640M(clk_640),
        .clk_320M(clkOut_320),

        .Scr_res_n(serializer_if.Scr_res_n),
        .Ser_res_n(serializer_if.Ser_res_n),

        .DeScrOut_fast(serializer_if.DeScrOut_1G28),
        .DeScrOut_slow(serializer_if.DeScrOut_640M),
        .DeScrOut_slowest(serializer_if.DeScrOut_320M),

        .Cnt8(serializer_if.Cnt8)
    );



endmodule