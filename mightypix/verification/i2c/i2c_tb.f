-access 
+rw 
-sv 

## Defines
-define SIMULATION 

## Headers
+incdir+$BASE/building_blocks/i2c

## Standard Cells  Models for Simulation
-makelib .:cells
$BASE/implementation/ams_h18/h18_CORELIB_HV_heiko.v
-endlib 

## Files
$BASE/rtl/i2c/i2c_slave_top.sv 
$BASE/rtl/i2c/i2c_receiver.v 
$BASE/rtl/i2c/i2c_sender.v 
$BASE/rtl/i2c/ranged_mux.sv
$BASE/rtl/i2c/register_interface_top.sv
$BASE/rtl/legacy_mupix10/ConfigBit.v
$BASE/rtl/readout_fsm/async_fifo.sv

## Testbench
$BASE/verification/i2c/i2c_tb.sv
