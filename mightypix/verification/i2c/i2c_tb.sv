// i2c_tb.sv
//
// Thomas Frei
// 
// Work in progress!
//
// Testbench for I2C communication.
//

// ------------------------------------------------------------------- 
// 
// Testbench headers
//                  
// -------------------------------------------------------------------

`include "lpgbt_vvc.sv"
`include "i2c_monitor_vvc.sv"
`include "mightypix_reg_vvc.sv"
`include "mightypix_monitor_vvc.sv"
`include "i2c_scoreboard_vvc.sv"
`include "i2c_global.sv"
`include "mightypix_tests_vvc.sv"

`include "address_space_pkg.sv"

// ------------------------------------------------------------------- 
// 
// Testbench definitions
//                  
// -------------------------------------------------------------------



// watchdog
`define TIMEOUT     10_000_000
`define FATAL_LEVEL 2

// ------------------------------------------------------------------- 
// 
// Testbench module
//                  
// ------------------------------------------------------------------- 

module i2c_tb;

import address_space_pkg::*;

parameter ADDR_BW = 11;
parameter ID_BW = 3;

// tb interfaces
i2c_wire_if i2c_master_wire_if();
i2c_reg_if #(.ADDR_BW(ADDR_BW)) i2c_reg_if_A();
i2c_reg_if #(.ADDR_BW(ADDR_BW)) i2c_reg_if_B();

// wire pullup
pullup(i2c_master_wire_if.scl);
pullup(i2c_master_wire_if.sda);

// tb clock generation
//synced Clocks

bit clk_640M;
bit clk_320M;
bit clk_40M;
always #0.5 clk_640M <= ~clk_640M;
always #1 clk_320M <= ~clk_320M;
always #8 clk_40M <= ~clk_40M;
//bit clk = 0;
//always  #1 clk = ~clk;

// reset
bit res_n;

// mailboxes
mailbox i2c_mailbox = new();
mailbox register_mailbox = new();

// events
event stopE;
event restartE;

// yyy 
wire nackA;
wire nackB;

// DUT connections
i2c_slave_top #(.ID_BW(ID_BW), .ADDR_BW(ADDR_BW)) i2c_decoderA(
    // operation
    .clk(clk_40M),
    .res_n(res_n),

    // i2c
    .scl(i2c_master_wire_if.scl),
    .sda(i2c_master_wire_if.sda),

    // iputs
    .chip_id(`CHIP_ID_A),

    // general
    .register_valid(i2c_reg_if_A.register_valid),
    .register_address(i2c_reg_if_A.register_address), 
    
    // read if
    .register_read_data(i2c_reg_if_A.register_read_data),
    .register_read(i2c_reg_if_A.register_read), 

    // write if
    .register_write_data(i2c_reg_if_A.register_write_data),
    .register_write(i2c_reg_if_A.register_write),

    .nack(nackA)
);

// DUT connections
i2c_slave_top #(.ID_BW(ID_BW), .ADDR_BW(ADDR_BW)) i2c_decoderB(
    // operation
    .clk(clk_40M),
    .res_n(res_n),

    // i2c
    .scl(i2c_master_wire_if.scl),
    .sda(i2c_master_wire_if.sda),

    // iputs
    .chip_id(`CHIP_ID_B),

    // general
    .register_valid(i2c_reg_if_B.register_valid),
    .register_address(i2c_reg_if_B.register_address), 
    
    // read if
    .register_read_data(i2c_reg_if_B.register_read_data),
    .register_read(i2c_reg_if_B.register_read), 

    // write if
    .register_write_data(i2c_reg_if_B.register_write_data),
    .register_write(i2c_reg_if_B.register_write),

    .nack(nackB)
);


`ifdef USE_REGISTER
    

    shift_register_if shift_register();
    addr_space_if #(.CONFIG_BIT_TOTAL(CONFIG_BIT_TOTAL), .READOUT_WIDTH(READOUT_WIDTH)) addr_space_if_A(.shift_register_if_A(shift_register));
    //addr_space_if #(.CONFIG_BIT_TOTAL(CONFIG_BIT_TOTAL), .READOUT_WIDTH(READOUT_WIDTH)) addr_space_if_A();

    // QConfig
    //wire [(CONFIG_BIT_TOTAL-1):0] QConfig;
    wire [(CONFIG_BIT_TOTAL-1):0] QConfigN;

    //reg [(CONFIG_BIT_TOTAL-1):0] QConfig;
    //reg [7:0] fsm_state;
    reg [READOUT_WIDTH-1:0] readoutQ[$];
    //reg [READOUT_WIDTH-1:0] readout;
    //wire rd;

    /*
    initial begin
        //assign QConfig = 62'h14_a3_bd_1c_45_3B_A0_66;
        addr_space_if_A.fsm_state <= 8'h1b;
        //readout <= 30'h01_45_34_09;

        readoutQ.push_back(30'h01_02_03_04);
        readoutQ.push_back(30'h05_06_07_08);
        readoutQ.push_back(30'h09_10_11_12);
    end

    always @ (posedge rd) begin
        addr_space_if_A.readout = readoutQ.pop_front();
    end*/

    
    

    //wire use_i2c;
    //wire [7:0] tfc_from_i2c;
    //wire use_tfc_from_i2c;

    reg [29:0] fifo_data_in;
    wire [29:0] fifo_data_out;

    reg wr = 0;
    wire rd;

    wire fifo_full;

    assign addr_space_if_A.readout = fifo_data_out;

    // write FIFO with random data
    task write_FIFO();
        wr <= 0;
        repeat (10) begin
            @ (negedge clk_640M);
            fifo_data_in <= $urandom();
            wr <= 1;
            @ (posedge clk_640M);
        end
        wr <= 0;
    endtask

    //FSM to I2C FIFO
    async_fifo #(.WIDTH(30),.PTR_WIDTH(3),.FALLTROUGH(0))
    async_fifo_I(

        // Write
        .data_in(fifo_data_in),
        .wr(wr),
        .wr_clk(clk_640M),

        // Read
        .data_out(fifo_data_out),
        .rd(rd),
        .rd_clk(clk_40M),

        // Flags
        .full(fifo_full),
        .empty(addr_space_if_A.fifo_empty),

        .res_n(res_n)
    );


    register_interface_top #(.ADDR_BW(ADDR_BW), .READOUT_WIDTH(READOUT_WIDTH)) register_interface_A (
        // operation
        .clk(clk_40M),
        .res_n(res_n),

        
        .QConfig(addr_space_if_A.QConfig),
        .fsm_state(addr_space_if_A.fsm_state),

        // tfc over i2c
        .tfc_from_i2c(addr_space_if_A.tfc_from_i2c),
        .tfc_from_i2c_ready(addr_space_if_A.tfc_from_i2c_ready),
        .use_tfc_from_i2c(addr_space_if_A.use_tfc_from_i2c),

        // i2c flag
        .use_i2c(addr_space_if_A.use_i2c),

        // shift regsiter
        .sin(shift_register.sin),
        .ck1(shift_register.ck1),
        .ck2(shift_register.ck2),
        .ldA(shift_register.ldA),
        .ldB(shift_register.ldB),
        .ldC(shift_register.ldC),
        .ldD(shift_register.ldD),
        .ldE(shift_register.ldE),

        // general
        .register_valid(i2c_reg_if_A.register_valid),
        .register_address(i2c_reg_if_A.register_address), 
        
        // read if
        .register_read_data(i2c_reg_if_A.register_read_data),
        .register_read(i2c_reg_if_A.register_read), 

        // write if
        .register_write_data(i2c_reg_if_A.register_write_data),
        .register_write(i2c_reg_if_A.register_write),

        // fifo
        .readout(addr_space_if_A.readout),
        .fifo_empty(addr_space_if_A.fifo_empty),
        .rd(rd),

        .ptr_reset(nack)
    );


    // SIn
    wire [CONFIG_BIT_TOTAL:0] SInConfig;
    assign SInConfig[0] = shift_register.sin;

    generate 
        genvar j;

        for (j=0; j<CONFIG_BIT_TOTAL; j=j+1) begin: GenConfigBit
        
            ConfigBit ConfigBit_I(  
                .Ck1(shift_register.ck1),
                .Ck2(shift_register.ck2),
                .Ld(shift_register.ldA),
                .SIn(SInConfig[j]),
                .RB(1'b0),
                .SOut(SInConfig[j+1]),
                .Q(addr_space_if_A.QConfig[j]),
                .QB(QConfigN[j])
            );
        
        end: GenConfigBit
    endgenerate
`endif

// ------------------------------------------------------------------- 
// 
// Misc functions
//                  
// ------------------------------------------------------------------- 

// testbench reset
task reset_action;
    res_n <= 0;
    repeat(8)
        @(posedge clk_640M);

    res_n <= 1;
    repeat(8)
        @(posedge clk_640M);
endtask

// timeout in case of simulation error
task watchdog_monitor;
    forever 
        #(`TIMEOUT) $fatal(`FATAL_LEVEL);
endtask



// ------------------------------------------------------------------- 
// 
// Testbench main
//                  
// ------------------------------------------------------------------- 

// xxx address unknown at start

initial begin
    // classes
    automatic lpGBT lpGBT_A = new(i2c_master_wire_if);
        
    automatic I2CMonitor I2CMonitor_A = new(i2c_master_wire_if, i2c_mailbox, stopE, restartE);
    
    automatic I2CScoreboard I2CScoreboard_A = new(i2c_mailbox, register_mailbox, stopE, restartE);
    automatic MightyPixTest MightyPixTest_A = new();

    `ifndef USE_REGISTER 
        automatic MightyPix MightyPix_A = new(i2c_master_wire_if, i2c_reg_if_A);
        automatic MightyPix MightyPix_B = new(i2c_master_wire_if, i2c_reg_if_B);
        automatic MightypixMonitor MightypixMonitor_A = new(i2c_master_wire_if, i2c_reg_if_A, register_mailbox, `CHIP_ID_A, stopE);
        automatic MightypixMonitor MightypixMonitor_B = new(i2c_master_wire_if, i2c_reg_if_B, register_mailbox, `CHIP_ID_B, stopE);
    `else
        automatic MightypixMonitor MightypixMonitor_A = new(i2c_master_wire_if, i2c_reg_if_A, addr_space_if_A, register_mailbox, `CHIP_ID_A, stopE);
        //automatic MightypixMonitor MightypixMonitor_A = new(i2c_master_wire_if, i2c_reg_if_B, addr_space_if_B, register_mailbox, `CHIP_ID_B, stopE);
        //automatic MightypixMonitor MightypixMonitor_B = new(i2c_master_wire_if, i2c_reg_if_B, register_mailbox, `CHIP_ID_B);
    `endif

    // reset action
    reset_action();

    // run all threads
    fork
        lpGBT_A.run();
        
        I2CMonitor_A.run();
        MightypixMonitor_A.run();
        `ifndef USE_REGISTER
            MightypixMonitor_B.run(); //yyy fix later
        `endif
        I2CScoreboard_A.run();
        watchdog_monitor();
    join_none

    // register stimulation not needed if register inserted
    `ifndef USE_REGISTER
        fork
            MightyPix_A.run();
            MightyPix_B.run();
        join_none
    `endif


    // test basic functionality
    `ifdef USE_REGISTER
        write_FIFO();

        MightyPixTest_A.shift_register_test(lpGBT_A);

        // refill FIFO and test
        // checks if emptying of FIFO leads to correct restart
        write_FIFO();
        MightyPixTest_A.shift_register_test(lpGBT_A);
    `else
        MightyPixTest_A.functionality_test(lpGBT_A);
    `endif

    #20 $finish();
end

endmodule