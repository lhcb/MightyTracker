-access 
+rw 
-sv 

## Defines
-define SIMULATION 
-define USE_REGISTER

## Headers
+incdir+$BASE/building_blocks/i2c

## Files
$BASE/rtl/i2c/i2c_slave_top.sv 
$BASE/rtl/i2c/i2c_receiver.v 
$BASE/rtl/i2c/i2c_sender.v 
$BASE/rtl/i2c/register_interface_top.sv
$BASE/rtl/i2c/ranged_mux.sv

## Testbench
$BASE/verification/i2c/i2c_tb.sv
