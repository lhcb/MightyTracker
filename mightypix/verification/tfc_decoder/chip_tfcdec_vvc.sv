`ifndef chip_tfcdec_vvc
    `define chip_tfcdec_vvc

    //`define TFC_LOG_OK
    //`define TFC_LOG_ALL
    //`define TFC_LOCK_DRIVER

    // tfcdec vvc: WIP

    `include "tfc_cmd_pkg.sv"

    import tfc_cmd_pkg::*;

    // TFC Interface
    // ----------------------------------------------------
    interface chip_tfcdec_if(input logic clk);

        logic tfc_in;
        logic tfc_lock;

        logic tfc_cmd_received;

        logic [2:0] Cnt8;

        logic [4:0] tfc_cmd_dec_out;
        logic tfc_wd_reset;

        logic tfc_test_wire;

        logic res_n;

        logic [7:0] i2c_tfc_data;
        logic i2c_tfc_ready;
        logic use_I2C_tfc;

        clocking driver_cb @ (posedge clk);
            output tfc_in, tfc_lock;
            output tfc_test_wire;
            input tfc_cmd_dec_out, Cnt8;
        endclocking : driver_cb

        clocking monitor_cb @ (posedge clk);
            input tfc_in, tfc_lock;
            input tfc_cmd_dec_out, Cnt8;
        endclocking : monitor_cb
    endinterface

    // Input Mailbox struct
    // ----------------------------------------------------
    typedef struct{
        logic [7:0] tfc_reg;
        logic res_n;
        logic tfc_lock;
        logic use_I2C_tfc;
        logic [7:0] i2c_tfc_data;
    } Mailbox_in_struct;

    // TFC Rand
    // ----------------------------------------------------
    class tfc_rand;
        rand bit [7:0] tfc_cmd;

        rand integer tfc_lock_time;

        rand integer tfc_offset_to_40Mhz;

        constraint legal {
            $countones(tfc_cmd) < 2;

            tfc_lock_time <= 300;
            tfc_lock_time >= 1;

            tfc_offset_to_40Mhz <= 7;
            tfc_offset_to_40Mhz >= 0;
        }
    endclass

    // Scoreboard
    // ----------------------------------------------------
    class tfc_Scoreboard;

        mailbox tfc_input_mbx;
        mailbox tfc_output_mbx;

        virtual chip_tfcdec_if tfcdec_if;

        function new(virtual chip_tfcdec_if tfcdec_if, mailbox tfc_input_mbx, mailbox tfc_output_mbx);
            this.tfc_input_mbx = tfc_input_mbx;
            this.tfc_output_mbx = tfc_output_mbx;
            this.tfcdec_if = tfcdec_if;
        endfunction

        task main();

            logic [4:0] output_mbx_item;

            string match;

            int count_fail = 0;
            int count_success = 0;

            int count_total = 0;
            int count_display = 0;

            Mailbox_in_struct input_mbx_struct;

            logic [7:0] tfc_command;
            logic [4:0] tfc_decoded;

            fork
                forever begin
                    tfc_input_mbx.get(input_mbx_struct);
                    tfc_output_mbx.get(output_mbx_item);

                    tfc_command = input_mbx_struct.tfc_reg;
                    tfc_decoded = output_mbx_item;

                    if(!input_mbx_struct.res_n) begin
                        `ifdef TFC_LOG_OK
                            $display("OK \tTFC \t\tScoreboard: IN \t%b, RESET at \t%0t ps", tfc_command, $time);
                        `endif
                        count_success++;
                    end
                    else if(input_mbx_struct.tfc_lock) begin
                        `ifdef TFC_LOG_OK
                            $display("OK \tTFC \t\tScoreboard: IN \t%b, TFC_LOCK \t%b at \t%0t ps", tfc_command, tfc_decoded, $time);
                        `endif
                        count_success++;
                    end
                    else if(input_mbx_struct.use_I2C_tfc && (output_mbx_item == $clog2(input_mbx_struct.i2c_tfc_data + 1))) begin
                        `ifdef TFC_LOG_OK
                            $display("OK \tTFC \t\tScoreboard: IN \t%b, TFC_I2C \t%b at \t%0t ps", tfc_command, tfc_decoded, $time);
                        `endif
                        count_success++;
                    end
                    else if((input_mbx_struct.tfc_reg[7:6]) == TFC_ALIGNBITS && (input_mbx_struct.tfc_reg[0] == TFC_ALIGNBIT_LSB)) begin //only check if aligned
                        `ifdef TFC_LOG_OK
                            $display("OK \tTFC \t\tScoreboard: IN \t%b, OUT \t%b at \t%0t ps", tfc_command, tfc_decoded, $time);
                        `endif
                        count_success++;
                    end
                    else if(input_mbx_struct.use_I2C_tfc) begin
                            $display("fail \tTFC \t\tScoreboard: unexpected command via I2C \t%b, OUT \t%b at \t%0t ps", input_mbx_struct.i2c_tfc_data, tfc_decoded, $time);
                        count_fail++;
                    end
                    else begin
                        $display("fail \tTFC \t\tScoreboard: IN \t%b, OUT \t%b at \t%0t ps", input_mbx_struct.tfc_reg, tfc_decoded, $time);
                        count_fail++;
                    end
                end

                forever begin
                    wait(count_success + count_fail >= (count_display + 250));
                        count_display = count_success + count_fail;
                        $display("TFC Summary: SUCCESSFULL: %0d \tFAILED: %0d", count_success, count_fail);
                end
            join
        endtask
    endclass

    // TFC Output monitor
    // ----------------------------------------------------
    class chip_tfcdec_output_monitor;

        mailbox tfc_output_mbx;

        virtual chip_tfcdec_if tfcdec_if;

        event sync_event;

        function new(virtual chip_tfcdec_if tfcdec_if, mailbox tfc_output_mbx, event sync_event);
            this.tfc_output_mbx = tfc_output_mbx;
            this.tfcdec_if = tfcdec_if;
            this.sync_event = sync_event;
        endfunction

        task tfc_output_monitor(ref logic clk_320M, ref logic clk_40M);

            @(posedge clk_40M);

            forever begin

                @(posedge clk_40M);

                //Wait until last 320M cycle before next 40M posedge
                repeat(7) begin
                    @(clk_320M);
                end

                `ifdef TFC_LOG_ALL
                    $display("CMD Decoder output at %0t ps: 5'b%b",$time, tfcdec_if.tfc_cmd_dec_out);
                `endif

                tfc_output_mbx.put(tfcdec_if.tfc_cmd_dec_out);
            end

        endtask
    endclass

    // TFC Input Monitor
    // ----------------------------------------------------
    class chip_tfcdec_driver_monitor;

        mailbox tfc_input_mbx;

        virtual chip_tfcdec_if tfcdec_if;

        event sync_event;

        function new(virtual chip_tfcdec_if tfcdec_if, mailbox tfc_input_mbx, event sync_event);
            this.tfc_input_mbx = tfc_input_mbx;
            this.tfcdec_if = tfcdec_if;
            this.sync_event = sync_event;
        endfunction

        task tfc_lock_monitor();
            forever begin

                    @( posedge tfcdec_if.tfc_lock);
                            $display("Invoke tfc_lock at %0t ps", $time);
                    @( negedge tfcdec_if.tfc_lock);
                            $display("Release tfc_lock at %0t ps", $time);
            end
        endtask

        task tfc_input_monitor(ref logic clk_320, ref logic clk_40M);

            Mailbox_in_struct mailbox_in_struct;
            logic [7:0] tfc_reg;

            @(posedge clk_40M);

            repeat(3) begin
                @(posedge clk_320);
            end

            forever begin

                //@(posedge clk_40M);
                repeat (8) begin
                    @(negedge clk_320);
                    tfc_reg = tfc_reg << 1;
                    tfc_reg[0] = tfcdec_if.tfc_in;

                end

                //To mailbox struct
                mailbox_in_struct.tfc_reg = tfc_reg;
                mailbox_in_struct.res_n = tfcdec_if.res_n;
                mailbox_in_struct.tfc_lock = tfcdec_if.tfc_lock;
                mailbox_in_struct.use_I2C_tfc = tfcdec_if.use_I2C_tfc;
                mailbox_in_struct.i2c_tfc_data = tfcdec_if.i2c_tfc_data;

                `ifdef TFC_LOG_ALL
                    $display ("Generated tfc_word at %0t ps: 8'b%b", $time, tfc_reg);
                `endif

                tfc_input_mbx.put(mailbox_in_struct);

            end

        endtask

    endclass

    // TFC Input driver
    // ----------------------------------------------------
    class chip_tfcdec_driver;

        virtual chip_tfcdec_if tfcdec_if;

        function new(virtual chip_tfcdec_if tfcdec_if);
            this.tfcdec_if = tfcdec_if;
        endfunction

        task tfc_command_driver(ref logic clk_320, ref logic clk_40);

            tfc_rand tfc_word = new();

            logic [7:0] word_copy;

            tfcdec_if.tfc_in = 0;

            $display ("Generate random TFC Commands");


            @(posedge clk_40);

            forever begin

                void'(tfc_word.randomize());
                word_copy = tfc_word.tfc_cmd;

                `ifdef TFC_LOG_ALL
                    $display ("Driver Generated tfc_word at %0t ps: 8'b%b", $time, word_copy);
                `endif

                repeat (8) begin
                    tfcdec_if.tfc_in = tfc_word.tfc_cmd[7];
                    tfc_word.tfc_cmd = tfc_word.tfc_cmd << 1;
                    @(posedge clk_320);
                end

            end
        endtask

        task tfc_gen_command(ref bit [7:0] cmd, ref logic clk_320, ref logic clk_40);

            bit [7:0] cmd_in;

            tfc_rand tfc_offset = new();
            void'(tfc_offset.randomize());

            tfcdec_if.tfc_in = 0;

            //Wait for clk_40 posedge, send MSB first @320MHz
            @(posedge clk_40);

            //Wait some cycles rand (0-7)
            repeat(3) begin
                @(posedge clk_320);
            end

            forever begin

                cmd_in=cmd;
                //$display ("Driver Generated tfc_word at %0t ps: 8'b%b", $time, word_copy);

                repeat (8) begin
                    tfcdec_if.tfc_in = cmd_in[7];
                    cmd_in = cmd_in[6:0] << 1;
                    @(posedge clk_320);

                end

            end

        endtask

        task tfc_lock_driver(ref logic clk_320, ref logic res_n);

            tfc_rand tfc_lock = new();

            tfcdec_if.tfc_lock = 0;
            @(posedge res_n);

            $display ("Generate random TFC Lock periods");

            forever begin
                @(tfcdec_if.tfc_cmd_dec_out); //wait for new command from decoder

                void'(tfc_lock.randomize());

                @(posedge clk_320);//Wait for one clock cycle to check, TFC Lock waitstate
                //Set tfc_lock
                tfcdec_if.tfc_lock = 1;

                //wait for random 1-300 periods of clk_320
                repeat(tfc_lock.tfc_lock_time) begin
                    @(posedge clk_320);
                end

                //release tfc_lock
                tfcdec_if.tfc_lock = 0;
            end
        endtask
    endclass: chip_tfcdec_driver

    // TFC Testclass
    // ----------------------------------------------------
    class top_tfcdec_test;

        //mon + driver handles
        automatic chip_tfcdec_driver_monitor      monitor_in;
        automatic chip_tfcdec_output_monitor      monitor_out;
        automatic chip_tfcdec_driver              driver;
        automatic tfc_Scoreboard                  scb;


        mailbox tfc_input_mbx;
        mailbox tfc_output_mbx;

        virtual chip_tfcdec_if tfcdec_if;

        event sync_event;

        function new(virtual chip_tfcdec_if tfcdec_if);
            this.tfcdec_if = tfcdec_if;

            //creating the mailbox (Same handle will be shared across generator and driver)
            tfc_input_mbx   = new();
            tfc_output_mbx  = new();

            //Monitors
            monitor_in      = new(tfcdec_if, tfc_input_mbx, sync_event);
            monitor_out     = new(tfcdec_if, tfc_output_mbx, sync_event);

            //Scoreboard
            scb             = new(tfcdec_if,tfc_input_mbx,tfc_output_mbx);

            //Driver
            driver          = new(tfcdec_if);
        endfunction

        task main(ref logic clk_320M, ref logic clk_40M, ref logic res_n);
            $display("Start TFC Scoreboard with Drivers + Monitors");
            fork
                driver.tfc_command_driver(clk_320M, clk_40M);

                //Drive tfc_lock (could be driven by the Readout FSM)
                begin
                `ifdef TFC_LOCK_DRIVER
                    tfcdec_if.tfc_lock = 0;
                `else
                    driver.tfc_lock_driver(clk_320M, res_n);
                `endif
                end

                //Scoreboard
                scb.main();

                //Monitors
                monitor_in.tfc_input_monitor(clk_320M, clk_40M);
                monitor_in.tfc_lock_monitor();
                monitor_out.tfc_output_monitor(clk_320M, clk_40M);

            join
        endtask

        task monitor_only(ref logic clk_320M, ref logic clk_40M);
            $display("Start TFC Scoreboard in Monitor-Mode");
            fork
                //Scoreboard
                scb.main();

                //Monitors
                monitor_in.tfc_input_monitor(clk_320M, clk_40M);
                monitor_out.tfc_output_monitor(clk_320M, clk_40M);

                `ifdef TFC_LOG_OK
                    monitor_in.tfc_lock_monitor();
                `endif

            join
        endtask

    endclass

`endif