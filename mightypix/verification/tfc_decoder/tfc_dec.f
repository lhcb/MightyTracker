## This Is a DUT Testbench, which is a wrapper around the Chip and performs some initialisations so that is can be quickly integrated
## into a higher level simulation for first tests.

## Main Design
$BASE/rtl/tfc_decoder/tfc_decoder_top.sv
$BASE/rtl/tfc_decoder/tfc_rec_align.sv
$BASE/rtl/tfc_decoder/tfc_fereset.sv
$BASE/rtl/tfc_decoder/tfc_dec.sv

## Modules for verification
$BASE/rtl/serializer/PhaseCounter.sv

## Standard Cells  Models for Simulation
-makelib .:cells
$BASE/implementation/ams_h18/h18_CORELIB_HV_heiko.v
-endlib 

## Testbench
$BASE/verification/tfc_decoder/tfc_dec_tb.sv

+incdir+$BASE/rtl/tfc_decoder
+incdir+$BASE/verification/tfc_decoder

## Standard Args for Cadence XCELIUM
-sv
-access r+w
-timescale 1ns/10ps
-define SIMULATION
#-define VERBOSE