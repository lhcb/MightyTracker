`timescale 1ns/1ps

`include "chip_tfcdec_vvc.sv"

// TFC_decoder Testbench

module tfc_dec_tb;


logic clk_320M;
logic clk_40M;

chip_tfcdec_if tfcdec_if(clk_320M);

bit [7:0] tfc_command = 8'b1100_0000;


PhaseCounter PhaseCounter_I (
    .sig_i(clk_40M),
    .clk_i(clk_320M),
    .resN_i(tfcdec_if.res_n),
    .cnt8_o(tfcdec_if.Cnt8)
);

assign tfcdec_if.tfc_cmd_dec_out = tfc_decoder_top_I.tfc_dec_I.tfc_bits;

tfc_decoder_top tfc_decoder_top_I (
        .clk_in(clk_320M),//(clk_320M),
        .clk_40M(clk_40M),

        .tfc_in(tfcdec_if.tfc_in),

        .res_n(tfcdec_if.res_n),
        .align_res_n(tfcdec_if.res_n),

        .Cnt8(tfcdec_if.Cnt8),

        //Lock input, ignores new commands while high
        .tfc_lock(1'b0),

        //watchdog timeout reset
        .wd_reset(tfcdec_if.tfc_wd_reset),

        //Decoded CMD out
        .cmd_mux(cmd_mux),
        .cmd_cal(cmd_cal),
        .cmd_tsgen({snapshot,bxreset}),

        .serializer_res_n(TFC_Ser_res_n),
        .readout_res_n(TFC_RO_res_n),
        .i2c_res_n(TFC_I2C_res_n),

        //Interrupt out
        .cmd_received(tfcdec_if.tfc_cmd_received),

        .tfc_from_i2c(tfcdec_if.i2c_tfc_data),
        .tfc_from_i2c_ready(tfcdec_if.i2c_tfc_ready),
        .use_tfc_from_i2c(tfcdec_if.use_I2C_tfc)
    );

initial begin
        automatic top_tfcdec_test top_tfcdec_test = new(tfcdec_if);
        automatic chip_tfcdec_driver top_tfcdec_driver = new(tfcdec_if);

        fork
            top_tfcdec_test.monitor_only(clk_320M, clk_40M);
            top_tfcdec_driver.tfc_gen_command(tfc_command, clk_320M, clk_40M);

            begin
                //Init
                clk_320M = 1;
                clk_40M = 1;
                tfcdec_if.res_n = 0;
                tfcdec_if.use_I2C_tfc = 0;
                #100 tfcdec_if.res_n = 1;
                
                #100
                tfc_command = 8'b1100_0000;
                #1000
                tfc_command = 8'b1100_0010; 
                #1000

                //Generate 2 failed transactions with incorrect alignbits
                tfc_command = 8'b1000_0000;
                #32

                tfc_command = 8'b1100_0010;

                //#300 res_n = 0;
                //#10 res_n = 1;

            end
        join
end

always begin
    #10000 $stop("timeout");
end

//synced Clocks
always #1 clk_320M <= ~clk_320M;
always #8 clk_40M <= ~clk_40M;

endmodule
