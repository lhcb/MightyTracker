`ifndef chip_matrix_vvc
    `define chip_matrix_vvc

    `ifdef EOC_MODEL
        `include "eoc_model_pkg.sv"
        `include "load_data.sv"

        import eoc_model_pkg::*;
    `endif

    interface chip_matrix_if;

        // Readout Control From FSM
        bit LdCol;
        bit RdCol;
        bit LdPix;
        bit PullDN;

        // Timestamps from Digital
        bit [11:0]  TSToDet;
        bit [11:0]  NegTSToDet;
        bit [6:0]   TSToDetTDC;
        bit [2:0]   FineTSToDet;

        // Returned Current addresses and timestamp values
        logic        PriFromDet;
        logic [8:0]  RowAddFromDet;//10 bits
        logic [7:0]  ColAddFromDet;
        logic [11:0] TSFromDet;
        logic [11:0] NegTSFromDet;
        logic [6:0]  TDCFromDet; //10 bits
        logic [6:0]  AmpFromDet; //7 bits
        logic [2:0]  FineTSFromDet;





    endinterface

    class chip_pixel_transaction;

        rand logic [8:0] row;
        rand logic [7:0] col;

        rand logic [11:0] TSFromDet;
        rand logic [11:0] NegTSFromDet;
        rand logic [6:0]  TDCFromDet; //10 bits
        rand logic [6:0]  AmpFromDet; //7 bits
        rand logic [2:0]  FineTSFromDet;

        int hits;

        int readout_time;

       /*
       function void unpack_addr_ts3(logic [31:0] pkt);
            TS3FromDet = pkt[6:0];
            row        = pkt[16:7];
            col        = pkt[21:17];
        endfunction

        function void unpack_ts2_ts(logic [31:0] pkt);
            TS2FromDet = pkt[9:0];
            TSFromDet  = pkt[29:10];
        endfunction
        */
        /* constraint legal {

            NegTSFromDet < TSFromDet+2;
            NegTSFromDet >= TSFromDet;
        } */

        function logic [11:0] gray2bin (logic [11:0] gray);

            logic [11:0] bin;

            int i;

            bin[11] = gray[11];


            for(i = 10; i >= 0; i--) begin: gray_2_bin
                bin[i] = bin[i + 1] ^ gray[i];
            end

            return bin;

        endfunction

        function logic [6:0] gray2bin7 (logic [6:0] gray);

            logic [6:0] bin;

            int i;

            bin[6] = gray[6];


            for(i = 5; i >= 0; i--) begin: gray_2_bin
                bin[i] = bin[i + 1] ^ gray[i];
            end

            return bin;

        endfunction


        function void print(string printer);
            $display("[%s pixel] x=%x,y=%x,ts=%x,tdc=%x,amp=%x,fine=%x",printer,col,row,TSFromDet,TDCFromDet,AmpFromDet,FineTSFromDet);
        endfunction

        function int calc_tot(logic [11:0] TSFromDet, logic [6:0] AmpFromDet);
            int tot = (gray2bin({TSFromDet[11:7], AmpFromDet})-gray2bin(TSFromDet))*25;

            return tot;
        endfunction

        function void print2(string printer);
            $display("[%s pixel]\t#: %d\tColumn: %0d\tRow: %0d\tToT_ns: %0d\tTimestamp: %0d\tAmp: %0d\tFineTS: %0d at %0t", printer, hits, col, row, calc_tot(TSFromDet, AmpFromDet), gray2bin(TSFromDet), gray2bin7(AmpFromDet), FineTSFromDet, $time);
        endfunction


    endclass

    /**
     * Monitors Chip-Matrix interface to gather generated
     * @author rleys
     *
     */
    class chip_matrix_monitor;

        integer silent                = 0;
        mailbox received_transactions = new();

        task monitor(virtual chip_matrix_if matrix_if);
            chip_pixel_transaction current_transaction = new();

            forever
            begin
                // Wait for PriFromDet to be 1
                @(posedge matrix_if.PriFromDet);

                // On Each ReadCol Toggling, sample data
                while(matrix_if.PriFromDet==1'b1)
                begin
                    @(posedge matrix_if.RdCol);

                    current_transaction.col           = matrix_if.ColAddFromDet;
                    current_transaction.row           = matrix_if.RowAddFromDet;
                    current_transaction.TSFromDet     = matrix_if.TSFromDet;
                    current_transaction.NegTSFromDet  = matrix_if.NegTSFromDet;
                    current_transaction.TDCFromDet    = matrix_if.TDCFromDet;
                    current_transaction.AmpFromDet    = matrix_if.AmpFromDet;
                    current_transaction.FineTSFromDet = matrix_if.FineTSFromDet;

                    received_transactions.put(current_transaction);

                    //$display("[matrix-monitor] Pixel data x=%d,y=%d",current_transaction.col,current_transaction.row);
                    if (silent==0)
                        current_transaction.print("matrix-monitor");

                    current_transaction = new();

                    // Wait for RdCol to be back low so that looping will catch PriFromDet == 0 from matrix model if not random pixels are availabe anymore
                    @(negedge matrix_if.RdCol);

                end

            end


        endtask

    endclass

    /**
     * Monitors Chip-Matrix interface to gather data from Pixelmatrix model
     * @author nstriebig
     *
     */
    `ifdef EOC_MODEL
    class chip_matrix_monitor_eoc extends write_data;

        integer silent                = 0;
        mailbox received_transactions = new();

        task monitor(virtual chip_matrix_if matrix_if, input int write2file = 0, input string filename = 0);
            chip_pixel_transaction current_transaction = new();

            int hits = 0;

            forever
            begin
                // Wait for RdCol
                @(posedge matrix_if.RdCol);
                    #1; //TODO: why ?
                    hits++;

                    current_transaction.col           = matrix_if.ColAddFromDet;
                    current_transaction.row           = ~matrix_if.RowAddFromDet;
                    current_transaction.TSFromDet     = matrix_if.TSFromDet;
                    current_transaction.NegTSFromDet  = matrix_if.NegTSFromDet;
                    //current_transaction.TDCFromDet    = matrix_if.TDCFromDet;
                    current_transaction.AmpFromDet    = matrix_if.AmpFromDet;
                    current_transaction.FineTSFromDet = matrix_if.FineTSFromDet;
                    current_transaction.hits = hits;
                    current_transaction.readout_time = $time();

                    received_transactions.put(current_transaction);

                    if (silent==0) begin
                        current_transaction.print2("matrix-monitor");
                        //$display("[matrix-monitor pixel] x=%x,y=%x,ts=%x,tdc=%x,amp=%x,fine=%x", current_transaction.col, current_transaction.row, current_transaction.TSFromDet, current_transaction.TDCFromDet, current_transaction.AmpFromDet, current_transaction.FineTSFromDet);
                    end

                    if (write2file == 1)
                        write_to_file(filename, hits, current_transaction.col, current_transaction.row, current_transaction.calc_tot(current_transaction.TSFromDet, current_transaction.AmpFromDet), current_transaction.gray2bin(current_transaction.TSFromDet), current_transaction.gray2bin7(current_transaction.AmpFromDet), current_transaction.readout_time);

                    current_transaction = new();

                    // Wait for RdCol to be back low so that looping will catch PriFromDet == 0 from matrix model if not random pixels are availabe anymore
                    @(negedge matrix_if.RdCol);

            end


        endtask

    endclass
    `endif
    /**
     * Acts like the Pixel matrix to send data to digital part
     * @author rleys
     *
     */
    class chip_matrix_driver;

        chip_pixel_transaction pixels_queue[$];

        rand integer pause;
        constraint c_pause {
            pause >= 0 && pause <= 5;
        }

        task generate_pixel_data(input integer count);
            chip_pixel_transaction current_transaction = new();
            repeat(count)
            begin
                void'(current_transaction.randomize());
                pixels_queue.push_back(current_transaction);
                current_transaction = new();
            end
        endtask

        task drive_pixels_on_matrix(virtual chip_matrix_if matrix_if , input integer count);


            chip_pixel_transaction current_transaction = new();
            chip_pixel_transaction lpixels_queue[$];
            /*matrix_if.PriFromDet    = 1'b0;
            matrix_if.TSFromDet     = 0;
            matrix_if.TDCFromDet    = 0;
            matrix_if.AmpFromDet    = 0;
            matrix_if.FineTSFromDet = 0;*/

            // Main Loop
            //-------------------
            repeat(count)
            begin
                void'(current_transaction.randomize());
                lpixels_queue.push_back(current_transaction);
                current_transaction = new();
            end
            // Readout sends Load Col and then Load Pixel, when that comes, output pixel data if available
            @(posedge matrix_if.LdCol);

            #10ps if (lpixels_queue.size()>0)
            begin
                matrix_if.PriFromDet <= 1'b1;
                $display("Driving  %d pixels: ",lpixels_queue.size);
                while(lpixels_queue.size()>0)
                begin

                    @(posedge matrix_if.RdCol);
                    current_transaction     = lpixels_queue.pop_front();
                    matrix_if.ColAddFromDet     = current_transaction.col;
                    matrix_if.RowAddFromDet     = current_transaction.row;
                    matrix_if.TSFromDet         = current_transaction.TSFromDet;
                    matrix_if.NegTSFromDet      = current_transaction.NegTSFromDet;
                    matrix_if.TDCFromDet        = current_transaction.TDCFromDet;
                    matrix_if.AmpFromDet        = current_transaction.AmpFromDet;
                    matrix_if.FineTSFromDet     = current_transaction.FineTSFromDet;

                    // Pause ?
                    void'(this.randomize());
                    repeat(pause)
                    begin
                        matrix_if.PriFromDet <= 1'b0;
                        @(posedge matrix_if.LdCol);
                    end

                    // Resume
                    matrix_if.PriFromDet <= 1'b1;

                end
                matrix_if.PriFromDet <= 1'b0;


            end
        endtask

        task drive_pixel_data( ref logic res_n , input virtual chip_matrix_if matrix_if );

            chip_pixel_transaction current_transaction;

            matrix_if.PriFromDet = 1'b0;

            @(posedge res_n);
            /*matrix_if.PriFromDet = 0;
            matrix_if.TS3FromDet = 0;
            matrix_if.TS2FromDet = 0;
            matrix_if.TSFromDet  = 0;*/

            // Main Loop
            //-------------------
            forever
            begin
                // Readout sends Load Col and then Load Pixel, when that comes, output pixel data if available
                @(posedge matrix_if.LdCol);

                #10ps if (pixels_queue.size()>0)
                begin
                    matrix_if.PriFromDet <= 1'b1;
                    while(pixels_queue.size()>0)
                    begin
                        @(posedge matrix_if.RdCol);
                        current_transaction     = pixels_queue.pop_front();
                        matrix_if.ColAddFromDet     = current_transaction.col;
                        matrix_if.RowAddFromDet     = current_transaction.row;
                        matrix_if.TSFromDet         = current_transaction.TSFromDet;
                        matrix_if.TDCFromDet        = current_transaction.TDCFromDet;
                        matrix_if.AmpFromDet        = current_transaction.AmpFromDet;
                        matrix_if.FineTSFromDet     = current_transaction.FineTSFromDet;
                        matrix_if.NegTSFromDet      = current_transaction.NegTSFromDet;

                    end
                    matrix_if.PriFromDet <= 1'b0;


                end


            end


        endtask

        `ifdef EOC_MODEL
        task automatic generateHits(ref bit [N_COLUMNS-1:0] [N_HITBUFFERS-1:0] OutB, input [CA_WIDTH-1:0] col, input [ROW_WIDTH-1:0] row, input int ToT_ns, input int n_hits=1);
            begin
                int i;
                //$display("Gen hit col: %0d row: %0d at %0t", col, row, $time);

                for(i=0; i<n_hits; i++) begin
                    OutB[col][row+i] = 0;
                end

                # (ToT_ns * 1000ps)

                for(i=0; i<n_hits; i++) begin
                    OutB[col][row+i] = 1;
                end
            end
        endtask
        `endif

    endclass

`endif
