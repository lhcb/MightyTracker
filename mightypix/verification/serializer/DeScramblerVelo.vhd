-- IEEE VHDL standard library:
library ieee, work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--USE work.Constant_GWT_Declaration.all;

--USE work.detector_constant_declaration.all;

entity VeloPixDeScrambler is
        port(
                dataIn : in std_logic_vector(31 downto 0)  := (others => '0');
                dataInValid : in std_logic := '0';
                scrambleEnable : in std_logic := '0';
                parity     : in std_logic := '0';
                clock : in std_logic;
                reset	 : in  std_logic;  
                reset_counters	 : in  std_logic;  
                --nextState  : out std_logic_vector(29 downto 0);
                dataOut : out std_logic_vector(31 downto 0);
                dataOutValid : out std_logic;
                cnt8: in std_logic_vector(2 downto 0)
                --parity_error_counter :out std_logic_vector(bar_register_length_long-1 downto 0):= (others => '0')
            );
end VeloPixDeScrambler;

architecture a of VeloPixDeScrambler is

        signal state : std_logic_vector(29 downto 0);
        -------------- ALGORITHM ---------------------
        signal nextState_reg : std_logic_vector(29 downto 0) := (others => '0');
        signal parity_pipe, parity_calculated   : std_logic;
        --signal parity_counter : unsigned(bar_register_length_long-1 downto 0) := (others => '0');
        signal parity_counter_reset : std_logic := '0';
        signal s_DV : std_logic := '0';
        signal dataOut_temp : std_logic_vector(31 downto 0) := (others => '0');
begin

        --state <= nextState_reg when rising_edge(clock);--nextState_reg;
        process(clock,reset)

        begin
                if (reset = '0') then	
                        nextState_reg <= (others => '0');
                        dataOut   <= (others => '0');
                        dataOut_temp   <= (others => '0');
                elsif rising_edge(clock) then
                                dataOut_temp(0)  <= dataIn(0)  XOR nextState_reg(0)  XOR nextState_reg(1)  XOR nextState_reg(15) XOR nextState_reg(16);
                                dataOut_temp(1)  <= dataIn(1)  XOR nextState_reg(1)  XOR nextState_reg(2)  XOR nextState_reg(16) XOR nextState_reg(17);
                                dataOut_temp(2)  <= dataIn(2)  XOR nextState_reg(2)  XOR nextState_reg(3)  XOR nextState_reg(17) XOR nextState_reg(18);
                                dataOut_temp(3)  <= dataIn(3)  XOR nextState_reg(3)  XOR nextState_reg(4)  XOR nextState_reg(18) XOR nextState_reg(19);
                                dataOut_temp(4)  <= dataIn(4)  XOR nextState_reg(4)  XOR nextState_reg(5)  XOR nextState_reg(19) XOR nextState_reg(20);
                                dataOut_temp(5)  <= dataIn(5)  XOR nextState_reg(5)  XOR nextState_reg(6)  XOR nextState_reg(20) XOR nextState_reg(21);
                                dataOut_temp(6)  <= dataIn(6)  XOR nextState_reg(6)  XOR nextState_reg(7)  XOR nextState_reg(21) XOR nextState_reg(22);
                                dataOut_temp(7)  <= dataIn(7)  XOR nextState_reg(7)  XOR nextState_reg(8)  XOR nextState_reg(22) XOR nextState_reg(23);
                                dataOut_temp(8)  <= dataIn(8)  XOR nextState_reg(8)  XOR nextState_reg(9)  XOR nextState_reg(23) XOR nextState_reg(24);
                                dataOut_temp(9)  <= dataIn(9)  XOR nextState_reg(9)  XOR nextState_reg(10) XOR nextState_reg(24) XOR nextState_reg(25);
                                dataOut_temp(10) <= dataIn(10) XOR nextState_reg(10) XOR nextState_reg(11) XOR nextState_reg(25) XOR nextState_reg(26);
                                dataOut_temp(11) <= dataIn(11) XOR nextState_reg(11) XOR nextState_reg(12) XOR nextState_reg(26) XOR nextState_reg(27);
                                dataOut_temp(12) <= dataIn(12) XOR nextState_reg(12) XOR nextState_reg(13) XOR nextState_reg(27) XOR nextState_reg(28);
                                dataOut_temp(13) <= dataIn(13) XOR nextState_reg(13) XOR nextState_reg(14) XOR nextState_reg(28) XOR nextState_reg(29);
                                dataOut_temp(14) <= dataIn(14) XOR nextState_reg(14) XOR nextState_reg(15) XOR nextState_reg(29) XOR dataIn(0);
                                dataOut_temp(15) <= dataIn(15) XOR nextState_reg(15) XOR nextState_reg(16) XOR dataIn(0)  		XOR dataIn(1);
                                dataOut_temp(16) <= dataIn(16) XOR nextState_reg(16) XOR nextState_reg(17) XOR dataIn(1)  		XOR dataIn(2);
                                dataOut_temp(17) <= dataIn(17) XOR nextState_reg(17) XOR nextState_reg(18) XOR dataIn(2)  		XOR dataIn(3);
                                dataOut_temp(18) <= dataIn(18) XOR nextState_reg(18) XOR nextState_reg(19) XOR dataIn(3)  		XOR dataIn(4);
                                dataOut_temp(19) <= dataIn(19) XOR nextState_reg(19) XOR nextState_reg(20) XOR dataIn(4)  		XOR dataIn(5);
                                dataOut_temp(20) <= dataIn(20) XOR nextState_reg(20) XOR nextState_reg(21) XOR dataIn(5)  		XOR dataIn(6);
                                dataOut_temp(21) <= dataIn(21) XOR nextState_reg(21) XOR nextState_reg(22) XOR dataIn(6)  		XOR dataIn(7);
                                dataOut_temp(22) <= dataIn(22) XOR nextState_reg(22) XOR nextState_reg(23) XOR dataIn(7)  		XOR dataIn(8);
                                dataOut_temp(23) <= dataIn(23) XOR nextState_reg(23) XOR nextState_reg(24) XOR dataIn(8)  		XOR dataIn(9);
                                dataOut_temp(24) <= dataIn(24) XOR nextState_reg(24) XOR nextState_reg(25) XOR dataIn(9)  		XOR dataIn(10);
                                dataOut_temp(25) <= dataIn(25) XOR nextState_reg(25) XOR nextState_reg(26) XOR dataIn(10) 		XOR dataIn(11);
                                dataOut_temp(26) <= dataIn(26) XOR nextState_reg(26) XOR nextState_reg(27) XOR dataIn(11) 		XOR dataIn(12);
                                dataOut_temp(27) <= dataIn(27) XOR nextState_reg(27) XOR nextState_reg(28) XOR dataIn(12) 		XOR dataIn(13);
                                dataOut_temp(28) <= dataIn(28) XOR nextState_reg(28) XOR nextState_reg(29) XOR dataIn(13) 		XOR dataIn(14);
                                dataOut_temp(29) <= dataIn(29) XOR nextState_reg(29) XOR dataIn(0)  	  XOR dataIn(14) 		XOR dataIn(15);
                                nextState_reg <= dataIn(29 downto 0);

                                dataOut_temp(31 downto 30) <= dataIn(31 downto 30);  -- position in gwt frame

                                if cnt8 = "100" then
                                    dataOut <= dataOut_temp;
                                end if;
-- synthesis translate_off   -- turn off scrambling for sim
                    if scrambleEnable = '0' then
                        dataOut <= dataIn;
                    end if;
-- synthesis translate_on
                        --dataOutValid <= dataInValid;
                end if;
        end process;

        par_chk : process(clock,reset)
        begin
                if reset = '1' then
                        s_DV <= '0';
                        parity_pipe <= '0';
                        parity_calculated <= '0';
                        dataOutValid <= '0';
                elsif rising_edge(clock) then
                        parity_pipe <= parity;
                        parity_calculated <= dataIn(0) xor dataIn(1) xor dataIn(2) xor dataIn(3) xor dataIn(4) xor dataIn(5) xor dataIn(6) xor
                                             dataIn(7) xor dataIn(8) xor dataIn(9) xor dataIn(10) xor dataIn(11) xor dataIn(12) xor dataIn(13) xor dataIn(14) xor dataIn(15) xor
                                             dataIn(16) xor dataIn(17) xor dataIn(18) xor dataIn(19) xor dataIn(20) xor dataIn(21) xor dataIn(22) xor dataIn(23) xor dataIn(24) xor
                                             dataIn(25) xor dataIn(26) xor dataIn(27) xor dataIn(28) xor dataIn(29);

                        if parity_pipe /= parity_calculated then
                                s_DV <= '0';
                                dataOutValid <= '0';
                        else
                                s_DV <= '1';
                                dataOutValid <= dataInValid;
                        end if;
-- synthesis translate_off   -- turn off scrambling for sim
                    if scrambleEnable = '0' then
                        parity_pipe <= '0';
                        dataOutValid <= dataInValid;
                    end if;
-- synthesis translate_on
                end if;
        end process;


        -- bad_par_cnt : process(clock, reset_counters, reset)
        -- begin
        --         if reset_counters = '1' then
        --                 parity_counter <= (others => '0');
        --         elsif reset = '1' then  -- pause counter if data input invalid - i.e. don't count unlocked header as bad parity
        --                 parity_counter <= parity_counter;
        --         elsif rising_edge(clock) then
        --                 if s_DV = '0' then
        --                         parity_counter <= parity_counter +1 ;
        --                 end if;
        --         end if;
        -- end process;

        -- parity_error_counter <= std_logic_vector(parity_counter);
end a;
