`timescale 1ns/1ps

`include "chip_serializer_vvc.sv"

// Serializer Testbench
//
//  Data processing chain:
//  Inputword -> add header -> scrambler -> Mux -> (SIM only) serializer -> (SIM only) Deserializer ->  (SIM only) Descrambler
//
//  - Serialization of 30bit datawords in fast, slow, slowest mode
//  - Deserialization in fast mode with aligned output, check if Input matches Descrambled data
//  - Velo Scrambler is only dependent on previous word, not previous scrambled word? -> Different to standard multiplicative scrambler
//
//  TODO: - Module: Add 4->1 Mux to match 128 bit frame from Velo?
//        - datavalid, dataoff

module serializer_tb;

parameter   RESLOW_CYCLES = 5,
            SYNCWORD = 30'h3F555555,
            USE_PLL = 1;

chip_serializer_if serializer_if();

logic [29:0] DataIn_A;
logic dataoff_A = 0;
logic datavalid_A = 1;

logic clkIn_640M;
logic clk_320M;

wire [1:0] Bitdataout;

logic Scr_res_n;
logic Ser_res_n;
wire clkOut_40M;
logic clkIn_40M;

logic [1:0] timer;
logic [1:0] speed_mode;

wire [1:0] DataOut_slow;
wire DataOut_slowest;

reg Scr_en;

wire [2:0] Cnt8;

//DUT
SerializerTop #(
    .USE_PLL(USE_PLL)
)SerializerTop_I(
    .DataIn_A(serializer_if.DataIn_A),
    .dataoff_A(serializer_if.dataoff_A),
    .datavalid_A(serializer_if.datavalid_A),
    .clkIn_640M(clkIn_640M),
    .clkIn_320M(clk_320M),
    .Scr_res_n(serializer_if.Scr_res_n),
    .Ser_res_n(serializer_if.Ser_res_n),
    .clkIn_40M(clkIn_40M),

    .clkOut_40M(clkOut_40M),
    .BitDataOut_A(serializer_if.Bitdataout),
    .DataOut_slow(serializer_if.DataOut_slow),
    .DataOut_slowest(serializer_if.DataOut_slowest),

    .timer(serializer_if.timer),
    .speed_mode(serializer_if.speed_mode),
    .Scr_en(serializer_if.Scr_en),

    .Cnt8(serializer_if.Cnt8)
);

//SerDes SIM Module
logic [29:0] DeScrOut_1G28;
logic [29:0] DeScrOut_640M;
logic [29:0] DeScrOut_320M;

SerDesTop_SIM SerDesTop_SIM(
    .DataIn_fast(serializer_if.Bitdataout),
    .DataIn_slow(serializer_if.DataOut_slow),
    .DataIn_slowest(serializer_if.DataOut_slowest),

    .clkIn_640M(clkIn_640M),
    .clk_320M(clk_320M),
    .Scr_res_n(serializer_if.Scr_res_n),
    .Ser_res_n(serializer_if.Ser_res_n),

    .DeScrOut_fast(serializer_if.DeScrOut_1G28),
    .DeScrOut_slow(serializer_if.DeScrOut_640M),
    .DeScrOut_slowest(serializer_if.DeScrOut_320M),

    .Cnt8(serializer_if.Cnt8)
);

//logic gen_datawords = 0;

initial
begin
    automatic chip_serializer_driver serializer_driver = new(serializer_if);
    automatic top_serializer_test top_test = new(serializer_if);
    
    //Init
    clkIn_640M = 1;
    clk_320M = 1;
    clkIn_40M = 1;
    serializer_if.Scr_en = 0;
    serializer_if.speed_mode = 3'd0; //Fast mode
    fork      
        //Inject error in Scrambleroutput[5] between 300 and 350 ns
        //serializer_driver.scr_biterror_driver(serializer_if, 300,50);

        //Top level testclass, calls all drivers and monitors
        top_test.main(clk_320M, Ser_res_n);

        begin
            serializer_driver.reset_ser_driver(clkIn_640M); //Reset
            //Enable Scr_en after giving some time to align descrambling
            #200 serializer_if.Scr_en = 1;
            #500 serializer_if.Scr_en = 0;
            serializer_driver.reset_ser_driver(clkIn_640M);

            //Slow Mode
            serializer_if.speed_mode = 3'd1;
            #200 serializer_if.Scr_en = 1;
            #500 serializer_if.Scr_en = 0;
            serializer_driver.reset_ser_driver(clkIn_640M);

            //Slowest Mode
            serializer_if.speed_mode = 3'd2;
            #200 serializer_if.Scr_en = 1;
        end
    join
end

always begin
    #3000 $finish("timeout");
end

//synced Clocks
always #0.5 clkIn_640M <= ~clkIn_640M;
always #1 clk_320M <= ~clk_320M;
always #8 clkIn_40M <= ~clkIn_40M;

endmodule
