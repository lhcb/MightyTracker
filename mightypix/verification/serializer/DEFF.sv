//SerDes FOR SIMULATION ONLY

//Dual edge FF for sim
module DEFF_ser(
    input logic [1:0] d,
    input logic clk,
    output logic q,
    input logic res_n
);

always @(clk) begin
    if (!res_n)
        q <= 0;
    else 
        case(clk)
            1'b1: q <= d[0];
            1'b0: q <= d[1];
        endcase
end

endmodule

//Dual edge deserializer with alignment to sync_word
module DeSer #(
    parameter SYNC_WORD = 30'h552B55,
    parameter STARTBIT = 2'b10,

    parameter DEFF = 1'b1,
    parameter WORDLEN = 32
)
(
    input logic d,
    input logic clk,
    output logic [WORDLEN-1:0] q,
    output logic aligned,
    input logic res_n
);

logic [WORDLEN-1:0] sync_word = {STARTBIT, SYNC_WORD};
logic [$clog2(WORDLEN)-1:0] i;
logic sync;
logic aligned_temp; 
logic [WORDLEN-1:0] q_temp;

task func_align_ser();
    if (!aligned) begin
        if (d == !sync_word[i]) begin
            if (d == sync_word[WORDLEN-1]) begin //If value doesn't match sync pattern, but sync MSB, use as new MSB for alignment
                i <= WORDLEN - 2;
                q_temp[WORDLEN-1] <= d;
                sync <= 1;
            end
            sync <= 0;
        end
        else if (d == sync_word[i]) begin
            sync <= 1;
            if (i == 0) begin
                aligned_temp <= 1;

                i <= '1;
                q <= q_temp;
            end
            else
                i <= i - 1;
            q_temp[i] <= d;
        end
    end
    else begin
        q_temp[i] <= d;

        if (i == 0) begin
            i <= '1;
            q <= q_temp;
        end
        else
            i <= i - 1;
    end
endtask


//dual or single edge
generate 
    if(DEFF) begin
        always_ff @(clk) begin
            if (!res_n) begin
                    q <= 0;
                    aligned <= 0;
                    aligned_temp <= 0;
                    i <= '1;
                    sync <= 0;
                    q_temp <= 0;
                end
                else begin
                    aligned <= aligned_temp; //Delay by one cycle

                    func_align_ser();
                end
        end
    end
    else begin
        always_ff @(posedge clk) begin
            if (!res_n) begin
                q <= 0;
                aligned <= 0;
                aligned_temp <= 0;
                i <= '1;
                sync <= 0;
                q_temp <= 0;
            end
            else begin
                aligned <= aligned_temp; //Delay by one cycle

                func_align_ser();
            end
        end
    end
endgenerate

endmodule
