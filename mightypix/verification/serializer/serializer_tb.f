## This Is a DUT Testbench, which is a wrapper around the Chip and performs some initialisations so that is can be quickly integrated
## into a higher level simulation for first tests.

## Main Design
$BASE/rtl/serializer/SerializerTop.sv
$BASE/rtl/serializer/SerializerMain.sv
$BASE/rtl/serializer/SerializerTree.sv
$BASE/rtl/serializer/Scrambler.v
$BASE/rtl/serializer/ClockGen_new.sv
$BASE/rtl/serializer/PhaseCounter.sv
$BASE/rtl/serializer/ScramblerVelo.sv

## Modules for verification
$BASE/verification/serializer/DEFF.sv
$BASE/verification/serializer/DeScramblerVelo.vhd
$BASE/verification/serializer/DeSerTop.sv

## Standard Cells  Models for Simulation
-makelib .:cells
$BASE/implementation/ams_h18/h18_CORELIB_HV_heiko.v
-endlib 

## Testbench
$BASE/verification/serializer/serializer_tb.sv

+incdir+$BASE/verification/serializer

## Standard Args for Cadence XCELIUM
-sv
-access r+w
-timescale 1ns/10ps
-define SIMULATION