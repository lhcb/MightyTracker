`timescale 1ns/1ps

module SerDesTop_SIM#( 
    //parameter SYNC_WORD = 30'h552B55)
    parameter SYNC_WORD = 30'hBFFFC000
)
(
    input logic [1:0] DataIn_fast,
    input logic [1:0] DataIn_slow,
    input logic DataIn_slowest,

    input logic clkIn_640M,
    input logic clk_320M,
    input logic Scr_res_n,
    input logic Ser_res_n,

    input logic [2:0] Cnt8,

    output logic [29:0] DeScrOut_fast, 
    output logic [29:0] DeScrOut_slow,
    output logic [29:0] DeScrOut_slowest
);


logic dataoff_A = 0;
logic datavalid_A = 1;

logic DeScr_en = 1;

wire SerOut_1G28;
wire SerOut_640M;
logic SerOut_320M;

wire [29:0] DeSerOut_fast, DeSerOut_slow, DeSerOut_slowest;
wire aligned, aligned2, aligned3;

//DEFFs for SIM
//Fast Mode 2:1 Serializer
DEFF_ser DEFF_I(
    .d(DataIn_fast),
    .clk(clkIn_640M),
    .q(SerOut_1G28),
    .res_n(Ser_res_n)
);

//Slow Mode 2:1 Serializer
DEFF_ser DEFF_I2(
    .d(DataIn_slow),
    .clk(clk_320M),
    .q(SerOut_640M),
    .res_n(Ser_res_n)
);

//Single Edge FF for slowest Mode
always @(posedge clk_320M) begin
    SerOut_320M <= DataIn_slowest;
end

// Fast Mode Deserializer, output words are aligned to sync word
DeSer #(.SYNC_WORD(SYNC_WORD))
DeSer_I1(
    .d(SerOut_1G28),
    .clk(clkIn_640M),
    .q(DeSerOut_fast),
    .res_n(Ser_res_n),
    .aligned(aligned)
);

DeSer #(.SYNC_WORD(SYNC_WORD))
DeSer_I2(
    .d(SerOut_640M),
    .clk(clk_320M),
    .q(DeSerOut_slow),
    .res_n(Ser_res_n),
    .aligned(aligned2)
);

DeSer #(
    .DEFF(1'b0),
    .SYNC_WORD(SYNC_WORD)
)
DeSer_I3 
(
    .d(SerOut_320M),
    .clk(clk_320M),
    .q(DeSerOut_slowest),
    .res_n(Ser_res_n),
    .aligned(aligned3)
);

// Velo Descrambler
VeloPixDeScrambler VeloPixDeScrambler_I(
    .dataIn(DeSerOut_fast),
    .dataInValid(datavalid_A),
    .scrambleEnable(DeScr_en),
    .parity(1'b1),
    .clock(clk_320M),
    .reset(Scr_res_n),
    .reset_counters(Scr_res_n),
    .dataOut(DeScrOut_fast),
    .dataOutValid(),
    .cnt8(Cnt8)
    //.parity_error_counter()
);

VeloPixDeScrambler VeloPixDeScrambler_I2(
    .dataIn(DeSerOut_slow),
    .dataInValid(datavalid_A),
    .scrambleEnable(DeScr_en),
    .parity(1'b1),
    .clock(clk_320M),
    .reset(Scr_res_n),
    .reset_counters(Scr_res_n),
    .dataOut(DeScrOut_slow),
    .dataOutValid(),
    .cnt8(Cnt8)
    //.parity_error_counter()
);

VeloPixDeScrambler VeloPixDeScrambler_I3(
    .dataIn(DeSerOut_slowest),
    .dataInValid(datavalid_A),
    .scrambleEnable(DeScr_en),
    .parity(1'b1),
    .clock(clk_320M),
    .reset(Scr_res_n),
    .reset_counters(Scr_res_n),
    .dataOut(DeScrOut_slowest),
    .dataOutValid(),
    .cnt8(Cnt8)
    //.parity_error_counter()
);

endmodule