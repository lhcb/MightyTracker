`ifndef chip_serializer_vvc
    `define chip_serializer_vvc

    //`define SERIALIZER_LOG_OK

    interface chip_serializer_if;

        parameter SYNCWORD = 30'h3f555555;

        //Data in
        logic [29:0] DataIn_A;

        logic DataIn_valid;

        //timer clkdivider from Readout FSM in
        logic[1:0] timer;

        //switch between fast, slow and slowest
        logic [1:0] speed_mode;

        //Scrambler enable in
        logic Scr_en;

        //Data out
        logic [1:0] Bitdataout;
        logic [1:0] DataOut_slow;
        logic DataOut_slowest;

        //Reset in
        logic Scr_res_n;
        logic Ser_res_n;

        //Phasecounter out
        logic [2:0] Cnt8;
        
        `ifdef SIMULATION
            //DescrOut
            logic [29:0] DeScrOut_1G28;
            logic [29:0] DeScrOut_640M;
            logic [29:0] DeScrOut_320M;
        `endif

    endinterface

    

    class chip_serializer_monitor_in;

        mailbox serializer_input_mbx;
        
        virtual chip_serializer_if serializer_if;

        function new(virtual chip_serializer_if serializer_if, mailbox serializer_input_mbx);
            this.serializer_input_mbx = serializer_input_mbx;
            this.serializer_if = serializer_if;
        endfunction

        task monitor_in();
            begin
                
                forever begin
                        @(serializer_if.DataIn_A);

                        serializer_input_mbx.put(serializer_if.DataIn_A);

                end
            end
        endtask
    endclass

    class chip_serializer_monitor_out;

        mailbox serializer_output_mbx;
        
        virtual chip_serializer_if serializer_if;

        function new(virtual chip_serializer_if serializer_if, mailbox serializer_output_mbx);
            this.serializer_output_mbx = serializer_output_mbx;
            this.serializer_if = serializer_if;
        endfunction

        task monitor_out();
            begin
                logic [29:0] DeScrOut;

                forever begin
                    @(serializer_if.DeScrOut_1G28,serializer_if.DeScrOut_640M,serializer_if.DeScrOut_320M);

                        case(serializer_if.speed_mode)
                            2'd0, 2'd2: DeScrOut = serializer_if.DeScrOut_1G28;
                            2'd1: DeScrOut = serializer_if.DeScrOut_640M;
                            2'd3: DeScrOut = serializer_if.DeScrOut_320M;
                        endcase

                        serializer_output_mbx.put(DeScrOut);
                end
            end
        endtask
    endclass

    class chip_serializer_scoreboard;

        mailbox serializer_input_mbx;
        mailbox serializer_output_mbx;
        
        virtual chip_serializer_if serializer_if;

        logic [29:0] input_word, output_word;

        function new(virtual chip_serializer_if serializer_if, mailbox serializer_input_mbx, mailbox serializer_output_mbx);
            this.serializer_input_mbx = serializer_input_mbx;
            this.serializer_output_mbx = serializer_output_mbx;
            this.serializer_if = serializer_if;
        endfunction

        task main();

            int count_fail = 0;
            int count_success = 0;

            int count_total = 0;
            int count_display = 0;

            int count_sync = 0;

        fork
            forever begin
                serializer_input_mbx.get(input_word);
                serializer_output_mbx.get(output_word);
                if(input_word == serializer_if.SYNCWORD) begin
                    $display("OK \tSerializer \tScoreboard: Input Sync");
                    count_success++;
                    count_sync = 4;
                end
                else if(input_word == output_word) begin
                    `ifdef SERIALIZER_LOG_OK
                        $display("OK \tSerializer \tScoreboard: %h, %h at %0t ps", input_word, output_word, $time);
                    `endif
                    count_success++;
                end
                else begin
                    $display("Fail \tSerializer \tScoreboard: %h, %h at %0t ps", input_word, output_word, $time);
                    count_fail++;
                end
            end

            forever begin
                wait(count_success + count_fail >= (count_display + 50));
                    count_display = count_success + count_fail;
                    $display("Serializer Summary: SUCCESSFULL: %0d \tFAILED: %0d", count_success, count_fail);
            end
        join
        endtask
    endclass

    class chip_serializer_driver #(RESLOW_CYCLES = 5, SYNCWORD = 30'h3F555555);
        //// Basic Readout FSM emulation

        virtual chip_serializer_if serializer_if;

        function new(virtual chip_serializer_if serializer_if);
            this.serializer_if = serializer_if;
        endfunction

        //FSM timer counter Clockdivider for cnt8
        task fsm_timer_driver(ref logic clk_320M);
            begin
                serializer_if.timer = 0;
                @(posedge serializer_if.Ser_res_n);

                forever begin
                    @(negedge serializer_if.Cnt8[2]);
                        if (!serializer_if.Ser_res_n)
                            serializer_if.timer = 0;
                        else if (serializer_if.Cnt8 == 0)
                            serializer_if.timer = serializer_if.timer + 1;
                end
                
            end
        endtask

        /* task scr_biterror_driver(input integer  start_time_ns, input integer release_after_ns);
            begin
                //#(1ns*start_time_ns) force serializer_if.ScrOut_A[5] = 1'b1;
                //#(1ns*release_after_ns) release serializer_if.ScrOut_A[5];
                #(1ns*start_time_ns) force $root.serializer_tb.SerializerTop_I.ScrOut_A[5] = 1'b1;
                #(1ns*release_after_ns) release $root.serializer_tb.SerializerTop_I.ScrOut_A[5];
            end
        endtask */

        // Generate Dataword in sync with Cnt8
        task fsm_wordgen_driver();
            begin
                integer dataword_counter = 0;
                forever begin
                    begin
                        @(posedge serializer_if.Cnt8[2]);
                        if(serializer_if.Scr_en) begin
                            if (serializer_if.speed_mode == 2'd0 | (serializer_if.speed_mode == 2'd1 & serializer_if.timer[0]) | (serializer_if.speed_mode == 2'd2 & serializer_if.timer == 3) ) begin
                                dataword_counter = dataword_counter + 1;
                                serializer_if.DataIn_A = dataword_counter;
                            end
                        end
                        else begin
                            serializer_if.DataIn_A = SYNCWORD;
                            //dataword_counter = 0;
                        end
                    end
                end
            end
        endtask

        task reset_ser_driver(ref logic clkIn_640M);
            begin
                serializer_if.Scr_res_n = 0;
                serializer_if.Ser_res_n = 0;
                repeat(RESLOW_CYCLES) @(posedge clkIn_640M);

                serializer_if.Scr_res_n = 1;
                serializer_if.Ser_res_n = 1;
            end
        endtask
    endclass: chip_serializer_driver

    class top_serializer_test;

        //mon + driver handles
        automatic chip_serializer_monitor_in      monitor_in;
        automatic chip_serializer_monitor_out     monitor_out;
        
        automatic chip_serializer_driver          driver;
        automatic chip_serializer_scoreboard      scb;


        mailbox serializer_input_mbx;
        mailbox serializer_output_mbx;

        virtual chip_serializer_if serializer_if;

        //event sync_event;

        function new(virtual chip_serializer_if serializer_if);
            this.serializer_if = serializer_if;
            
            //mailboxes
            serializer_input_mbx    = new();
            serializer_output_mbx   = new();
            
            //Monitors
            //monitor                 = new(serializer_if);
            monitor_in              = new(serializer_if, serializer_input_mbx);
            monitor_out             = new(serializer_if, serializer_output_mbx);

            //Scoreboard
            scb             = new(serializer_if, serializer_input_mbx, serializer_output_mbx);

            //Driver
            driver          = new(serializer_if);
        endfunction

        task main(ref logic clk_320M, ref logic res_n);
            $display("Start Serializer Scoreboard with Drivers + Monitors");
            fork
                driver.fsm_timer_driver(clk_320M);
                driver.fsm_wordgen_driver();

                
                //driver.scr_biterror_driver(300, 50); //Only works with specific hierarchy

                //Scoreboard
                scb.main();

                //Monitors
                monitor_in.monitor_in();
                monitor_out.monitor_out();
                
            join
        endtask

        task monitor_only();
            $display("Start Serializer Scoreboard in Monitor-Mode");
            fork
                //Scoreboard
                scb.main();

                //Monitors
                monitor_in.monitor_in();
                monitor_out.monitor_out();
                
            join
        endtask

    endclass

`endif