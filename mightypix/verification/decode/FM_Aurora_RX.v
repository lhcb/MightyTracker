`timescale 1ns/1ps

module FM_Aurora_RX(
			clk_ser,
			clk_800p,
			res_n,
			d_out,
			BUS_CLK,
			WCLK,
			RESET,
			FCLK,
			FCLK2X,
			write_dec_in,
			data_dec
			);

input			clk_ser;
input			clk_800p;
input			res_n;
input [1:0]      	d_out;
input			BUS_CLK;
input			WCLK;
input			RESET;
input			FCLK;
input			FCLK2X;
output			write_dec_in;
output [31:0]		data_dec;
	
//for 0.4 use negedge and ~clk

//Instantiation of Stoyan Todorov's Aurora Protocol Engine



////////////////
reg [1:0]  d_out_reg;
reg [1:0]  d_out_reg_del;


//AURORA MSB FIRST TRANSMITTED!!!

//can be simplified is pos edge pos edge
////////////////

always @ (negedge clk_800p) begin//fast neg
    d_out_reg <= d_out;
end

always @ (posedge clk_800p) begin//fast neg
    d_out_reg_del <= d_out_reg;
end

wire bit_d_out;

assign bit_d_out = ~clk_800p  ?  d_out_reg[1] : d_out_reg_del[0];

receiver_logic #(
   .DSIZE(10)
) I_receiver_logic (
    .RESET(RESET),
    .WCLK(WCLK),
    .FCLK(FCLK),
    .FCLK2X(FCLK2X),
    .BUS_CLK(BUS_CLK),
    .RX_DATA(bit_d_out),
    .read(),
    .data(),
    .empty(),
    .full(),
    .rec_sync_ready(),
    .lost_err_cnt(),
    .decoder_err_cnt(),
//    output reg [15:0]       fifo_size,
    .invert_rx_data(1'b0),
    .write_dec_in(write_dec_in),
    .data_dec(data_dec)
);

endmodule
