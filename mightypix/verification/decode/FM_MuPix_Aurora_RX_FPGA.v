
module FM_MuPix_Aurora_RX_FPGA(
			clk_ser,  // clk_8n
			clk_800p, //fast_clk_100
			clk20, // clk divided
			res_n,
			bit_d_out,
			Aur_res_n,
			Cnt4,
			clk_slow,
			d_decoded
			
			);

input			clk_ser;
input			clk_800p;
input 			clk20;
input			res_n;
input 			bit_d_out;
input			Aur_res_n;
input [1:0]		Cnt4;
output [8:0] d_decoded;

output			clk_slow;
//input wire [19:0] ParallelIn,
	
//for 0.4 use negedge and ~clk

reg dispin;
wire dispout;

wire slow_ck;
assign slow_ck = clk20;//genclk;//clk20;

//Instantiation of Stoyan Todorov's Aurora Protocol Engine

/*

////////////////
reg [1:0]  d_out_reg;
reg [1:0] d_out_reg_del;
reg [1:0] d_out_reg_del2;


//AURORA MSB FIRST TRANSMITTED!!!

//can be simplified is pos edge pos edge
////////////////

always @ (negedge clk_800p) begin//fast neg
    d_out_reg <= d_out;
end

always @ (posedge clk_800p) begin//fast neg
    d_out_reg_del <= d_out_reg;
end

always @ (negedge clk_800p) begin//fast neg
    d_out_reg_del2[0] <= d_out_reg_del[0];
end




//assign DataOut = clkIn_800p  ?  Stage2[1] : Stage2[0];
wire bit_d_out;
wire bit_d_out_del;

assign bit_d_out = ~clk_800p  ?  d_out_reg[1] : d_out_reg_del[0];//fast ~
assign bit_d_out_del = clk_800p  ?  d_out_reg_del[1] : d_out_reg_del2[0];//fast ~

/////////////////////
*/

assign clk_slow = Cnt4[1];//assign clk_slow = ~Cnt4[1];





wire disperror;	 

/*
always @ (posedge clk_ser or negedge Aur_res_n) begin

    if (~Aur_res_n) begin
	dispin        <= 1'b0;
    end  

else begin
      if(shiftcnt == 6'd8) dispin <= dispout;
end
end
*/
wire DOAin;
assign DOAin = bit_d_out;

//shift-register for encoded input data
reg [19:0] shift_DOA = 0;
reg [19:0] shift_DOApar = 0;
//storage for encoded data, input for decoder
reg [9:0] DOA = 0;

reg [3:0] select_shift_slow = 0;
reg [19:0] shift_DOA_slow ;
wire [9:0] shift_DOA_slow0, shift_DOA_slow1, shift_DOA_slow2, shift_DOA_slow3, shift_DOA_slow4, shift_DOA_slow5, shift_DOA_slow6, shift_DOA_slow7, shift_DOA_slow8, shift_DOA_slow9;

always @ (posedge clk_ser) // try negedge
begin

    shift_DOA[19:0] <= {DOAin, shift_DOA[19:1]}; // receiver

end


assign shift_DOA_slow0 = shift_DOA[9:0] ;//10'b0011111010 ; // shift_DOA_slow[9:0] ;
assign shift_DOA_slow1 = shift_DOA[10:1] ;
assign shift_DOA_slow2 = shift_DOA[11:2] ;
assign shift_DOA_slow3 = shift_DOA[12:3] ;
assign shift_DOA_slow4 = shift_DOA[13:4] ;
assign shift_DOA_slow5 = shift_DOA[14:5] ; 
assign shift_DOA_slow6 = shift_DOA[15:6] ;
assign shift_DOA_slow7 = shift_DOA[16:7] ;
assign shift_DOA_slow8 = shift_DOA[17:8] ;
assign shift_DOA_slow9 = shift_DOA[18:9] ;


parameter code_minus = 10'b0101111100;
parameter code_plus = 10'b1010000011;

//parameter code_minus = 10'b0101111100;
//parameter code_plus = 10'b1010000011;

reg match0, match1, match2, match3, match4, match5, match6, match7, match8, match9;

   always @(posedge slow_ck) begin
   
  
   
   match0 <= ((shift_DOA_slow0  == code_minus) || (shift_DOA_slow0 == code_plus));
   match1 <= ((shift_DOA_slow1  == code_minus) || (shift_DOA_slow1 == code_plus));
   match2 <= ((shift_DOA_slow2  == code_minus) || (shift_DOA_slow2 == code_plus));
   match3 <= ((shift_DOA_slow3  == code_minus) || (shift_DOA_slow3 == code_plus));
   match4 <= ((shift_DOA_slow4  == code_minus) || (shift_DOA_slow4 == code_plus));
   match5 <= ((shift_DOA_slow5  == code_minus) || (shift_DOA_slow5 == code_plus));
   match6 <= ((shift_DOA_slow6  == code_minus) || (shift_DOA_slow6 == code_plus));
   match7 <= ((shift_DOA_slow7  == code_minus) || (shift_DOA_slow7 == code_plus));
   match8 <= ((shift_DOA_slow8  == code_minus) || (shift_DOA_slow8 == code_plus));
   match9 <= ((shift_DOA_slow9  == code_minus) || (shift_DOA_slow9 == code_plus));
   
   
   // shift_DOA_slow[19:0] <= ParallelIn[19:0]; // modified
                        
       case (select_shift_slow)    
           4'd0: DOA[9:0] <= shift_DOA_slow0[9:0] ; 
           4'd1: DOA[9:0] <= shift_DOA_slow1[9:0] ;
           4'd2: DOA[9:0] <= shift_DOA_slow2[9:0] ;
           4'd3: DOA[9:0] <= shift_DOA_slow3[9:0] ;
           4'd4: DOA[9:0] <= shift_DOA_slow4[9:0] ;
           4'd5: DOA[9:0] <= shift_DOA_slow5[9:0] ;
           4'd6: DOA[9:0] <= shift_DOA_slow6[9:0] ;
           4'd7: DOA[9:0] <= shift_DOA_slow7[9:0] ;
           4'd8: DOA[9:0] <= shift_DOA_slow8[9:0] ;
           4'd9: DOA[9:0] <= shift_DOA_slow9[9:0] ;
           default: DOA[9:0] <= 0;
       endcase // case

dispin        <= dispout;

           if(!res_n) begin
    
            select_shift_slow <= 0; 
           end          
           else if (match0 == 1) begin 
            select_shift_slow <= 0;
      
           end 
           else if (match1 == 1) begin 
            select_shift_slow <= 1;
        
           end          
           else if (match2 == 1) begin 
            select_shift_slow <= 2;
        
           end 
           else if (match3 == 1)  begin 
            select_shift_slow <= 3;
       
           end 
           else if (match4 == 1)  begin 
             select_shift_slow <= 4;
        
           end 
           else if (match5 == 1)  begin 
             select_shift_slow <= 5;
           
           end
           else if (match6 == 1)  begin 
             select_shift_slow <= 6;
        
           end 
           else if (match7 == 1)  begin 
             select_shift_slow <= 7;
        
           end
           else if (match8 == 1)  begin 
             select_shift_slow <= 8;
        
           end 
           else if (match9 == 1)  begin 
             select_shift_slow <= 9;
          
           end 
           else begin
               //select_shift_slow <= select_shift_slow;
           end
           
end // end always

//Decoder
decode decode_A(
    .datain(DOA[9:0]),
    .dispin(dispin), 
    .dataout(d_decoded),  
    .dispout(dispout),
    .code_err(),
    .disp_err(disperror)
);  

endmodule
