`timescale 1ns/10ps

`define EOC_MODEL

`include "chip_matrix_vvc.sv"
`include "load_data.sv"

`include "veritas_base_tb.sv"
`include "shr_vvc.sv"


`include "i2c_global.sv"

`include "address_space_pkg.sv"

`include "eoc_model_pkg.sv"

`include "serializer/chip_serializer_vvc.sv"
`include "tfc_decoder/chip_tfcdec_vvc.sv"

import eoc_model_pkg::*;

import address_space_pkg::*;

module tb_top;


    /**
        This object is only used to define current running test suite and test in the testbench.
        It's internal String variable can be displayed in the wafevorm to easily identify which parts of the wafevorm belong to which test

    */
    veritas_base_tb veritas_tb = new();

    //localparam CLK_SPEED = ((1.0/1000.0)*1000000);
    logic   clk_ts;

    //-- 800p Clock from PLL in real design
    logic   clk_40;
    logic   clk_320;
    logic   clk_640;
    logic   res_n;

    wire clkOut_320;

    // I2C
    //----------------
    //wire scl;
    //wire sda;

    logic use_I2C_extern = 1'b0;
    logic use_I2C_tfc_extern = 1'b0;

    //localparam  I2C_CHIP_ID = 3'b1;

    i2c_wire_if i2c_master_wire_if();
    i2c_reg_if #(.ADDR_BW(11)) i2c_reg_if_A();
    shift_register_if shift_register();


    // wire pullup
    pullup(i2c_master_wire_if.scl);
    pullup(i2c_master_wire_if.sda);


    // Matrix
    //---------------

    //-- Matrix Connections
    chip_matrix_if matrix_if();

    //Serialiser
    //--------------------
    chip_serializer_if serializer_if();

    //TFC Decoder
    chip_tfcdec_if tfcdec_if(clk_320);


    assign tfcdec_if.Cnt8 = serializer_if.Cnt8;

    // Clocks + Reset
    //------------
    localparam CLK40_SPEED  = 40;
    localparam CLK320_SPEED = 320;
    localparam CLK640_SPEED = 640;
    localparam TSCLK_SPEED  = 40;

    initial begin
        clk_640 = 1;
        clk_320 = 1;
        clk_40 = 1;
        clk_ts = 1;
    end

    always #0.78125 clk_640 <= ~clk_640;
    always #1.5625 clk_320 <= ~clk_320;
    always #12.5 clk_40 <= ~clk_40;
    always #12.5 clk_ts <= ~clk_ts;


    FM_RstnGen resn_gen(.res_n(res_n),.ref_clk(clk_40));

    // Shift Register
    //--------------------

    //-- External Shift Register
    localparam CONFIG_BIT_EXTERNAL_COUNT = 16;
    SHR_INTERFACE shiftregister_driver_if();
    SHR_INTERFACE shiftregister_output_if();
    wire [CONFIG_BIT_EXTERNAL_COUNT-1:0] SRExtraBits;

    bit [7:0] tfc_command = 8'b1100_0010;

    bit [N_COLUMNS-1:0][N_HITBUFFERS-1:0] OutB = '1;


    initial
    begin
        // SDF Annotation
        // This must be place at the beginning of times and not in any "if" conditions
        // Only used after post place and route, just ignore for the first verilog only simulations
        //-------------------
        `ifdef SIMULATE_WITH_SDF
            `ifdef SDF_CORNER
                $sdf_annotate("../par/stream_out/MuPixDigitalTop.`SDF_CORNER.sdf", mproc.u1,, "annotate.log", "maximum");
            `else
                // $sdf_annotate("../par/stream_out/MPROC_v2.functional_800p.slow.sdf", mproc.u1,, "annotate.log", "maximum:typical:minimun");
                //$sdf_annotate("../par/stream_out/MPROC_v2.functional_800p.slow.sdf", mproc.u1,, "annotate.log","MAXIMUM");
                //
                //$sdf_annotate("../par/stream_out/MPROC_v2.functional_1n25.slow.sdf", mproc.u1,, "annotate.log","MAXIMUM");

                $sdf_annotate("../par/stream_out/MPROC_v2.functional_800p.slow.sdf", mproc.u1,, "annotate.log","MAXIMUM");
                //$sdf_annotate("../par/stream_out/MPROC_v2.functional_800p.noedge.slow.sdf", mproc.u1,, "annotate.log","MAXIMUM");
            `endif
        `endif
    end

    // Test
    //------------------------
    initial
    begin



        // Chip Model
        automatic chip_matrix_driver    matrix_driver       = new();
        automatic chip_matrix_monitor_eoc   matrix_monitor      = new();

        // Load data
        automatic load_data load_data = new();
        automatic write_data write_data = new();

        // Shift Register
        automatic shr_driver            shift_register_driver   = new();

        automatic shr_segment ckdivend          = shift_register_driver.add_segment(6,0);
        automatic shr_segment ckdivend2         = shift_register_driver.add_segment(6,0);
        automatic shr_segment timerend          = shift_register_driver.add_segment(4,4'd0);
        automatic shr_segment slowdownend       = shift_register_driver.add_segment(4,4'd0);
        automatic shr_segment maxcycend         = shift_register_driver.add_segment(6,6'd63);

        automatic shr_segment sr_empty         = shift_register_driver.add_segment(2,1);


        automatic shr_segment resetckdivend     = shift_register_driver.add_segment(4,0);
        automatic shr_segment sendcounter       = shift_register_driver.add_segment(1,0);
        automatic shr_segment itsphase          = shift_register_driver.add_segment(6,1);
        automatic shr_segment EnSync_SC         = shift_register_driver.add_segment(1,0);
        automatic shr_segment slowdownldcolend  = shift_register_driver.add_segment(5,5'd0); //5'd3
        automatic shr_segment countsheeps       = shift_register_driver.add_segment(1,0);
        automatic shr_segment syncpattern       = shift_register_driver.add_segment(30,30'h3FFF8000);
        automatic shr_segment bxresetval        = shift_register_driver.add_segment(4,4'd0);
        automatic shr_segment ts_tdc_min        = shift_register_driver.add_segment(7,7'd1);
        automatic shr_segment ts_tdc_max        = shift_register_driver.add_segment(7,7'd10);
        automatic shr_segment ckdivend3         = shift_register_driver.add_segment(6,0);
        automatic shr_segment IenclkgenB        = shift_register_driver.add_segment(1,0);
        automatic shr_segment Iphase            = shift_register_driver.add_segment(1,0);
        automatic shr_segment Ien_ScrB          = shift_register_driver.add_segment(1,0);
        automatic shr_segment dummy             = shift_register_driver.add_segment(16,16'hACDB);
        //automatic shr_segment dummy2            = shift_register_driver.add_segment(16,16'hACDB);


        // Scoreboard
        //automatic scoreboard sb = new();

        // Inits
        //-----------
        shift_register_driver.time_delay = 5ns; // Delay determines speed of Shift Register interface

        matrix_monitor.silent                  = 0; // Console will display monitored generated pixel hits

        // Start
        //---------------------------------------------------

        // Reset
        resn_gen.enableReset ();


        //------------------------------------------------------------

        // Start Monitors etc..
        ///------------------
        fork
            //Start matrix monitor and write output to file (Arguments: matrix_if, write2file (1 or 0), output_file)
            matrix_monitor.monitor(matrix_if, 1, "$BASE/verification/simulation_data/output_C4785x17600um2_P165x55um2_E1to500_L1to6_app_Q1_wosecs.csv");

            //load data
            load_data.load_file("$BASE/verification/simulation_data/data_C4785x17600um2_P165x55um2_E1to500_L1to6_app_Q1_wosecs.csv", "r");

            begin

                @(posedge res_n);
                #1000;

                forever begin
                    load_data.load_hit;

                    if (load_data.end_flag)
                        break;

                    load_data.display_hit(load_data.col, load_data.row, load_data.tot, load_data.t);

                    #(load_data.t); //1.562/1.5 =1.04
                    write_data.write_to_file("$BASE/verification/simulation_data/output_extra_C4785x17600um2_P165x55um2_E1to500_L1to6_app_Q1_wosecs.csv", 0, load_data.col, load_data.row, load_data.tot, 0, 0, $time());

                    fork
                        matrix_driver.generateHits(OutB, load_data.col, load_data.row, load_data.tot*1000);
                    join_none
                    #0;
                end
            end

        join_none

        // Reset Sequence
        //---------
        shift_register_driver.print_length();
        shift_register_driver.init(shiftregister_driver_if); // This inits IO

        resn_gen.enableReset (); // Release resets


        //-- Send Configuration to Chip
        #250ns veritas_tb.test_start("Sending SR Configuration to chip");

        shift_register_driver.shift_zeros(shiftregister_driver_if,64);
        shift_register_driver.shift_local_segments(shiftregister_driver_if);

        // Extra bits should be ACDB
        if (SRExtraBits!=16'hACDB)
        begin
            #500 $fatal("SR Bits don't match!");
        end

        veritas_tb.test_stop();


        //-- Remove main reset, keep transmit paths in reset to avoid simulation chaos
        resn_gen.disableReset();

    end


    always begin
        #200000 $stop();
    end

    // DUT
    //---------------------------
    mightypix_top dut (

        // Clocking + Reset
        //---------
        .clk_40M            ( clk_40    ),
        .clk_320M           ( clk_320   ),
        .clk_640M           ( clk_640   ),
        .clk_timestamp_in   ( clk_ts    ),

        .Ser_res_n          ( res_n     ),
        .RO_res_n           ( res_n     ),
        .TSGen_res_n        ( res_n     ),
        .TFCalign_res_n     ( res_n     ),
        .i2c_res_n          ( res_n     ),

        .clkOut_320M        (clkOut_320 ),

        // Matrix
        //-------------

        //from matrix
        .PriFromDet    ( matrix_if.PriFromDet    ),

        .RowAddFromDet ( matrix_if.RowAddFromDet ),
        .ColAddFromDet ( matrix_if.ColAddFromDet ),
        .TSFromDet     ( matrix_if.TSFromDet     ),
        .NegTSFromDet  ( matrix_if.NegTSFromDet  ),
        .TDCFromDet    ( matrix_if.TDCFromDet    ),
        .AmpFromDet    ( matrix_if.AmpFromDet    ),
        .FineTSFromDet ( matrix_if.FineTSFromDet    ),

        //to matrix
        .TSToDet    ( matrix_if.TSToDet    ),
        .NegTSToDet  ( matrix_if.NegTSToDet  ),
        .FineTSToDet ( matrix_if.FineTSToDet ),
        .TSToDetTDC  ( matrix_if.TSToDetTDC    ),

        // Readout Control
        .LdCol  ( matrix_if.LdCol   ),
        .RdCol  ( matrix_if.RdCol   ),
        .LdPix  ( matrix_if.LdPix   ),
        .PullDN ( matrix_if.PullDN ),

        // Serialiser Out
        //----------------
        .d_out(serializer_if.Bitdataout),
        .DataOut_slow(serializer_if.DataOut_slow),
        .DataOut_slowest(serializer_if.DataOut_slowest),

        // TFC In
        .tfc_in(tfcdec_if.tfc_in),

        //PLL FB
        .clkOut_40M(),

        // SR
        //--------------
        .SIn_Ext        (shiftregister_driver_if.SIN),
        .Ck1_Ext        (shiftregister_driver_if.CK1),
        .Ck2_Ext        (shiftregister_driver_if.CK2),
        .Ld_Ext         (shiftregister_driver_if.LOAD),
        .Readback_Ext   (1'b0   ),
        .SRExtraBits    (  SRExtraBits     ),
        .SRExtraBitsN   (       ),

        .WriteRAM0      (       ),
        .WriteRAM1      (       ),
        .WriteRAM2      (       ),
        .WriteRAM3      (       ),

        .SOut_Ext(),


        //.shift_register_input (shiftregister_driver_if),
        //.shift_register_output(shiftregister_output_if),

        // I2C
        //------------
        .i2c_sda        (i2c_master_wire_if.sda),
        .i2c_scl        (i2c_master_wire_if.scl),
        .i2c_chip_id    (3'd1),

        .use_I2C_extern(use_I2C_extern),
        .use_I2C_tfc_extern(use_I2C_tfc_extern)
    );

    wire [N_COLUMNS-1:0] FastOut, SlowOut;


    assign matrix_if.PriFromDet = FastOut[N_COLUMNS-1];

    Matrix_top Matrix_top_I (
        .OutB               (OutB),
        .PDIn               (matrix_if.PullDN),
        .LdColIn            (matrix_if.LdCol),
        .RdColIn            (matrix_if.RdCol),
        .LdPixIn            (matrix_if.LdPix),

        .CkIn               (clk_320), // only for TDC
        .TS1In              (matrix_if.TSToDet),
        .TS2In              (matrix_if.TSToDet),
        .TS3In              (matrix_if.FineTSToDet),
        //({1'b0,OutAddrB,OutTS2B,OutTS1B,OutTS3B})
        .DataOut            ({matrix_if.RowAddFromDet, matrix_if.AmpFromDet, matrix_if.TSFromDet, matrix_if.FineTSFromDet}),
        .ColAddOut          (matrix_if.ColAddFromDet),
        .FastOut            (FastOut),
        .SlowOut            (SlowOut),

        .SlowInB            (),
        .OrInB              (),

        .HB                 (hb)
    );

endmodule
