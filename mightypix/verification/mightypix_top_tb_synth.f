## This Is a DUT Testbench, which is a wrapper around the Chip and performs some initialisations so that is can be quickly integrated
## into a higher level simulation for first tests.

## Main Design from GTL
../synthesis/netlist/mightypix_top.gtl.v 

## Standard Cells  Models for Simulation
-makelib .:cells
$BASE/implementation/ams_h18/h18_CORELIB_HV_heiko.v
-endlib 

## Testbench
$BASE/verification/mightypix_top_tb.sv 
$BASE/building_blocks/clock_and_reset/FM_ClkGen_MHz.sv
$BASE/building_blocks/clock_and_reset/FM_RstnGen.sv
$BASE/verification/serializer/DeSerTop.sv
$BASE/verification/serializer/DEFF.sv
$BASE/verification/serializer/DeScramblerVelo.vhd

## Include paths for included headers with class definitions
+incdir+$BASE/verification
+incdir+$BASE/building_blocks/veritas
+incdir+$BASE/building_blocks/shiftregister
+incdir+$BASE/building_blocks/i2c
+incdir+$BASE/rtl/tfc_decoder

## Standard Args for Cadence XCELIUM
-sv
-access r+w 
-timescale 1ns/10ps
-define SIMULATION
-define ASIC
-define ASYNC_RES
## The probes script makes the simulation save all the values
## You can then add a signal to the wafevorm without having to resimulate to get the values
-input $BASE/verification/xcelium/probes.tcl
