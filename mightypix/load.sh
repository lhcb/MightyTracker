## Project Directory to easily reference file paths
export BASE="$(dirname "$(readlink -f ${BASH_SOURCE[0]})")"
export REPO=$BASE/../

## Add bin folder for project utilities
export PATH="$BASE/bin:$PATH"

export DEFAULT_KIT=/adl/design_kits/ams_18_7m_2021.sh 
export TSI_HOME=/adl/design_kits/TSI/IBM_PDK/cmhv7sf/V1.4.3.3AM

if [[ -z $ADL_TECH  ]]
then
    echo "(W) It looks like no technology is loaded. Remember to load the design kit"
    echo "Would you like to load the default design kit $DEFAULT_KIT (y for yes)?"
    read load_kit
    if  [[ $load_kit == "y" ]] 
    then 
         source $DEFAULT_KIT; 
    fi
    	
fi

alias streamin_ams="SOS_WORKAREA=$HOME/sos/TSI_SHARED_ams design_stream_sos_import"
alias streamin_tsi="design_stream_sos_import"

echo "(I) Loaded Mightypix ASIC source project"
