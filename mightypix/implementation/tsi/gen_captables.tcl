## Parameters
################
set scriptLocation [file normalize [file dirname [info script]]]
set outFolder capTables
exec mkdir -p capTables

set cell_lib sc_1p8v_9T_RVT
set metal 7AM

## Load Design Kit array
###############
source $scriptLocation/std_cells.tcl



## Search for ICT
###############
foreach ict [glob $::env(TSI_HOME)/digital/cmhv7sf/cadence/v.20121206/ict/*] {

    set ictName [file tail $ict]
    #puts "Testing $ictName"

    regexp {[\w]+_([\w]+)_([\w]+).*} $ictName -> metalName timing

    if {$metalName == $metal} {
        puts "Generating cap tables for: $metalName -> $timing"
        #set lefFiles [list $STD_CELLS($cell_lib/lef/$metal)  $STD_CELLS($cell_lib/lef/common)]
        set lefFiles [list $STD_CELLS($cell_lib/lef/$metal)]

        generateCapTbl -ict $ict -output $outFolder/${metalName}_${timing}.ict -lef $lefFiles

    }
    

    

}
