############################
## This File Contains an array definition of standard cells for TSI
## These utilities are meant to easily use the design kit which has rather long file names
## Also this file will help better change cell sets to compare behaviour between corners
##################################

set scriptLocation [file normalize [file dirname [info script]]]

## Name of Server folder (eda/adl):
set ADL_HOME  $::env(ADL_HOME)

## Some Folders from design kit to make the floy script more readable
#####################

set TSI_HOME  $::env(TSI_HOME)
set STD_CELL_HOME $TSI_HOME/digital/ibm_cmhv7sf/std_cell

## Base folder where the lib files for each cell library and corners are located
array set ::STD_CELLS {}

## Define Corner libs and lef files
#################################
set lefAvailableMetals  {3AM 4AM 5AM 6AM 7AM}
set cellLibs            {sc_1p8v_9T_RVT sc_1p8v_9T_HVT sc_5V_18T}
set corners             {fast_v132_tm40  fast_v132_tm55  fast_v198_tm40  fast_v198_tm55  slow_v108_t125  slow_v162_t125  typ_v120_t025  typ_v180_t025}

foreach cellLib $cellLibs {

    ## Get Version (Folder in cell lib folder, sorted and take last one)
	set libVersionFolder 	[lindex [lsort [glob -types d $STD_CELL_HOME/$cellLib/*]] end]
    set libVersion 			[file tail $libVersionFolder]

    ## Timing Corners -> look into folder
    foreach cornerFolder [glob -types d $STD_CELL_HOME/$cellLib/$libVersion/synopsys/*] {
        
        set cornerName [file tail $cornerFolder]
        foreach libFile  [glob -types f $STD_CELL_HOME/$cellLib/$libVersion/synopsys/$cornerName/*.lib] {
            
            set libFileName [file tail $libFile]
            #puts "Testing file existence: $scriptLocation/${libFileName} -> [file exists $scriptLocation/${libFileName}]"
            if {[file exists $scriptLocation/${libFileName}]} {
                set     STD_CELLS($cellLib/lib/$cornerName)     $scriptLocation/${libFileName}
            } else {
                lappend STD_CELLS($cellLib/lib/$cornerName)     $libFile
            }

        }

        set STD_CELLS($cellLib/cdb/$cornerName) [glob -types f $STD_CELL_HOME/$cellLib/$libVersion/synopsys/$cornerName/*.db] 
    


    }

    
    ## LEF 
    #set STD_CELLS($cellLib/lef/common) $STD_CELL_HOME/$cellLib/v.20120320/lef/IBM_CMHV7SF_SC_1P8V_9T_RVT.lef
    set STD_CELLS($cellLib/lef/common) [list $scriptLocation/IBM_CMHV7SF_SC_1P8V_9T_RVT.lef  $scriptLocation/CTS_ROUTES.lef]
    foreach lefMetal $lefAvailableMetals {
        set STD_CELLS($cellLib/lef/$lefMetal) $STD_CELL_HOME/$cellLib/$libVersion/lef/cmhv7sf_${lefMetal}_tech.lef 
    }
	
	# GDS
	set  STD_CELLS($cellLib/gds) [lindex [glob -types f $libVersionFolder/gds2/*] 0]
	
}

## Technology Files
##############################
set qrcFolder  $TSI_HOME/Assura/QRC

foreach metalFolder [glob -directory $qrcFolder -types d *] {
    set name [file tail $metalFolder]
    puts "Saving QRCfor Metal Config: $name"
    set STD_CELLS(qrc/$name/min) $metalFolder/min/qrcTechFile
    set STD_CELLS(qrc/$name/max) $metalFolder/max/qrcTechFile
    set STD_CELLS(qrc/$name/typ) $metalFolder/typ/qrcTechFile
}

## Physical Cells & Infos
###################
set STD_CELLS(physical/antenna) FGTIE_A_HV_9T
set STD_CELLS(physical/fillers) [list FILL1_HV_9T FILL2_HV_9T]
set STD_CELLS(physical/fillcaps) [list ]
set STD_CELLS(physical/tie) 	{TIE0_HV_9T TIE1_HV_9T}

set STD_CELLS(physical/gds) 	$::env(TSI_HOME)/digital/cmhv7sf/techlef/v.20121206/lef/cmhv7sf_soce2gds.map
#set STD_CELLS(physical/gdsmap) 	$::env(TSI_HOME)/digital/cmhv7sf/techlef/v.20121206/lef/cmhv7sf_soce2gds.map
set STD_CELLS(physical/gdsmap)  $scriptLocation/cmhv7sf_soce2gds.map

## Utils
#######################
namespace  eval tsi {
    proc tsi_print_stdcells args {
        puts "(ADL) Printing available STD Cells definitions"
        foreach k [lsort [array names ::STD_CELLS]] {
            puts "$k -> $::STD_CELLS($k)" 
        }
    }
}


tsi::tsi_print_stdcells