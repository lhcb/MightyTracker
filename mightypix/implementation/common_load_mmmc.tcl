## Base Parameters and variables
##########################

## Base from environment
set BASE $::env(BASE)
## Name of Server folder (eda/adl):
set ADL_HOME  $::env(ADL_HOME)
set TSI_HOME  $::env(TSI_HOME)
set AMS_HOME  $::env(ADL_HOME)

## Load Definitions
##############################

##set BASE /home/iperic/git/mupix/pATLASpix-3_DCU

## We only set some variables here, the actual init design call is done in the main script
#set captable_filename /adl/design_kits/AMS/ah18-hitkit414/cds/HK_AH18/LEF/ah18a7/ah18a7-worst.capTable
#set captable_filename /adl/design_kits/AMS/ah18-hitkit414/cds/HK_AH18/LEF/ah18a7/ah18a7-typical.capTable

## TODO -> Change to TSI capTables, but AMS Can be used
set captable_filename $AMS_HOME/cds/HK_H18/LEF/h18a7/h18a7.capTable

## Load Library files using Multi Mode Multi Corner
#########################

#### First: Create Library Sets
create_library_set -name slowHT \
    -timing $STD_CELLS($cell_lib/lib/${::cell_corner_slow})  \
    -si     $STD_CELLS($cell_lib/cdb/${::cell_corner_slow})

create_library_set -name fastLT \
    -timing  $STD_CELLS($cell_lib/lib/${::cell_corner_fast}) \
    -si      $STD_CELLS($cell_lib/cdb/${::cell_corner_fast})

#### Second: Constraints
create_constraint_mode -name functional 		-sdc_files $BASE/implementation/constraints/constraints_functionalModeSyn.sdc
#create_constraint_mode -name test 				-sdc_files $BASE/implementation/constraints/constraints_testModeSyn.sdc

#### Third: Create a delay corner ??
if {${::implStage} == "SYN" } {

	#-cap_table $captable_filename
    create_rc_corner -name rcfastLT \
	-qrc_tech  $STD_CELLS(qrc/${::selected_metal}/min) \
	-pre_route_res          0.5 \
	-pre_route_cap          0.93 \
	-pre_route_clock_res    0.5 \
	-pre_route_clock_cap    0.93 \
	-post_route_res          0.5 \
	-post_route_cap          0.93 \
	-post_route_clock_res    0.5 \
	-post_route_clock_cap    0.93 \
	-temperature -40

    create_rc_corner -name rcslowHT \
	-qrc_tech  $STD_CELLS(qrc/${::selected_metal}/max) \
	-pre_route_res          1.5 \
	-pre_route_cap          1.07 \
	-pre_route_clock_res        1.5 \
	-pre_route_clock_cap        1.07 \
	-post_route_res             1.5 \
	-post_route_cap             1.07 \
	-post_route_clock_res       1.5 \
	-post_route_clock_cap       1.07 \
	-temperature 125

} else {

    create_rc_corner -name rcfastLT \
	-qx_tech_file  $STD_CELLS(qrc/${::selected_metal}/min) \
	-preRoute_res 0.5 \
	-preRoute_cap 0.93 \
	-preRoute_clkres 0.5 \
	-preRoute_clkcap 0.93 \
	-postRoute_res 0.5 \
	-postRoute_cap 0.93 \
	-postRoute_clkres 0.5 \
	-postRoute_xcap 0.93 \
	-T -40

    create_rc_corner -name rcslowHT \
    -qx_tech_file  $STD_CELLS(qrc/${::selected_metal}/max) \
	-preRoute_res 1.5 \
	-preRoute_cap 1.07 \
	-preRoute_clkres 1.5 \
	-preRoute_clkcap 1.07 \
	-postRoute_res 1.5 \
	-postRoute_cap 1.07 \
	-postRoute_clkres 1.5 \
	-postRoute_xcap 1.07 \
	-T 125

}

if {${::implStage} == "SYN" } {

    create_timing_condition -name slowHT -library_sets slowHT
    create_timing_condition -name fastLT -library_sets fastLT

    create_delay_corner -name slowHTrcw -timing_condition slowHT -rc_corner rcslowHT
    create_delay_corner -name fastLTrcb -timing_condition fastLT -rc_corner rcfastLT

} else {

    create_delay_corner -name slowHTrcw -library_set slowHT -rc_corner rcslowHT
    create_delay_corner -name fastLTrcb -library_set fastLT -rc_corner rcfastLT

}

#### Final: Create view
create_analysis_view -name functional_slowHT -constraint_mode functional -delay_corner slowHTrcw
create_analysis_view -name functional_fastLT -constraint_mode functional -delay_corner fastLTrcb

#create_analysis_view -name test_slowHT -constraint_mode test -delay_corner slowHTrcw
#create_analysis_view -name test_fastLT -constraint_mode test -delay_corner fastLTrcb



#### Load View
if {${::implStage} == "SYN" } {

    set_analysis_view -setup {functional_slowHT} -hold {functional_fastLT}

}

