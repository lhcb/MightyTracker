## Base Parameters and variables
##########################

## Base from environment
set BASE   $::env(BASE)
set ADL_HOME   $::env(ADL_HOME)
set TSI_HOME   $::env(TSI_HOME)
set AMS_HOME   $::env(AMS_HOME)

## Efforts
set EFFORT high 
if {[catch {set ::env(EFFORT_LOW)}]==0} {
	set EFFORT low
}


#Set the name of the top module
set top_module mightypix_top

## Select cell and corner
set CELLS_AMS true
if {[catch {set ::env(CELLS_TSI)}]==0} {
	set CELLS_AMS false
}
if {$CELLS_AMS} {
	
	## Load STD Cells array definition helper to make scripting easier
	source $::env(BASE)/implementation/ams_h18/std_cells.tcl
	
	set cell_lib            CORELIB_HV
	set cell_corner_slow    h18_1.8V/WC
	set cell_corner_fast    h18_1.8V/BC
	set selected_metal      7AM
	
	## Override the QRC files from TSI KIT
	set STD_CELLS(qrc/$selected_metal/min) $TSI_HOME/Assura/QRC/7AM/min/qrcTechFile
	set STD_CELLS(qrc/$selected_metal/max) $TSI_HOME/Assura/QRC/7AM/max/qrcTechFile
	
} else {
	
	## Load STD Cells array definition helper to make scripting easier
	source $::env(BASE)/implementation/tsi/std_cells.tcl
	
	set cell_lib            sc_1p8v_9T_RVT
	set cell_corner_slow    slow_v162_t125
	set cell_corner_fast    fast_v198_tm55
	set selected_metal      7AM
	
}

## Design implementation Stage
##  - SYN -> Synthesis
##  - PR0 -> Place and route relaxed
##  - SOFF -> Place and route signoff
set implStage SYN
set forceNonPhysical false

if {[catch {set ::env(NO_PHYSICAL)}]==0} {
	puts "Forcing Not physical"
	set forceNonPhysical true
}

## Location of this script
#################
set scriptLocation [file normalize [file dirname [info script]]]
puts "Synthesis script is located in: $scriptLocation, base is $BASE"

## Utils
#################

source $scriptLocation/retime.tcl 

proc logStep {name script} {
	puts "######################## BEG: $name ########################"
	uplevel $script
	puts "######################## EOF: $name ########################"
}



#source $BASE/implementation/scripts/settings.tcl
#source $scriptLocation/settings_rc.tcl

## Summary
#########################
logStep "Summary" {

	puts "(I) Sources Folder: $BASE/rtl"

}

################################################################################
#
#   The work flow starts here
#
################################################################################

set_db information_level 3

## Warning that cells are not aligned to origin in LEF
suppress_messages PHYS-127

## Globally disable clock gating here first
set_db lp_insert_clock_gating false


## Load MMMC
#####################
logStep "MMMC" {
	read_mmmc $scriptLocation/../common_load_mmmc.tcl
}

## Load Physical
###############
#To prevent the library cell from being set to 'avoid', set attribute 'lib_lef_consistency_check_enable' to 'false'.
set_db lib_lef_consistency_check_enable true

read_physical -lefs [list  $STD_CELLS($cell_lib/lef/$selected_metal) $STD_CELLS($cell_lib/lef/common) ]

set_db qrc_tech_file $STD_CELLS(qrc/$selected_metal/max)

set_db design_process_node 180

set_db scale_of_cap_per_unit_length 1.5
set_db scale_of_res_per_unit_length 1.07

# Elaborate the design
#############

# Read the HDL files
# We are only using AMS Cell, but the if/else is there in case TSI would be to be tried at a later point
# Read first VHDL as one file, then verilog as .f - This is normal, Genus is stupid and can't read vhdl files from .f netlist files :(
if {$CELLS_AMS} {
    read_hdl -sv -f $BASE/rtl/mightypix_top_verilog.f
    
} else {
    read_hdl -sv -f $BASE/rtl/mightypix_top_verilog.f
    
}
	

set_db hdl_error_on_blackbox true
set_db hdl_track_filename_row_col 1

elaborate $top_module

## Init Design
###############
init_design

## Check Design
##########
check_design -all > check_design.rpt

if {[catch {set ::env(CHECK_DESIGN)}]==0} {
	puts "Please Check Design warnings before continuing"

	report_messages -all
	
	## Timing Lint Output, very important to check for wrong things leading to massive logic optimisation
	report timing -lint > timing_lint.rpt
	report timing -lint -verbose > timing_lint.verbose.rpt
	
	suspend

}

## Retiming and Clock Gating 
## Not needed right now
##############

## Update clock gating
## Exclude everywhere but from Timestamp counters (not used in this design for now, keep as an example on how to configure)
set_db [get_db hinsts *] .lp_clock_gating_exclude true
#set_db [get_db hinsts TS_Counters_I*] .lp_clock_gating_exclude false

#suspend
## Enable Retiming for a couple clocks to help improve slack results
####

## Generate Retiming for resets
set_db retime_async_reset 	 true
set_db retime_optimize_reset true

## No clock retiming for now 
#adl_retime_clocks clk_timestamp clk_4n clk_8n clk_800p

## Don't touchs if needed
#set_db [get_db hinsts *ConfigBit*_I*] .dont_touch true
#suspend

#set_db [get_db hinsts *aurora_trans_I*] .ungroup_ok false
#suspend
# set_db [get_db hinsts *ConfigBit_I*] .dont_touch true
# set_db [get_db hinsts Verilog_PLL_I] .dont_touch true

## First Report, see if all is fine
############
report ple > ple.rpt


## Define cost groups (clock-clock, clock-output, input-clock, input-output)
## This is Taken from Genus Reference example template
###################################################################################

source $BASE/implementation/genus/create_default_cost_groups.tcl

## Physical Synthesis
##  - First Synthesis pass won't be placing, once a floorplan has been defined and saved then we can run physical synthesis
####################

if {$CELLS_AMS} {
    
    # Don't use TieHighLow, will be set by innovus
    dc::set_dont_use {TIE0_HV TIE1_HV} false
    
} else {
    
    # Don't use TieHighLow, will be set by innovus
    dc::set_dont_use {TIE1_HV_9T TIE0_HV_9T} false
    
}


## Synthesise to gates

if {!$forceNonPhysical && [file exists ../par/floorplan/floorplan.def]} {

	puts "Using DEF"

	## Effort set by environment at the beginning
	set effort $EFFORT 

	# Physical Synthesis Mode -> no wire delay
	set_db interconnect_mode ple

	## Init values for innovus
	set_db design_process_node 180

	## Effort
	set_db phys_flow_effort $effort

	#suspend
	read_def ../par/floorplan/floorplan.def

	## To Mapped
	logStep "SYN GENERIC" {
		set_db / .syn_generic_effort $effort
		syn_generic -physical

		
	}

	logStep "SYN MAP" {
		set_db / .syn_map_effort $effort
		syn_map	-physical
	}

	logStep "SYN OPT" {
		syn_opt -physical
	}

	#synthesize -to_placed -effort high
	#synthesize -to_placed -effort high -incr

	##syn_gen
	##syn_map
	##syn_opt -incremental
	##syn_opt -incremental

	# return 0
} else {

	puts "Not using DEF"
	#suspend

	## To Generic
	set_db / .syn_generic_effort $EFFORT 
	syn_generic

	if {[catch {set ::env(CHECK_DESIGN)}]==0} {
		puts "Please Check Design warnings before continuing"
		report_messages -all
		suspend
	}

	## Map
	set_db / .syn_map_effort $EFFORT 
	syn_map

	syn_opt
	syn_opt -incremental

}

## Write Timing out
exec mkdir -p timing
foreach cg [vfind / -cost_group *] {
	foreach mode [vfind / -mode *] {
		report_timing -group [list $cg] -mode $mode >> timing/${top_module}_[vbasename $cg]_post_map.rpt
	}
}

#print reports to the console and the log file
report gates
#report timing -mode test
#report timing -mode functional
report qor
report qor > qor.rpt
report_clock_gating > cgate.rpt

## Timing Lint Output, very important to check for wrong things leading to massive logic optimisation
report timing -lint > timing_lint.rpt
report timing -lint -verbose > timing_lint.verbose.rpt

## Write Backend Files
###############################
exec mkdir -p netlist
write_snapshot -outdir netlist -tag final
report_summary -directory netlist
write_hdl  > netlist/${top_module}.gtl.v
#foreach mode [vfind / -mode *] {
#	write_sdc -mode $mode > netlist/${top_module}_${mode}.sdc
#}

