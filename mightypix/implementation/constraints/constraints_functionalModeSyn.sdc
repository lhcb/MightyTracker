set sdc_version 1.4


## Use implStage to make constraints compatible for Synthesis and Place&Route Tools
set hS "/"
if {$implStage!="SYN"} {
	## Hierarchy separator is different in P&R Tool -> _
	set hS "_"
}



########################################################################################
#
#       Clocks
#
########################################################################################

#Define functional clock

## Parameters
## Periods in ns
##############
set clock_list [list clk_40M clk_320M clk_640M]
#set clock_list [list clk_40M clk_320M clk_640M clk_ts]

set clk_40M_period  [expr 25.0]
set clk_320M_period [expr $clk_40M_period  / 8]
set clk_640M_period [expr $clk_320M_period / 2]
set clk_ts_period   1.5

## Clock uncertainties, 10% for setup and 20% for hold during synthesis
set clk_uncertainty_setup 0.1
set clk_uncertainty_hold  0.2

if {$implStage!="SYN"} {
    set clk_uncertainty_setup 0.02
}

set clk_40M_uncertainty_setup   [expr $clk_40M_period * $clk_uncertainty_setup]
set clk_40M_uncertainty_hold    .050

set clk_320M_uncertainty_setup  [expr $clk_320M_period * $clk_uncertainty_setup]
set clk_320M_uncertainty_hold   .050

set clk_640M_uncertainty_setup  [expr $clk_640M_period * $clk_uncertainty_setup]
set clk_640M_uncertainty_hold   .050

set clk_ts_uncertainty_setup    [expr $clk_ts_period * $clk_uncertainty_setup]
set clk_ts_uncertainty_hold     .050



## SDC 
#############

## Top level clocks
create_clock -name clk_40M          -add -period $clk_40M_period    [get_ports clk_40M]
create_clock -name clk_320M         -add -period $clk_320M_period   [get_ports clk_320M]
create_clock -name clk_640M         -add -period $clk_640M_period   [get_ports clk_640M]
#create_clock -name clk_ts           -add -period $clk_ts_period     [get_ports clk_timestamp_in]

## Clock generator

create_generated_clock -source clk_640M  -name clk_40M_internal -divide_by 16 [get_pins serialiser${hS}ClockGen_I${hS}FastCnt16_reg\[7\]/Q]
#create_generated_clock -source clk_640M -name clk_3n2 -divide_by 4 [get_pins {serialiser/ClockGen_I/FastCnt4_reg[1]/Q}]

#create_generated_clock -source clk_640M -name clk_320M_internal -divide_by 2 [get_pins {serialiser/ClockGen_I/FastCnt2_reg/Q}]
create_generated_clock -source clk_640M -name clk_320M_internal -divide_by 2 [get_pins serialiser${hS}ClockGen_I${hS}clkOut_320M_reg/Q]

## Uncertainties
set_clock_uncertainty -setup $clk_40M_uncertainty_setup     clk_40M
set_clock_uncertainty -hold  $clk_40M_uncertainty_hold      clk_40M

set_clock_uncertainty -setup $clk_320M_uncertainty_setup    clk_320M
set_clock_uncertainty -hold  $clk_320M_uncertainty_hold     clk_320M

set_clock_uncertainty -setup $clk_640M_uncertainty_setup    clk_640M
set_clock_uncertainty -hold  $clk_640M_uncertainty_hold     clk_640M

#set_clock_uncertainty -setup $clk_ts_uncertainty_setup      clk_ts
#set_clock_uncertainty -hold  $clk_ts_uncertainty_hold       clk_ts

## Generated Clocks
## TODO: Improve to calculate values automatically
set_clock_uncertainty -setup $clk_320M_uncertainty_setup    clk_320M_internal
set_clock_uncertainty -hold  $clk_320M_uncertainty_hold     clk_320M_internal

#set_clock_uncertainty -setup 0.050 clk_3n2
#set_clock_uncertainty -hold  0.050 clk_3n2

########################################################################################
#
#       Input Delays
#
########################################################################################


## Readout <-> Matrix
##################

#Sets the input delay of 2ns for the signals that are sampled into the clock_slow domain Ivan 5
set_input_delay -clock clk_40M 2.0 [get_ports {*FromDet*}]

## Set a Slow input transition on all inputs to make them look weak and ensure enough buffering is added
set_input_transition 0.2 [all_inputs]

## Set Loading Cell for clock input, this would be the external buffer from MUX
#set_driving_cell -lib_cell CLKINVX32_HV [get_ports clk_timestamp_in]


########################################################################################
#
#       Capacitive Loads
#
########################################################################################


#Sets the output capacity to 100fF on all outputs
set_load 0.5 [all_outputs]
#Overwrites the output capacity with 10fF for all outputs that go to pads Jochen 10 Ivan 20
#set_load 0.2 [get_ports {d_out*}]

## Matrix <-> Readout, ensure enough load is set on outputs to have strong buffereing at the outputs
set_load 1.0 [get_ports {TSToDet*}]
set_load 1.0 [get_ports {FineTSToDet*}]
set_load 1.0 [get_ports {LdCol*}]
set_load 1.0 [get_ports {RdCol*}]
set_load 1.0 [get_ports {LdPix*}]
set_load 1.0 [get_ports {PullDN*}]

set_max_transition 0.5 [get_ports {TSToDet*}]
set_max_transition 0.5 [get_ports {FineTSToDet*}]
set_max_transition 0.5 [get_ports {LdCol*}]
set_max_transition 0.5 [get_ports {RdCol*}]
set_max_transition 0.5 [get_ports {LdPix*}]
set_max_transition 0.5 [get_ports {PullDN*}]

########################################################################################
#
#       Output Delays
#
########################################################################################

## Bit Data out: Force an high output delay and ensure the output is available before 1/4 of the clock period
## This is to ensure any DDR circuit after the outputs can't switch at the same moment a bit would switch
## For this Differential outputs the hold check doesn't really apply
set_max_transition 0.5 [get_ports {d_out*}]
set_output_delay  -clock clk_640M  -max [expr $clk_640M_period - ($clk_640M_period*.5)] [get_ports {d_out*}]
set_output_delay  -clock clk_640M  -min .150                                            [get_ports {d_out*}]

set_max_transition 0.5 [get_ports {DataOut_slow*}]
set_output_delay  -clock clk_320M  -max [expr $clk_320M_period - ($clk_320M_period*.5)] [get_ports {DataOut_slow*}]
set_output_delay  -clock clk_320M  -min .3                                              [get_ports {DataOut_slow*}]

## anything in an "ignore" block won't be called
proc ignore args { 

}

ignore {


    #set_max_transition 0.5 [get_ports {clkOut_4n}]
    #set_output_delay -clock clk_800p 0.0 [get_ports {clkOut_4n}]


    #set_max_transition 0.5 [get_ports {clk_8n}]
    #set_output_delay -clock clk_800p 0.0 [get_ports {clk_8n}]

    set_max_transition 0.5 [get_ports {d_out*}]
    set_output_delay -clock clk_800p 0.0 [get_ports {d_out*}]


    set_output_delay -clock ref_clock  0.0 [get_ports {SIn_*}]
    set_output_delay -clock ref_clock 0.0 [get_ports {Load_*}]
    set_output_delay -clock ref_clock 0.0 [get_ports {Ck1_*}]
    set_output_delay -clock ref_clock 0.0 [get_ports {Ck2_*}]
    set_output_delay -clock ref_clock 0.0 [get_ports {Readback}]
    set_output_delay -clock ref_clock 0.0 [get_ports {PCH}]
    set_output_delay -clock ref_clock 0.0 [get_ports {WrEnable}]
    set_output_delay -clock ref_clock 0.0 [get_ports {WR}]
    set_output_delay -clock ref_clock 0.0 [get_ports {Injection}]
    set_output_delay -clock ref_clock 0.0 [get_ports {Readback}]
    set_output_delay -clock ref_clock 0.0 [get_ports {ResetBiasBlocksB}]

    #Set the output delay for the ADC control signals to 2 ns Jochen clk_fsm Ivan clk_slow
    #note the time given is the time window to next clk change here could be 19ns for hard constrain



    #set_output_delay -clock clkIn_4n 2.0 [get_ports {TSToDet\[*}]

    #set_output_delay -clock clkIn_4n 2.0 [get_ports {TSDelToDet\[*}]


    set_output_delay -clock clk_8n 0.0 [get_ports {LdCol*}]
    set_output_delay -clock clk_8n 0.0 [get_ports {RdCol*}]
    set_output_delay -clock clk_8n 0.0 [get_ports {LdPix*}]
    set_output_delay -clock clk_8n 0.0 [get_ports {PullDN*}]



    set_max_transition 2.0 [get_ports {SIn_*}]
    set_max_transition 2.0 [get_ports {Load_*}]
    set_max_transition 2.0 [get_ports {Ck1_*}]
    set_max_transition 2.0 [get_ports {Ck2_*}]
    set_max_transition 2.0 [get_ports {Readback}]
    set_max_transition 2.0 [get_ports {PCH}]
    set_max_transition 2.0 [get_ports {WrEnable}]
    set_max_transition 2.0 [get_ports {WR}]
    set_max_transition 2.0 [get_ports {Injection}]
    set_max_transition 2.0 [get_ports {Readback}]
    set_max_transition 2.0 [get_ports {ResetBiasBlocksB}]

    set_max_transition 2.0 [get_ports {ADC_VDAC*}]

    #clk_ts_in 2/3
    set_max_transition 0.5 [get_ports {clk_ts_out}]
}

########################################################################################
#
#       Multi Cycle Paths and False paths
#
########################################################################################

## Reset False Paths !RESET SYNCHRONIZER?
set_false_path -from [get_ports Ser_res_n]
set_false_path -from [get_ports RO_res_n ]
set_false_path -from [get_ports i2c_res_n]

#Async Fifo
#Async Fifo pointer 2FF synchronized
set_false_path -from [get_pins async_fifo_*${hS}wptr_bin_reg*/CP] -to [get_pins async_fifo_*${hS}wptr_to_rdclk_reg\[0\]*/D]
set_false_path -from [get_pins async_fifo_*${hS}rptr_bin_reg*/CP] -to [get_pins async_fifo_*${hS}rptr_to_wrclk_reg\[0\]*/D]
set_false_path -from [get_pins async_fifo_*${hS}wptr_gry_reg*/CP] -to [get_pins async_fifo_*${hS}wptr_to_rdclk_reg\[0\]*/D]
set_false_path -from [get_pins async_fifo_*${hS}rptr_gry_reg*/CP] -to [get_pins async_fifo_*${hS}rptr_to_wrclk_reg\[0\]*/D]

set_false_path -from [get_pins async_fifo_*${hS}memory_reg*/CP]

#TFC decoder 2FF Synchronizer
set_false_path -from  [get_pins tfc_decoder_top_I${hS}tfc_dec_I${hS}tfc_bits_reg\[fereset\]/CP] -to [get_pins tfc_decoder_top_I${hS}tfc_fereset_I${hS}i2c_res_n_del_reg\[0\]/D]
set_false_path -from  [get_pins tfc_decoder_top_I${hS}tfc_dec_I${hS}tfc_bits_reg\[bxreset\]/CP] -to [get_pins MightyPixTSGen_I${hS}bxres_del_reg\[0\]/D]

# Revisit Falsh paths etc at a later point, once the clock schemes are understood
ignore {

    #clk_ts_in 3/3
    set_false_path -from [get_clocks ref_clock] -to [get_clocks clk_ts_in]


    set_false_path -from [get_clocks clkIn_4n] -to [get_clocks clk_800p]
    set_false_path -from [get_clocks clkIn_4n] -to [get_clocks clk_3n2]
    set_false_path -from [get_clocks ref_clock] -to [get_clocks clkIn_4n]

    set_clock_groups -asynchronous  -name asynch_clock -group {clkIn_4n} -group {clk_800p} 
}

########################################################################################
#
#       Other Stuff
#
########################################################################################

#set_propagated_clock [all_clocks]
if {$implStage=="SYN"} {
    #set_attribute preserve true [find GenConfigBit*${hS}ILatch*]
}


  
