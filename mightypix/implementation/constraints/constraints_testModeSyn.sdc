set sdc_version 1.4



########################################################################################
#
#       Clocks
#
########################################################################################

#Define functional clock


#1.6GHz
create_clock -name clk_800p -add -period 1.25 -waveform {0.0 0.625} [get_ports clk_800p]
create_clock -name clkIn_4n -add -period 6.25 -waveform {0.0 3.125} [get_ports clkIn_4n]
create_clock -name ref_clock -add -period 6.25 -waveform {0.0 3.125} [get_ports ref_clock]

#SPI 0/3
create_clock -name SCK -period 20.0 [get_ports SCK]

#clk_ts_in 0/3
create_clock -name clk_ts_in -add -period 1.25 -waveform {0.0 0.625} [get_ports clk_ts_in]

create_generated_clock -source clkIn_4n -name clk_8n -divide_by 2 [get_pins {SerializerTop_*/SerializerMain_I/Cnt4_reg[0]/Q}]

create_generated_clock -source clk_800p -name clk_3n2 -divide_by 4 [get_pins {SerializerTop_*/ClockGen_I/FastCnt4_reg[1]/Q}]

create_generated_clock -source clk_800p -name clk_1n6 -divide_by 2 [get_pins {SerializerTop_*/ClockGen_I/clkOut_1n6_reg/Q}]

#clk_ts_in 1/3
set_clock_uncertainty -setup 0.010 clk_ts_in
set_clock_uncertainty -hold  0.050 clk_ts_in

#SPI 1/3
set_clock_uncertainty -setup 0.010 SCK
set_clock_uncertainty -hold  0.050 SCK



set_clock_uncertainty -setup 0.010 ref_clock
set_clock_uncertainty -hold  0.050 ref_clock

set_clock_uncertainty -setup 0.010 clk_800p
set_clock_uncertainty -hold  0.050 clk_800p

set_clock_uncertainty -setup 0.010 clk_1n6
set_clock_uncertainty -hold  0.050 clk_1n6

set_clock_uncertainty -setup 0.080 clkIn_4n
set_clock_uncertainty -hold  0.080 clkIn_4n

set_clock_uncertainty -setup 0.050 clk_3n2
set_clock_uncertainty -hold  0.050 clk_3n2

set_clock_uncertainty -setup 0.080 clk_8n
set_clock_uncertainty -hold  0.080 clk_8n





########################################################################################
#
#       Input Delays
#
########################################################################################

#Sets the input delay of 2ns for the signals that are sampled into the clock_slow domain Ivan 5


set_input_delay -clock clk_8n 2.0 [get_ports {RowAddFromDet*}]
set_input_delay -clock clk_8n 2.0 [get_ports {ColAddFromDet*}]
set_input_delay -clock clk_8n 2.0 [get_ports {TSFromDet*}]
set_input_delay -clock clk_8n 2.0 [get_ports {PriFromDet*}]
#set_input_delay -clock clk_ts_in 0.0 [get_ports {sync_reset_Ext*}]


#set_input_delay -clock ref_clock 2.0 [get_ports {Serial_In*}]
#set_input_delay -clock ref_clock 2.0 [get_ports {Chip_Address*}]
#set_input_delay -clock ref_clock 2.0 [get_ports {Bias_Back_In*}]


#set_input_delay -clock clk_800p 0.1 [get_ports {data_valid}]






set_input_transition 0.2 [all_inputs]



########################################################################################
#
#       Capacitive Loads
#
########################################################################################


#Sets the output capacity to 100fF on all outputs
set_load 0.5 [all_outputs]
#Overwrites the output capacity with 10fF for all outputs that go to pads Jochen 10 Ivan 20
#set_load 0.2 [get_ports {d_out*}]


set_load 1.0 [get_ports {TSToDet*}]
set_load 1.0 [get_ports {TSToDet2*}]
set_load 1.0 [get_ports {LdCol*}]
set_load 1.0 [get_ports {RdCol*}]
set_load 1.0 [get_ports {LdPix*}]
set_load 1.0 [get_ports {PullDN*}]

########################################################################################
#
#       Output Delays
#
########################################################################################

#SPI 2/3
set_max_transition 2.0 [get_ports {MISO}]

set_max_transition 0.5 [get_ports {clkOut_4n}]
set_output_delay -clock clk_800p 0.0 [get_ports {clkOut_4n}]

#set_max_transition 0.5 [get_ports {clk_3n2}]
#set_output_delay -clock clk_800p 0.0 [get_ports {clk_3n2}]

set_max_transition 0.5 [get_ports {clk_8n}]
set_output_delay -clock clk_800p 0.0 [get_ports {clk_8n}]

set_max_transition 0.5 [get_ports {d_out*}]
set_output_delay -clock clk_800p 0.0 [get_ports {d_out*}]


set_output_delay -clock ref_clock  0.0 [get_ports {SIn_*}]
set_output_delay -clock ref_clock 0.0 [get_ports {Load_*}]
set_output_delay -clock ref_clock 0.0 [get_ports {Ck1_*}]
set_output_delay -clock ref_clock 0.0 [get_ports {Ck2_*}]
set_output_delay -clock ref_clock 0.0 [get_ports {Readback}]
set_output_delay -clock ref_clock 0.0 [get_ports {PCH}]
set_output_delay -clock ref_clock 0.0 [get_ports {WrEnable}]
set_output_delay -clock ref_clock 0.0 [get_ports {WR}]
set_output_delay -clock ref_clock 0.0 [get_ports {Injection}]
set_output_delay -clock ref_clock 0.0 [get_ports {Readback}]
set_output_delay -clock ref_clock 0.0 [get_ports {ResetBiasBlocksB}]

#Set the output delay for the ADC control signals to 2 ns Jochen clk_fsm Ivan clk_slow
#note the time given is the time window to next clk change here could be 19ns for hard constrain



#set_output_delay -clock clkIn_4n 2.0 [get_ports {TSToDet\[*}]

#set_output_delay -clock clkIn_4n 2.0 [get_ports {TSDelToDet\[*}]


set_output_delay -clock clk_8n 0.0 [get_ports {LdCol*}]
set_output_delay -clock clk_8n 0.0 [get_ports {RdCol*}]
set_output_delay -clock clk_8n 0.0 [get_ports {LdPix*}]
set_output_delay -clock clk_8n 0.0 [get_ports {PullDN*}]

set_max_transition 0.5 [get_ports {TSToDet*}]
set_max_transition 0.5 [get_ports {TSToDet2*}]
set_max_transition 0.5 [get_ports {LdCol*}]
set_max_transition 0.5 [get_ports {RdCol*}]
set_max_transition 0.5 [get_ports {LdPix*}]
set_max_transition 0.5 [get_ports {PullDN*}]

set_max_transition 2.0 [get_ports {SIn_*}]
set_max_transition 2.0 [get_ports {Load_*}]
set_max_transition 2.0 [get_ports {Ck1_*}]
set_max_transition 2.0 [get_ports {Ck2_*}]
set_max_transition 2.0 [get_ports {Readback}]
set_max_transition 2.0 [get_ports {PCH}]
set_max_transition 2.0 [get_ports {WrEnable}]
set_max_transition 2.0 [get_ports {WR}]
set_max_transition 2.0 [get_ports {Injection}]
set_max_transition 2.0 [get_ports {Readback}]
set_max_transition 2.0 [get_ports {ResetBiasBlocksB}]

set_max_transition 2.0 [get_ports {ADC_VDAC*}]

#clk_ts_in 2/3
set_max_transition 0.5 [get_ports {clk_ts_out}]

########################################################################################
#
#       Multi Cycle Paths
#
########################################################################################

#relaxing of conditions

#clk_ts_in 3/3
set_false_path -from [get_clocks ref_clock] -to [get_clocks clk_ts_in]

#SPI 3/3
set_false_path -from [get_clocks clkIn_4n] -to [get_clocks SCK]
set_false_path -from [get_clocks SCK] -to [get_clocks clkIn_4n]

set_false_path -from [get_clocks clkIn_4n] -to [get_clocks clk_800p]
set_false_path -from [get_clocks clkIn_4n] -to [get_clocks clk_3n2]

set_false_path -from [get_clocks ref_clock] -to [get_clocks clkIn_4n]

set_clock_groups -asynchronous  -name asynch_clock -group {clkIn_4n} -group {clk_800p} 

#this is good
set_false_path -from SerializerTop_*/SerializerMain_I/TenToEight_*_reg* -to SerializerTop_*/SerializerMain_I/Tree1R_*_reg*
set_false_path -from SerializerTop_*/SerializerMain_I/TenToEight_*_reg* -to SerializerTop_*/SerializerMain_I/Tree1L_*_reg*


########################################################################################
#
#       Other Stuff
#
########################################################################################

set_propagated_clock [all_clocks]  

set_attribute preserve true [find GenConfigBit*/ILatch*]

