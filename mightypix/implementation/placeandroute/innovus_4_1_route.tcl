proc innovus_4_1_route args {

	global ANTENNA_CELL

	# we get no multi-cuts without this command, but a SEGV with it...
	generateVias

	## Route from 2 to 6
	setExtractRCMode -engine preroute
	setNanoRouteMode -routeBottomRoutingLayer 1 \
		-routeTopRoutingLayer 6 \
		-routeWithTimingDriven true \
		-routeWithSiDriven true  \
		-routeWithViaInPin true \
		-routeWithViaOnlyForStandardCellPin 1:1 \
		-drouteUseMultiCutViaEffort high

	## Antenna Fix
	setNanoRouteMode -drouteFixAntenna true
	setNanoRouteMode -routeAntennaCellName $ANTENNA_CELL

	# /opt/eda/EDI120/doc/timingclosure/Detailed_Routing.html
	#changeUseClockNetStatus -noFixedNetWires

	#The new preferred routing command is routeDesign
	routeDesign

	## CTS Fixing if necessary
	ccopt_pro

	# Doing post routing optimization
	setExtractRCMode -engine postRoute
	setAnalysisMode -analysisType onChipVariation -cppr both

	setOptMode -reset
	setOptMode -fixDrc true -usefulSkewCCOpt standard
	setOptMode -powerEffort low
	
	optDesign -postRoute
	optDesign -postRoute -hold

	## Time Design
	###############
	if {[dbGet top.statusRCExtracted] == 0} {extractRC}
	exec mkdir -p reports/timing/
	timeDesign 	-postCTS -reportOnly -expandedViews -outDir reports/timing/  -numPaths 100 -prefix route
	timeDesign 	-postCTS -hold -reportOnly -expandedViews -outDir reports/timing/  -numPaths 100 -prefix route_hold

	
}

proc innovus_4_2_viaOpt args {
	setNanoRouteMode -drouteMinSlackForWireOptimization 0.1
	setNanoRouteMode -droutePostRouteSwapVia multiCut
	routeDesign -viaOpt
}

proc innovus_4_3_si args {

	setExtractRCMode -reset
	setExtractRCMode -engine postRoute -coupled true
	
	setDelayCalMode -engine default -siAware true
	
	setOptMode -reset
	setOptMode -fixDrc true -usefulSkewCCOpt standard
	setOptMode -powerEffort low
	# Leakage to dynamic ratio, from 0 to 1.0 -> don't use here because there is no dynamic power setup
	#setOptMode -leakageToDynamicRatio .1

	setOptMode -fixHoldAllowSetupTnsDegrade true
	setSIMode -deltaDelayThreshold -1 -analysisType default
	
	#setAnalysisMode -cppr none
	
	#  optDesign -postRoute -si -hold -outDir reports -prefix postroute_si_hold
	#The 'optDesign -postRoute -si [-hold]' command is obsolete. To avoid this message and ensure compatibility with future releases use the latest SI Optimization technology by ensuring 'setDelayCalMode -engine default -siAware true' is set & calling 'optDesign -postRoute [-hold]'
	optDesign -postRoute -hold -outDir reports -prefix postroute_si_hold
	optDesign -postRoute -outDir reports -prefix postroute_si
	
	if {[dbGet top.statusRCExtracted] == 0} {extractRC}
	exec mkdir -p reports/timing/
	timeDesign 	-postRoute -reportOnly -expandedViews -outDir reports/timing/  -numPaths 100 -prefix route
	timeDesign 	-postRoute -hold -reportOnly -expandedViews -outDir reports/timing/  -numPaths 100 -prefix route_hold
	
	#Save the design state after the power planning
	saveDesign savings/save05_postRoute.enc
}

proc innovus_4_repairHold args {

	setOptMode -reset
	setOptMode -fixFanoutLoad true
	setOptMode -fixDrc true 
	setOptMode -powerEffort low
	setOptMode -fixHoldAllowSetupTnsDegrade true
	setOptMode -fixHoldAllowResizing true

	setOptMode -postRouteAreaReclaim  holdAndSetupAware
	setOptMode -usefulSkewCCOpt extreme
	setOptMode -usefulSkewPostRoute true

	setDesignMode -flowEffort extreme

	optDesign -postRoute -outDir reports -prefix postroute_si_hold
	optDesign -postRoute -hold -outDir reports -prefix postroute_si_hold
	timeDesign 	-postCTS -hold -reportOnly -expandedViews -outDir reports/timing/  -numPaths 100 -prefix route_hold
	
}

proc innovus_4_4_repairViolations args {
	
	setOptMode -reset
	setOptMode -fixDrc true
	
	setNanoRouteMode -reset
	setNanoRouteMode -droutePostRouteLithoRepair true
	setNanoRouteMode -drouteSignOffEffort high
	
	detailRoute
	
	step_utils_verify
	
}
