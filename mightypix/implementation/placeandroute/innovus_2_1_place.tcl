
proc innovus_2_1_0_preplace args {
	
	if {!${::CELLS_AMS}} {
		setEndCapMode -leftEdge FILL1_HV_9T -rightEdge FILL1_HV_9T -rightBottomCorner FILL1_HV_9T -rightTopCorner FILL1_HV_9T -leftBottomCorner FILL1_HV_9T -leftTopCorner FILL1_HV_9T
		addEndCap -prefix PwrCap
	}
	
}
proc innovus_2_1_place args {

	
	
	## 02/2020 Richard: Updated to newest "place_design" and "place_opt_design" commands
	## This improves results and speed
	## Also the Trial Route mode is not supported anymode, switching to setRouteMode with "early" parameters switches
	
	setRouteMode -reset
	#setRouteMode -earlyGlobalMinRouteLayer 2
	#setRouteMode -earlyGlobalMaxRouteLayer 4
	setDesignMode -topRoutingLayer 4
	setDesignMode -bottomRoutingLayer 2
	
	# Place the Design with the following command
	# Lots of placement options can be set by means of the setPlaceMode command
	# before executing the placeDesign command
	# the inPlaceOpt switch turns on the optDesign with -preCTS
	#read_activity_file -format TCF -tcf_scope dcd_b_digital_block_full_sim_digital_top/dut_I ./power_intent/dcd_b_digital_block_togglecount.tcf
	#setPlaceMode -powerDriven true -maxRouteLayer 4 -wireLenOptEffort high


	## Select place mode
	if {[lsearch -exact $args -prototype]!=-1} {

		puts "####################### Running Place in Prototype mode ####################"
		setPlaceMode -reset
		setPlaceMode -timingDriven true -wireLenOptEffort medium -congEffort medium
		setPlaceMode -place_design_floorplan_mode true
		
		place_design
	
		
	} else {

		setDesignMode -earlyClockFlow true
		setPlaceMode -reset
		setPlaceMode -timingDriven true -wireLenOptEffort high -congEffort high

		setOptMode -reset
		setOptMode -allEndPoints true
		setOptMode -fixFanoutLoad true
		setOptMode -usefulSkew true
		
		
		## Place
		place_design
		
		## Optimize
		place_opt_design -out_dir reports -prefix prects
		place_opt_design -incremental_timing -out_dir reports -prefix prects


		## Time Design
		######################
		if {[dbGet top.statusRouted]      == 0} {trialRoute}
		if {[dbGet top.statusRCExtracted] == 0} {extractRC}
		exec mkdir -p reports/timing/
		timeDesign -reportOnly -expandedViews -outDir reports/timing/  -numPaths 100 -prefix placed

		

	}
	
	## Report
	checkPlace > reports/checkPlace.rpt

}

proc innovus_2_2_addTieHighLow args {

	global STD_CELLS

	setTieHiLoMode -cell $STD_CELLS(physical/tie)
	addTieHiLo

}

proc innovus_2_3_save { } {

	#Save the design state after the cell placement
	saveDesign savings/save02_placed.enc
}
