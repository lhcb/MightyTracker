## Design Parameters and configs
#########################

#Base from environment
set BASE $::env(BASE)
set REPO $::env(REPO)
set TSI_HOME  $::env(TSI_HOME)


## Location of the current script
set location [file dirname [file normalize [info script]]]
set parametersScript [file normalize [info script]]
proc reload_parameters args {
	uplevel {source $parametersScript}
}

## Load Design kit definitions


## Variables
set CELLS_AMS           true
set top_module          mightypix_top

if {$CELLS_AMS} {
    
    ## Load STD Cells array definition helper to make scripting easier
    source $::env(BASE)/implementation/ams_h18/std_cells.tcl
    
    set cell_lib            CORELIB_HV
    set cell_corner_slow    h18_1.8V/WC
    set cell_corner_fast    h18_1.8V/BC
    set selected_metal      7AM
    
    ## Override the QRC files from TSI KIT
    set STD_CELLS(qrc/$selected_metal/min) /adl/design_kits/TSI/IBM_PDK/cmhv7sf/V1.4.3.3AM/Assura/QRC/7AM/min/qrcTechFile
    set STD_CELLS(qrc/$selected_metal/max) /adl/design_kits/TSI/IBM_PDK/cmhv7sf/V1.4.3.3AM/Assura/QRC/7AM/max/qrcTechFile
    
    ## Cells to use and not to use
    #############
    set clockbuffer_cells   {}
    set clockinverter_cells {CLKINVX16_HV CLKINVX2_HV CLKINVX4_HV}
    set clockgating_cells   {}
    set buffer_cells_forbid {BUFX32_HV  BUFX16_HV}

    set TIE_CELLS {TIE0_HV TIE1_HV}
    set ANTENNA_CELL ANTENNA_HV
    set filler_cells {FILL1_HV_9T FILL2_HV_9T}
    
} else {
    
    ## Load STD Cells array definition helper to make scripting easier
    source $::env(REPO)/common/scripts/tsi/std_cells.tcl
    
    set cell_lib            sc_1p8v_9T_RVT
    set cell_corner_slow    slow_v162_t125
    set cell_corner_fast    fast_v198_tm55
    set selected_metal      7AM
    
    ## Cells to use and not to use
    #############
    set ckBuffer_allowed_strengths {I K M}
    set clockbuffer_cells   {CLK_I_HV_9T CLK_K_HV_9T CLK_M_HV_9T CLK_O_HV_9T }
    set clockinverter_cells {CLKI_I_HV_9T CLKI_K_HV_9T CLKI_M_HV_9T CLKI_O_HV_9T}
    set clockgating_cells   {DLG_E_HV_9T DLG_H_HV_9T DLG_K_HV_9T}
    
    set TIE_CELLS   {TIE1_HV_9T TIE0_HV_9T}
    set ANTENNA_CELL FGTIE_A_HV_9T
    set filler_cells {FILL1_HV_9T FILL2_HV_9T}
    
}



try {
	set implStage $::env(FLOW_STAGE)
} trap {} {} {
	set implStage "PR0"
	set implStage "PRQ"
}
#set implStage 		[expr catch {return $::env(FLOW_STAGE)} == 1 ? $::env(FLOW_STAGE) : "PR0" ]

puts "(I) Flow Stage: $implStage"

#########################
## Load all steps
#############################
foreach script [glob -types f $location/innovus_\[0-9\]*.tcl] {
	source $script
}

## Make utility steps
################

proc flow_sub_load args {
    innovus_0_0_load_design
    innovus_0_1_select_cells
}
proc flow_sub_plan args {
	global core_width
    global core_height

	innovus_1_1_floorplan
	innovus_1_2_placepins
	innovus_1_3_power_plan

    flow_utils_info_pins_placement
    puts "*ADL*: FP Width  = $core_width"
    puts "*ADL*: FP Height = $core_height"
	
}


proc flow_sub_place args {
	
	innovus_2_1_place $args
	innovus_2_2_addTieHighLow 
	innovus_2_3_save
    flow_utils_info_placement
}

proc flow_sub_cts args {
    innovus_3_1_setup
	innovus_3_2_CTS
	innovus_3_3_postCTS
}
proc flow_sub_route args {

	innovus_4_1_route
	innovus_4_2_viaOpt
	innovus_4_3_si

}


proc flow_sub_streamout args {

	innovus_5_1_addFillers

	flow_utils_fix_drc


	innovus_5_2_signoff
	innovus_5_3_streamout

	flow_utils_verify_for_drc

}


proc flow_prototype_place args {
	
	flow_sub_plan
	flow_sub_place -prototype
}



proc flow_sub_cts_to_signoff args {

	innovus_3_1_setup
	innovus_3_2_CTS
	innovus_3_3_postCTS
	
	innovus_4_1_route
	innovus_4_2_viaOpt
	innovus_4_3_si

	innovus_5_1_addFillers
	innovus_5_2_signoff
	innovus_5_3_streamout
}

proc flow_postplace args {

	innovus_3_1_1_setup
	innovus_3_1_2_CTS
	innovus_3_1_3_postCTS
	
	innovus_4_1_route
	innovus_4_2_viaOpt
	innovus_4_3_si
	
}

proc flow_place args {
	flow_sub_plan
	flow_sub_place
}

proc flow_cts args {
	flow_sub_plan
	flow_sub_place

	innovus_3_1_setup
	innovus_3_2_CTS
	innovus_3_3_postCTS
	
}

proc flow_fullroute args {
	
	flow_sub_plan
	flow_sub_place

	innovus_3_1_setup
	innovus_3_2_CTS
	innovus_3_3_postCTS
	
	innovus_4_1_route
	innovus_4_2_viaOpt
	innovus_4_3_si
}

proc flow_signoff args {

	innovus_1_1_floorplan
	innovus_1_2_placepins
	innovus_1_3_power_plan
	
	innovus_2_1_0_preplace
	innovus_2_1_place
	innovus_2_2_addTieHighLow
	innovus_2_3_save
	
	innovus_3_1_setup
	innovus_3_2_CTS
	innovus_3_3_postCTS
	
	innovus_4_1_route
	innovus_4_2_viaOpt
	innovus_4_3_si

	innovus_5_1_addFillers
	innovus_5_2_signoff
	innovus_5_3_streamout
}

##  Utility Steps for verifications and fixes
############



proc flow_utils_info_placement args {
    checkFPlan -reportUtil
}

## Use this utility to check which pins are not placed, or maybe wrongly placed
proc flow_utils_info_pins_placement args {
    set ignores {
        pin_guide
        pin_layer
        pin_on_fence
        pin_on_track
    }
    checkPinAssignment  -ignore $ignores

    ## Get Unplaced Pins
    set unplacedPins [dbGet -p top.terms.pStatus unplaced]
    if {[llength $unplacedPins]==1 && [lindex $unplacedPins 0]==0} {
        puts "*ADL*: All pins are placed :-)"
    } else {
        puts "*ADL*: Some pins are not placed, here's the list:"
        foreach pinName [dbGet ${unplacedPins}.name] {
            puts "- [lindex $pinName 0]"
        }
    }

}
## use -nofiller in args to not check fillers before filling has been done
proc flow_utils_verify_no_fillers args {
    flow_utils_verify_for_drc -no_fillers
}
proc flow_utils_verify_for_drc args {
	
	
	verifyConnectivity -noAntenna
	#verify_drc -ignore_trial_route -report verifyDRC.rpt 
	verify_drc
	verifyProcessAntenna
	verifyTieCell
    ## if nofiller not found, run filler check
    if {[lsearch -exact $args -no_fillers]==-1} {
        checkFiller
    }
	
}


proc flow_utils_fix_drc args {
	
    flow_utils_verify_for_drc
	setNanoRouteMode -reset
	ecoRoute -fix_drc
    flow_utils_verify_for_drc
	
}
		
