#################################################################################
#
#   Set some parameters in order to customize the script
#
################################################################################

set ring_size           30
set std_cell_height     5.04
set row_number          37

set total_width         1000
set core_height         [expr $row_number * $std_cell_height]
set core_width          [expr $total_width - $ring_size*2]

# Set the size of the die. The last four parameters specify the spacing between
# the die edge and the core edge -> currently, statically 0
proc innovus_1_1_floorplan args {
    global core_width
    global core_height
    global ring_size
    #floorPlan -d $die_size_x $die_size_y $ring_width_left $ring_width_bottom $ring_width_right $ring_width_top 
    floorPlan  -s [list $core_width $core_height $ring_size $ring_size $ring_size $ring_size] -coreMarginsBy die
}
