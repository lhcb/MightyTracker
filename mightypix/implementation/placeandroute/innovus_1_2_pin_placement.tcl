# List of pins
proc innovus_1_2_placepins args {

	#global core_height
	#global core_width

	global core_width
	global core_height
	
	set pinWidth 0.32

	setPinAssignMode -pinEditInBatch true

	
	#########################################################################################
	#
	#   Place Top Pins on metal 2
	#
	#########################################################################################

	#Place column2adc0 pins
	#set x_pos 100

	#########################################################################################
	#
	#   Place Left side Pins on metal 2
	#
	#########################################################################################
	set y_pos 100

	editPin -side Left -pin clk_40M -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign 0 [expr $y_pos] -snap USERGRID
	incr y_pos -10
    editPin -side Left -pin tfc_in -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign 0 [expr $y_pos]  -snap USERGRID
	

	#################
	## Right
	#################
	
	set y_pos 230

	

	## Clocks
	editPin -side Right -pin clk_640M -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign $core_width [expr $y_pos] -snap USERGRID
	incr y_pos -1
	editPin -side Right -pin clk_320M -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign $core_width [expr $y_pos] -snap USERGRID

    #incr y_pos -1
	#editPin -side Right -pin clk_40M -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign $core_width [expr $y_pos] -snap USERGRID

	
	## -4 to skip old pin positions
	incr y_pos -4 


	editPin -side Right -pin d_out[0] -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign $core_width [expr $y_pos] -snap USERGRID
	incr y_pos -1
	editPin -side Right -pin d_out[1] -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign $core_width [expr $y_pos] -snap USERGRID
    incr y_pos -1

    editPin -side Right -pin DataOut_slow[0] -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign $core_width [expr $y_pos] -snap USERGRID
	incr y_pos -1
	editPin -side Right -pin DataOut_slow[1] -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign $core_width [expr $y_pos] -snap USERGRID
    incr y_pos -1


    editPin -side Right -pin DataOut_slowest -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign $core_width [expr $y_pos] -snap USERGRID
	incr y_pos -1

	incr y_pos -20

	#editPin -side Right -pin clk_timestamp_in -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign $core_width [expr $y_pos + 2] -snap USERGRID
	#incr y_pos -1

	## Extra spacing to be similar to previous versions of Digital Part
	incr y_pos -1
	incr y_pos -1
	incr y_pos -1
	incr y_pos -1
	editPin -side Right -pin clkOut_40M -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign $core_width [expr $y_pos] -snap USERGRID
	incr y_pos -1

    editPin -side Right -pin clk40_from_TFC -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign $core_width [expr $y_pos] -snap USERGRID
	incr y_pos -1

    editPin -side Right -pin clkOut_320M -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign $core_width [expr $y_pos] -snap USERGRID
	incr y_pos -1

	#editPin -side Right -pin clkOut_8n -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign $core_width [expr $y_pos] -snap USERGRID
	#incr y_pos -1
	
	incr y_pos -20

	editPin -side Right -pin i2c_res_n -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign $core_width [expr $y_pos] -snap USERGRID
	incr y_pos -1

	editPin -side Right -pin Ser_res_n -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign $core_width [expr $y_pos] -snap USERGRID
	incr y_pos -1

	editPin -side Right -pin RO_res_n -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign $core_width [expr $y_pos] -snap USERGRID
	incr y_pos -1

	editPin -side Right -pin TSGen_res_n -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign $core_width [expr $y_pos] -snap USERGRID
	incr y_pos -1

    editPin -side Right -pin TFCalign_res_n -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign $core_width [expr $y_pos] -snap USERGRID
	incr y_pos -1
    

	## Extra Config Bits
	incr y_pos -10
	for {set x 0} {$x<16} {incr x} {
		editPin -side Right -pin SRExtraBits[$x] -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign $core_width [expr $y_pos]  -snap USERGRID
		incr y_pos -1
        editPin -side Right -pin SRExtraBitsN[$x] -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign $core_width [expr $y_pos]  -snap USERGRID
		incr y_pos -1
	}

	incr y_pos -50



	##########################
	## TOP Pins
	##########################
	
	## Init Position
	set x_pos 40

	## Pin Groups spacing to help easily adapt floorplan to changing width
	set top_group_spacing 100
	
	##### to Eoc

	for {set x 0} {$x<=11} {incr x} {
		editPin -side Top -pin TSToDet[$x] -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign [expr $x_pos] $core_height -snap USERGRID
		incr x_pos 1
	}

	for {set x 0} {$x<=11} {incr x} {
		editPin -side Top -pin NegTSToDet[$x] -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign [expr $x_pos] $core_height  -snap USERGRID
		incr x_pos 1
	}

	for {set x 0} {$x<=2} {incr x} {
		editPin -side Top -pin FineTSToDet[$x] -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign  [expr $x_pos] $core_height -snap USERGRID
		incr x_pos 1
	}

	for {set x 0} {$x<=6} {incr x} {
		editPin -side Top -pin TSToDetTDC[$x] -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign [expr $x_pos] $core_height -snap USERGRID
		incr x_pos 1
	}

	set x_pos [expr $x_pos + 10]

	editPin -side Top -pin RdCol -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign [expr $x_pos] $core_height -snap USERGRID
	incr x_pos 1

	editPin -side Top -pin LdCol -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign [expr $x_pos] $core_height -snap USERGRID
	incr x_pos 1

	editPin -side Top -pin LdPix -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign [expr $x_pos] $core_height -snap USERGRID
	incr x_pos 1

	editPin -side Top -pin PullDN -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign [expr $x_pos] $core_height -snap USERGRID
	incr x_pos 1

	

	#### From Eoc
	incr x_pos $top_group_spacing
	#set x_pos [expr $x_pos + $top_group_spacing]

	for {set x 0} {$x<=11} {incr x} {
		editPin -side Top -pin TSFromDet[$x] -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign [expr $x_pos] $core_height  -snap USERGRID
		incr x_pos 1
	}

	for {set x 0} {$x<=11} {incr x} {
		editPin -side Top -pin NegTSFromDet[$x] -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign [expr $x_pos] $core_height  -snap USERGRID
		incr x_pos 1
	}

	
	incr x_pos 70
	#set x_pos [expr $x_pos + 70]

	for {set x 0} {$x<=2} {incr x} {
		editPin -side Top -pin FineTSFromDet[$x] -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign [expr $x_pos] $core_height -snap USERGRID
		incr x_pos 1
	}

	for {set x 0} {$x<=6} {incr x} {
		editPin -side Top -pin TDCFromDet[$x] -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign [expr $x_pos] $core_height -snap USERGRID
		incr x_pos 1
	}

    for {set x 0} {$x<=6} {incr x} {
		editPin -side Top -pin AmpFromDet[$x] -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign [expr $x_pos] $core_height -snap USERGRID
		incr x_pos 1
	}

    

	incr x_pos 120
	#set x_pos [expr $x_pos + 120]

	for {set x 0} {$x<=8} {incr x} {
		editPin -side Top -pin RowAddFromDet[$x] -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign [expr $x_pos] $core_height -snap USERGRID
		incr x_pos 1
	}

	for {set x 0} {$x<=7} {incr x} {
		editPin -side Top -pin ColAddFromDet[$x] -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign [expr $x_pos] $core_height -snap USERGRID
		incr x_pos 1
	}

	editPin -side Top -pin PriFromDet -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign [expr $x_pos] $core_height -snap USERGRID
	incr x_pos 1

    ## 
    editPin -side Top -pin TSclkdel -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign [expr $x_pos] $core_height -snap USERGRID
	incr x_pos 1

    ## Pixel Tune Rams
    for {set x 0} {$x<4} {incr x} {
		editPin -side Top -pin WrRAM_out$x -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign [expr $x_pos] $core_height -snap USERGRID
		incr x_pos 1
	}
 
 

	######################
	## Bottom
	######################

    

	set x_pos 150

	# Shift Register
    ###################

	editPin -side Bottom -pin Ck1_Ext -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign [expr $x_pos] $core_height  -snap USERGRID
	incr x_pos 1

	editPin -side Bottom -pin Ck2_Ext -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign [expr $x_pos] $core_height -snap USERGRID
	incr x_pos 1
  
    for {set x 1} {$x<=5} {incr x} {
        editPin -side Bottom -pin Ld_Ext$x -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign [expr $x_pos] $core_height -snap USERGRID
	    incr x_pos 1
	}

	editPin -side Bottom -pin SIn_Ext -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign [expr $x_pos] $core_height -snap USERGRID
	incr x_pos 1

	editPin -side Bottom -pin Readback_Ext -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign [expr $x_pos] $core_height -snap USERGRID
	incr x_pos 1

	editPin -side Bottom -pin SOut_Ext -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign [expr $x_pos] $core_height -snap USERGRID
	incr x_pos 1

    ## SR Outputs to connect to next in the SR chain (internally in chip)
    editPin -side Bottom -pin SR_CK1_out -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign [expr $x_pos] $core_height -snap USERGRID
	incr x_pos 1
    editPin -side Bottom -pin SR_CK2_out -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign [expr $x_pos] $core_height -snap USERGRID
	incr x_pos 1
    editPin -side Bottom -pin SR_SIn_out -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign [expr $x_pos] $core_height -snap USERGRID
	incr x_pos 1

    for {set x 1} {$x<=5} {incr x} {
        editPin -side Bottom -pin Ld_Internal_Ext$x -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign [expr $x_pos] $core_height -snap USERGRID
	    incr x_pos 1
	}

	for {set x 0} {$x<4} {incr x} {
        editPin -side Bottom -pin WrRAM_in$x -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign [expr $x_pos] $core_height -snap USERGRID
	    incr x_pos 1
	}

	
	# I2C
    ################
	editPin -side Bottom -pin i2c_sda -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign [expr $x_pos] $core_height  -snap USERGRID
	incr x_pos 1

	editPin -side Bottom -pin i2c_scl -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign [expr $x_pos] $core_height -snap USERGRID
	incr x_pos 1
	editPin -side Bottom -pin use_I2C_extern -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign [expr $x_pos] $core_height  -snap USERGRID
	incr x_pos 1

	editPin -side Bottom -pin use_I2C_tfc_extern -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign [expr $x_pos] $core_height -snap USERGRID
	incr x_pos 1

	for {set x 0} {$x<=2} {incr x} {
		editPin -side Top -pin i2c_chip_id[$x] -layer 2 -use SIGNAL -pinWidth $pinWidth -pinDepth 1 -fixedPin 1 -fixOverlap 0 -honorConstraint 0 -assign [expr $x_pos] $core_height -snap USERGRID
		incr x_pos 1
	}

	set x_pos [expr $x_pos + 160]

	
	setPinAssignMode -pinEditInBatch false
	
	fit
}
