



proc innovus_5_1_addFillers args {
	global STD_CELLS
	
	#add filler cells to the design
	addFiller -cell $STD_CELLS(physical/fillers) -prefix FILLER
	
	## Fix DRC
	ecoRoute -target
}

proc innovus_5_2_signoff args {
	
	## Set Mode parameters for Extraction
	setExtractRCMode -reset
	setExtractRCMode -engine postRoute -effortLevel high -coupled true
	
	setDelayCalMode -reset
	setDelayCalMode -engine default -SIaware true 
	
	setAnalysisMode -reset
	setAnalysisMode -analysisType onChipVariation -cppr both 
	
	## Extract
	extractRC
	
	## Save Timing
	timeDesign -prefix signoff -signoff -outDir reports
	timeDesign -prefix signoff_hold -signoff -outDir reports -hold
	summaryReport -outDir reports
	
}

proc innovus_5_3_streamout args {
	
	global STD_CELLS 
	global top_module
	global cell_lib
	
	exec mkdir -p stream_out
	
	## SDF
	############
	#write_sdf -recompute_delay_calc -min_view functional_800p_slowHT -max_view functional_800p_fastLT -edges noedge ./stream_out/${top_module}.functional_800p.sdf
	#write_sdf -recompute_delay_calc -min_view functional_1n25_slowHT -max_view functional_1n25_fastLT -edges noedge ./stream_out/${top_module}.functional_1n25.sdf
	#write_sdf  -view functional_800p_slowHT -edges noedge   ./stream_out/${top_module}.functional_800p.slow.sdf
	#write_sdf  -view functional_1n25_slowHT -edges noedge ./stream_out/${top_module}.functional_1n25.slow.sdf
	
	 
	##write_sdf -view functional_fastLT -edges noedge ./stream_out/${top_module}.fast.sdf
	
	## Save Netlist for LVS
	
	if {${::CELLS_AMS}} {
		saveNetlist -flat -includePhysicalCell [join $STD_CELLS(physical/fillcaps)] -excludeLeafCell ./stream_out/${top_module}.lvs.v
	} else {
		saveNetlist -includePowerGround -flat -includePhysicalCell [join $STD_CELLS(physical/fillcaps)] -excludeLeafCell ./stream_out/${top_module}.lvs.v
	}
	 
	
	## Save Netlist for Simulation
	saveNetlist ./stream_out/${top_module}.simulation.nonFlat.v
	
	
	## GDS  -mapFile $STD_CELLS(physical/gdsmap)
	###################
	setStreamOutMode -textSize .2
	if {${::CELLS_AMS}} {
	
	        puts "Streamout Map: $STD_CELLS(physical/gdsmap)"
	        streamOut -outputMacros -mapFile $STD_CELLS(physical/gdsmap) ./stream_out/${top_module}.gds
	
	} else {
	        puts "Streamout Map: $STD_CELLS(physical/gdsmap)"
			streamOut -outputMacros -mapFile $STD_CELLS(physical/gdsmap) ./stream_out/${top_module}.gds
	        #streamOut ./stream_out/${top_module}.gds \
	        -merge $STD_CELLS($cell_lib/gds) \
	        -outputMacros \
	        -mapFile $STD_CELLS(physical/gdsmap)

	}

	saveDesign savings/save06_postStreamout.enc
	
	
	## Add Ports Text
	#############
	return
	set terms [dbGet top.terms]
	foreach term $terms {
		set pt [dbGet $term.pt]
		set port [get_ports [dbGet $term.name]]
		foreach p $pt {
			puts "Adding Custom text"
			addCustomText [dbGet $term.layer.name] [get_property $port net_name] [lindex $p 0] [lindex $p 1] 0.1
		}
	}
	
	set terms [dbGet top.pgTerms]
	foreach term $terms {
		set pt [dbGet $term.pt]
		# no buses here
		set name [dbGet $term.name]
		foreach p $pt {
			addCustomText [dbGet $term.layer.name] $name [lindex $p 0] [lindex $p 1] 0.1
		}
	}

	
	
	
	
	
}
