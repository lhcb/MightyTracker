##############################
## !! This file is ported to use newest CCOpt clock tree synthesis
## File ported in 02/2020 by Richard.
#################################

proc innovus_3_set_mode args {
	global clockbuffer_cells
	global clockinverter_cells
	global clockgating_cells
	

	setExtractRCMode -engine preroute
	setNanoRouteMode -reset
	setDesignMode -topRoutingLayer 4 -bottomRoutingLayer 2
	setNanoRouteMode  -routeWithTimingDriven true \
		-routeWithSiDriven true  \
		-routeWithViaInPin true \
		-routeWithViaOnlyForStandardCellPin 1:1 \
		-drouteUseMultiCutViaEffort high

	## Allowed Cells
	#set_ccopt_property buffer_cells 	$clockbuffer_cells
	## Leave empty to force non inverters to be not used
	set_ccopt_property buffer_cells 	{}
	set_ccopt_property inverter_cells	$clockinverter_cells
	set_ccopt_property clock_gating_cells $clockgating_cells

	set_ccopt_property use_inverters true
	
	## Create Non default Route for double spacing double width
	set non_default_rule width2x_space2x

	## Create Route Types
	catch {delete_route_type -name leaf_rule}
	create_route_type -name leaf_rule -non_default_rule $non_default_rule   -top_preferred_layer M3 -bottom_preferred_layer M2
	
	catch {delete_route_type -name trunk_rule}
	create_route_type -name trunk_rule -non_default_rule $non_default_rule	-top_preferred_layer M3 -bottom_preferred_layer M3  -shield_net GND 
	
	catch {delete_route_type -name top_rule}
	create_route_type -name top_rule   -non_default_rule $non_default_rule	-top_preferred_layer M4 -bottom_preferred_layer M4
	
	set_ccopt_property -net_type leaf 	route_type leaf_rule
	set_ccopt_property -net_type trunk 	route_type trunk_rule
	
	# Top Tree routing will be done for large trees only
	
	set_ccopt_property -net_type top route_type top_rule
	set_ccopt_property  routing_top_min_fanout 10000


        puts "EOF CTS Setup"
	#set_ccopt_property -net_type trunk target_max_trans 200ps 
	#set_ccopt_property -net_type leaf target_max_trans 150ps
	
	#set_ccopt_property -net_type trunk target_max_trans 700ps 
	#set_ccopt_property -net_type leaf  target_max_trans 600ps

	
}

proc innovus_3_1_setup args {
	
   	
	
	## First remove clock trees to ease reusing this step when debugging
	#############
	reset_ccopt_config
	delete_ccopt_clock_trees *
	
	## Set Properties
	##################
	innovus_3_set_mode
	
	## Create Clock Spec
	####################
	exec mkdir -p cts
	create_ccopt_clock_tree_spec -file cts/ccopt_spec.tcl
	


   #optDesign -preCTS -outDir reports -prefix cts_prep

	#step5a_onlySetCTSMode
	
#setCTSMode -reset
  # setCTSMode -addClockRootProp true 
	#-optAddBuffer true 
	#-optLatency true 
	##-reportHTML true 
	#-routeBottomPreferredLayer 2 
	#-routeClkNet false 
	#-routeTopPreferredLayer 5 
	#-routeLeafBottomPreferredLayer 2 
	#-routeLeafTopPreferredLayer 5 
	#-powerAware true 
	#-synthLatencyEffort high 
	#-traceIoPinAsLeaf true
 #-powerAware true
	
   
   #createClockTreeSpec \
   		-output ./cts/Clock.ctstch \
   		-bufferList $clockbuffer_cells
}


proc innovus_3_2_CTS args {
	
	####### New CCOPT ####################
	source cts/ccopt_spec.tcl
	
	ccopt_design 

		
	cts_refine_clock_tree_placement
	
	
	########################################
	return
	####### OLD CTS MODE ####################
    cleanupSpecifyClockTree

   #bufferTreeSynthesis -nets {RESET} -bufList {UCL_INV4 UCL_BUF UCL_BUF4 UCL_BUF8 UCL_BUF8_2 UCL_BUF16 UCL_INV16 UCL_BUF16B} -leafPorts {UCL_DFF_RES/RES UCL_DFF_SET/SET} -maxDelay +800ps -fixedBuf -fixedNet

    specifyClockTree -clkfile ./cts/Clock.ctstch

    clockDesign
    #ckSynthesis -report ./reports/clock.report -forceReconvergent -breakLoop

    #Doing postCTS optimization
	setOptMode -powerEffort low
    optDesign -postCTS -outDir reports -prefix postcts
    optDesign -postCTS -hold -outDir reports -prefix postcts_hold

    #The clock tree synthesis places clock buffers even if there is no space. That results in nasty DRC violations!
    #Execute refinePlace in order to correct the stuff
    refinePlace

    #Save the design state after the CTS
    saveDesign savings/save04_postCTS.enc

}

proc innovus_3_3_postCTS args {
	
	setOptMode -reset
	setOptMode -powerEffort low
	setOptMode -usefulSkewCCOpt standard
	setOptMode -fixHoldAllowSetupTnsDegrade true
	setOptMode -usefulSkewCCOpt standard
	
	setExtractRCMode -engine preroute
	
	optDesign -postCTS
	optDesign -postCTS -hold
	
	optDesign -postCTS -incr
	optDesign -postCTS -hold -incr
	
	## Time Design
	###############
	exec mkdir -p reports/timing/
	timeDesign 	-postCTS -reportOnly -expandedViews -outDir reports/timing/  -numPaths 100 -prefix cts
	timeDesign 	-postCTS -hold -reportOnly -expandedViews -outDir reports/timing/  -numPaths 100 -prefix cts_hold
	
	#Save the design state after the CTS
	saveDesign savings/save04_postCTS.enc
	
	
}

