# Mighty Tracker ASIC

This file contains the basic documentation needed to get start with the asic's low level 
simulation and the synthesis/place and route. 

## Scripts/Tools Environment 

The environment to work with the asic is structured around a couple of folder and files which one should know:

- **load.sh**
    - this script checks if a design kit was loaded, otherwise offers to load one
    - It adds the bin/ folder to the path, which contains utility scripts to start the tools
    - It also adds a variable called $BASE which points to this folder and is used in many files to get an absolute reference to other files, like in verilog netlists.
- **bin**: Place your utility scripts here
- **building_blocks**: contains some Verilog/SystemVerilog code imported from other projects, mostly simulation helpers
- **rtl**: contains the synthesisable chip's digital part
- **verification**: contains the testbenches
- **implementation**: contains the synthesis and place and route scripts for Genus/Innovus
- **run**: The run folder is ignored by GIT, and should be used to start all the tools. 
    - for example to start a top level simulation, create a run folder called run/sim_top and run the command *design_sim_rtl*

## Simulation 

The bin/ folder contains the necessary scripts to start simulations at different stages of the design. 

Those scripts call xcelium without a Graphical environment by default, but all command line arguments are passed on to xcelium. 

For example the script **desing_sim_rtl** starts a top level RTL based simulation.
To start any simulation with the SimVision GUI environment, simply add *-gui* to the command: 

    run/sim_top $ design_sim_rtl # starts without GUI 
    run/sim_top $ design_sim_rtl -gui # starts with a GUI 

The folder **verification** contains the testbenches and the .f files for each testbench. 

The .f files contain the list of arguments for this simulator, like the verilog files and other directives like defines. 
F files can reference other F files, which makes it easy to write well structured netlists. 

For example, the mightypix_top_tb.f file loads the rtl/mighypix_top.f file which contains the netlist for the top level, and adds the required files for the testbench environment.

## Synthesis/Place and route


###  Constraints 

The folder implementation/ contains the scripts for synthesis and place and route. 

The bin folder contains the scripts to start the tool quickly with the right TCL script loaded. 

Only the constaints are common to both tools, and are defined using two set of files: 

- **implementation/constraints** contain the actual constraints SDC file(s). At the moment, only the constraints_functionalmode.sdc file is being used.
- **implementation/common_load_mmmc.tcl** is a TCL script that is loaded by both Genus and Innovus, and loads the constraints using the multi-mode multi-corner functionality of the tools. 

The **common_load_mmmc.tcl** uses the TCL global variable $implStage to detect if the script is running under GENUS or INNOVUS, and adapts the commands call for the tool. 

Most of the commands in that file are nearly identical for both tools, but since some arguments name differ, the commands can't be called as-is in both. 

### Run folders 

!! The naming of the run folder is important, since the place and route script use a convention name to find the synthesis output netlist. 


Be careful to always name the synthesis run folder "synthesis", and the place and route folder "par" - these two folders can be placed at any level under run/, as long as they are next to each other. 

Examples: 

- Valid:  run/synthesis and run/par 
- Valid:  run/mytest/synthesis and run/mytest/par 
- !Valid: run/synth run/placeandroute


## Running Synthesis 

Prepare the Folders 
    $ mkdir -p run/synthesis/ 
    $ cd run/synthesis 

### Synthesis without stopping

    run/synthesis $ design_synthesis

Runs through the synthesis whitout stopping

### Synthesis with design check stop

    run/synthesis $ CHECK_DESIGN=1 design_synthesis

With the environment variable CHECK_DESIGN defined, the synthesis script will stop in a couple of strategic places, for example:

- After Constraints are read, so that the constraints commands report can be checked 
- After the check_design reports have been generated 

When the script stops, just enter the command *resume* in the Genus terminal to resume the script, or *exit* to stop if something is incorrect. 

### Post-Synthesis simulation 

WIP