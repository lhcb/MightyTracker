# Code for Mighty Tracker Project

## MightyPix ASIC folder

The sources for the ASIC are located under mightypix/, which include the RTL sources, the scripts for place and route and the ASIC-centric simulation testbenches. 

To use the scripts and environments in the mightipix folder, do the following:

- source the AMS 180 design kit depending on your environment. This is really only needed if you are going to run implementation scripts
- load your Cadence toolchain if necessary
- source the "load.sh" script, which will change your terminal environment providing:
    - a $REPO environment variable pointing to the base of the repository
    - a $BASE environment pointing to the mightipix project folder
    - the mightypix/bin folder is added to your PATH to easily provide helper scripts to quickly start the various tools

### Running tools

Whenever you are running a tool, please create a new "run" folder in the mightipix folder, and create a folder hierarchy in the run folder as you please. The run folder is ignored by default to avoid unpleasant commit of large working data. 

Some tools require a specific layout under the run folder by convention. 
For example: 

- run/sim_xxxx -> a simulation folder
    - example: run/sim_rtl -> RTL level simulation
    - example: run/sim_synth -> Post synthesis simulation
    - example: run/sim_par -> Post P&R simulation
- run/synthesis -> Synthesis run of the ASIC (searches for "../par" to run flooprplan based synthesis)
- run/par -> Place and route run of the ASIC (requires "../synthesis" to be present)


## MightyPix Simulation

WIP 19/04/2021

We are working with Sigrid, Karol and Richard to learn how the Chip basic simulation works, and how we can drive the Chip from the LHCB firmware
simulation environment. 

At the moment, the reference DUT is build from a testbench:

- verification/mightypix_top_tb_dut.v wraps the Chip DUT, and provides some initialisation for the ASIC. 
    - This module is intended to be instantiated in the LHCb simulation environment
- verification/mightypix_top_tb_dut.f is a Netlist file which can be followed to find all the files required to simulate the ASIC


## Submodules

- **MightyPixdaq:** MightyPix DAQ framework based on Basil
- **basil:** Modular DAQ system and system testing framework in Python, used for thr MightyPix DAQ
- **readout40-firmware:** Firmware for the LHCb Upgrade Readout System
- **lpgbt_model:** Model of the lpGBT chip
- **verification_framework:** LHCb Verification Framework for the MightyPix
